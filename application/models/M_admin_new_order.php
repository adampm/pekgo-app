<?php
class M_admin_new_order extends CI_Model {

    private $table = 'apm_admin';
    private $table_kategori = 'apm_kategori';

    public function resume_login_check($id_admin, $username, $session_id, $browser)
    {
        $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'username' => $username, 'session_id' => $session_id, 'browser' => $browser, 'status' => '1'), 1, 0);
        $row = $query->num_rows();
        return $row;
    }

    public function get_admin_info($id_admin)
    {
        $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'status' => '1'), 1, 0);
        return $query;
    }

    public function create_new_order($data)
    {
        $this->db->trans_start();
        $this->db->insert('apm_order', $data);
        $id_order = $this->db->insert_id();

        foreach($this->cart->contents() as $res){
            $berat = $res['berat'];
            $harga = $res['price'];
            $qty = $res['qty'];
            $id = $res['id'];
            $arr_id = explode('_', $id);
            $id_produk = $arr_id[0];
            $size = $arr_id[1];
            $this->db->query("UPDATE apm_stock SET stock_".$size." = (stock_".$size." - ".$qty.") WHERE id_produk = '".$id_produk."' ");
            $query_id_stock = $this->db->query("SELECT id_stock FROM apm_stock WHERE id_produk = '".$id_produk."' ");
            foreach($query_id_stock->result() as $res){
                $id_stock = $res->id_stock;
            }
            $data_sl = array(
                'id_stock' => $id_stock,
                'id_order' => $id_order,
                'action' => 'sell',
                'action_date' => date('Y-m-d H:i:s'),
                'size' => $size,
                'qty' => $qty,
                'id_admin' => $this->session->userdata('id_admin')
            );
            $this->db->insert('apm_stock_log', $data_sl);
            $id_stock_log = $this->db->insert_id();
            $data_po = array(
                'id_order' => $id_order,
                'id_produk' => $id_produk,
                'size' => $size,
                'qty' => $qty,
                'harga' => $harga,
                'berat_produk' => $berat,
                'status' => '1',
                'id_stock_log' => $id_stock_log
            );
            $this->db->insert('apm_order_produk', $data_po);
        }
        $this->db->trans_complete();
    }

    public function check_stock()
    {
        $total_data = 0;
        $arr_nama_produk = "";
        foreach($this->cart->contents() as $res){
            print_r($res);
            $nama_produk = $res['name'];
            $id = $res['id'];
            $qty = $res['qty'];
            $arr_id = explode('_', $id);
            $id_produk = $arr_id[0];
            $size = $arr_id[1];
            $query = $this->db->query("SELECT * FROM apm_stock WHERE id_produk = '".$id_produk."' AND stock_".$size." >= '".$qty."' ");
            $total_row = $query->num_rows($query);
            if($total_row == 0){
                $total_data -= $qty;
                $arr_nama_produk .= $nama_produk.' Size '.$size.','; 
            }else{
                $total_data += $qty;
            }
        }

        $this->session->set_userdata('arr_nama_produk', $arr_nama_produk);
        return $total_data;
    }

    public function add($kategori)
    {
        $data = array('kategori' => $kategori, 'status' => '1');
        $proses_add = $this->db->insert($this->table_kategori, $data);
        return $proses_add;
    }

    public function get_member()
    {
        $this->db->select('id_member, nama_lengkap, phone');
        $this->db->from('apm_member');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query;
    }

    public function get_member_info($id_member)
    {
        $this->db->select('*');
        $this->db->from('apm_member');
        $this->db->where('status', '1');
        $this->db->where('id_member', $id_member);
        $query = $this->db->get();
        return $query;
    }

    public function get_produk($q)
    {
        $this->db->select('id_produk, nama_produk');
        $this->db->from('apm_produk');
        $this->db->where('status', '1');
        if($q != null){
            $this->db->like('nama_produk', $q);
        }
        $query = $this->db->get();
        return $query;
    }

    public function get_produk_info($id_produk)
    {
        $this->db->select('*');
        $this->db->from('apm_produk');
        $this->db->where('status', '1');
        $this->db->where('id_produk', $id_produk);
        $query = $this->db->get();
        return $query;
    }

    public function get_kategori_by_id($id_kategori)
    {
        $query = $this->db->get_where($this->table_kategori, array('id_kategori' => $id_kategori), 1, 0);
        return $query;
    }

    public function update($data, $id_kategori)
    {
        $this->db->where('id_kategori', $id_kategori);
        $this->db->update($this->table_kategori, $data);
    }

    public function delete($id_kategori)
    {
        $this->db->trans_start();
        $this->db->query("DELETE FROM ".$this->table_kategori." WHERE id_kategori = '".$id_kategori."'");
        $this->db->trans_complete();
        $query = $this->db->trans_status();
        return $query;
    }

    public function get_kode_unik()
    {
        $cur_year = date('Y');
        $cur_month = date('m');
        $this->db->where('tahun', $cur_year);
        $this->db->where('bulan', $cur_month);
        $arr_kode_unik = $this->db->get('apm_kode_unik');
        $total_kode_unik = $arr_kode_unik->num_rows();
        if($total_kode_unik == 0){
            $kode_uniknya = $this->create_kode_unik();
        }else{
            $kode_uniknya = $this->increase_kode_unik();
            
        }
        return $kode_uniknya;
    }

    public function create_kode_unik()
    {
        $cur_year = date('Y');
        $cur_month = date('m');
        $this->db->insert('apm_kode_unik', array('tahun' => $cur_year, 'bulan' => $cur_month, 'kode_unik' => '1'));
    }

    public function increase_kode_unik()
    {
        $cur_year = date('Y');
        $cur_month = date('m');
        $this->db->query("UPDATE apm_kode_unik SET kode_unik = (kode_unik + 1) WHERE tahun = '".$cur_year."' AND bulan = '".$cur_month."' ");

        $this->db->select('kode_unik');
        $this->db->where('tahun', $cur_year);
        $this->db->where('bulan', $cur_month);
        $arr_kode_unik = $this->db->get('apm_kode_unik')->row();
        $kode_unik = $arr_kode_unik->kode_unik;
        return $kode_unik;
    }

    


}