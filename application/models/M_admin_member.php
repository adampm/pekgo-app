<?php
class M_admin_member extends CI_Model {

        private $table = 'apm_admin';
        private $table_produk = 'apm_produk';
        private $table_admin = 'apm_admin';
        private $table_member = 'apm_member';
        public function resume_login_check($id_admin, $username, $session_id, $browser)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'username' => $username, 'session_id' => $session_id, 'browser' => $browser, 'status' => '1'), 1, 0);
            $row = $query->num_rows();
            return $row;
        }

        public function get_admin_info($id_admin)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'status' => '1'), 1, 0);
            return $query;
        }

        public function get_member($keyword)
        {
            $sql = "SELECT m.id_member, m.nama_lengkap, m.email, m.phone, m.birth_place, m.birth_date, m.id_province, m.id_city, m.id_district, m.created_date, m.updated_date AS last_login, m.status ";
            $sql .= "FROM apm_member AS m ";
            if(!empty($keyword)){
                $sql .= "WHERE m.nama_lengkap LIKE '%".$keyword."%' ";
                $sql .= "OR m.email LIKE '%".$keyword."%' ";
                $sql .= "OR m.phone LIKE '%".$keyword."%' ";
            }
            $sql .= "ORDER BY m.id_member DESC ";
            $query = $this->db->query($sql);
            return $query;
        }

        public function lock_member($id_member)
        {
            $this->db->where('id_member', $id_member);
            $query = $this->db->update($this->table_member, array('status' => '2'));
            if($query){
                return "berhasil";
            }else{
                return "gagal";
            }
        }
        public function unlock_member($id_member)
        {
            $this->db->where('id_member', $id_member);
            $query = $this->db->update($this->table_member, array('status' => '1'));
            if($query){
                return "berhasil";
            }else{
                return "gagal";
            }
        }
}