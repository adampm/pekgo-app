<?php
class M_admin_stock_log extends CI_Model {

        private $table = 'apm_admin';
        private $table_produk = 'apm_produk';
        private $table_admin = 'apm_admin';
        private $table_stock = 'apm_stock';
        private $table_stock_log = 'apm_stock_log';

        public function resume_login_check($id_admin, $username, $session_id, $browser)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'username' => $username, 'session_id' => $session_id, 'browser' => $browser, 'status' => '1'), 1, 0);
            $row = $query->num_rows();
            return $row;
        }

        public function get_admin_info($id_admin)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'status' => '1'), 1, 0);
            return $query;
        }

        public function get_stock_log($keyword)
        {
            $sql = "SELECT sl.action_date AS tanggal, p.nama_produk, sl.action AS tipe, sl.size, sl.qty, o.no_invoice, a.nama_lengkap AS admin ";
            $sql .= "FROM apm_stock_log AS sl ";
            $sql .= "LEFT JOIN apm_stock AS s ";
                $sql .= "ON s.id_stock = sl.id_stock ";
            $sql .= "LEFT JOIN apm_produk AS p ";
                $sql .= "ON p.id_produk = s.id_produk ";
            $sql .= "LEFT JOIN apm_order AS o ";
                $sql .= "ON o.id_order = sl.id_order ";
            $sql .= "LEFT JOIN apm_admin AS a ";
                $sql .= "ON a.id_admin = sl.id_admin ";
            if(!empty($keyword)){
                $sql .= "WHERE p.nama_produk LIKE '%".$keyword."%' ";
            }
            $sql .= "ORDER BY sl.id_stock_log DESC ";
            $query = $this->db->query($sql);
            return $query;
        }
}