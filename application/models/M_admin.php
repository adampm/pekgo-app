<?php
class M_admin extends CI_Model {

        private $table = 'apm_admin';
        private $table_admin_log = 'apm_admin_log';
        private $table_order = 'apm_order';
        private $table_order_produk = 'apm_order_produk';

        public function resume_login_check($id_admin, $username, $session_id, $browser)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'username' => $username, 'session_id' => $session_id, 'browser' => $browser, 'status' => '1'), 1, 0);
            $row = $query->num_rows();
            return $row;
        }

        public function check_username_login($username)
        {
            $query = $this->db->get_where($this->table, array('username' => $username, 'status' => '1'), 1, 0);
            $row = $query->num_rows();
            return $row;
        }

        public function check_pass_login($username, $pass)
        {
            $query = $this->db->get_where($this->table, array('username' => $username, 'status' => '1'), 1, 0);
            $row = $query->num_rows();
            if($row > 0){
                    $res = $query->row();
                    $pass_hash = $res->pass_hash;
                    $check_pass = password_verify($pass, $res->pass_hash);
                    
                    if($check_pass === TRUE){
                        return '1';
                    }else{
                        return '0'; 
                    }
                    
            }else{
                return '0';
            }
        }

        public function get_single_admin_data($username)
        {
            $query = $this->db->get_where($this->table, array('username' => $username, 'status' => '1'), 1, 0);
            $row = $query->num_rows();
                if($row > 0){
                        $res = $query->row();
                        $result['status'] = 200;
                            $data['id_admin'] = $res->id_admin;
                            $data['username'] = $res->username;
                            $data['nama_lengkap'] = $res->nama_lengkap;
                            $data['session_id'] = $res->session_id; 
                            $data['username_status'] = $res->status; 
                        $result['data'] = $data;
                }else{
                        $result['status'] = 400;
                        $data = [];
                        $result['data'] = $data;
                }

                return $result;
        }

        public function get_admin_info($id_admin)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'status' => '1'), 1, 0);
            return $query;
        }

        public function update_admin($data, $where)
        {
            $this->db->update($this->table, $data);
            $this->db->where('id_admin', $where);
        }

        public function insert_admin_log($data)
        {
            $this->db->insert($this->table_admin_log, $data);
        }

}