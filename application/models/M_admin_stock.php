<?php
class M_admin_stock extends CI_Model {

    private $table = 'apm_admin';
    private $table_produk = 'apm_produk';
    private $table_stock = 'apm_stock';
    private $table_stock_log = 'apm_stock_log';

    public function resume_login_check($id_admin, $username, $session_id, $browser)
    {
        $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'username' => $username, 'session_id' => $session_id, 'browser' => $browser, 'status' => '1'), 1, 0);
        $row = $query->num_rows();
        return $row;
    }

    public function get_admin_info($id_admin)
    {
        $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'status' => '1'), 1, 0);
        return $query;
    }

    public function add($id_produk, $tipe, $stock_s, $stock_m, $stock_l, $stock_xl, $stock_xxl, $action_date, $id_admin)
    {
        $this->db->trans_start();

        $this->db->where('id_produk', $id_produk);
        $query_id_stock = $this->db->get($this->table_stock);
        $id_stock = $query_id_stock->row()->id_stock;
        $stock_s_old = $query_id_stock->row()->stock_s;
        $stock_m_old = $query_id_stock->row()->stock_m;
        $stock_l_old = $query_id_stock->row()->stock_l;
        $stock_xl_old = $query_id_stock->row()->stock_xl;
        $stock_xxl_old = $query_id_stock->row()->stock_xxl;

        if($tipe == 'add'){
            $stock_s_new = $stock_s_old + $stock_s;
            $stock_m_new = $stock_m_old + $stock_m;
            $stock_l_new = $stock_l_old + $stock_l;
            $stock_xl_new = $stock_xl_old + $stock_xl;
            $stock_xxl_new = $stock_xxl_old + $stock_xxl;
        }else{
            $stock_s_new = $stock_s_old - $stock_s;
            $stock_m_new = $stock_m_old - $stock_m;
            $stock_l_new = $stock_l_old - $stock_l;
            $stock_xl_new = $stock_xl_old - $stock_xl;
            $stock_xxl_new = $stock_xxl_old - $stock_xxl;
        }

        $data_stock = array(
            'stock_s' => $stock_s_new,
            'stock_m' => $stock_m_new,
            'stock_l' => $stock_l_new,
            'stock_xl' => $stock_xl_new,
            'stock_xxl' => $stock_xxl_new
        );
        
        
        $this->db->where('id_produk', $id_produk);
        $this->db->update($this->table_stock, $data_stock);

        if($stock_s > 0){
            $data_stock_log = array(
                'id_stock' => $id_stock,
                'action' => $tipe,
                'action_date' => $action_date,
                'size' => 's',
                'qty' => $stock_s,
                'id_admin' => $id_admin
            );

            $this->db->insert($this->table_stock_log, $data_stock_log);
        }

        if($stock_m > 0){
            $data_stock_log = array(
                'id_stock' => $id_stock,
                'action' => $tipe,
                'action_date' => $action_date,
                'size' => 'm',
                'qty' => $stock_m,
                'id_admin' => $id_admin
            );

            $this->db->insert($this->table_stock_log, $data_stock_log);
        }

        if($stock_l > 0){
            $data_stock_log = array(
                'id_stock' => $id_stock,
                'action' => $tipe,
                'action_date' => $action_date,
                'size' => 'l',
                'qty' => $stock_l,
                'id_admin' => $id_admin
            );

            $this->db->insert($this->table_stock_log, $data_stock_log);
        }

        if($stock_xl > 0){
            $data_stock_log = array(
                'id_stock' => $id_stock,
                'action' => $tipe,
                'action_date' => $action_date,
                'size' => 'xl',
                'qty' => $stock_xl,
                'id_admin' => $id_admin
            );

            $this->db->insert($this->table_stock_log, $data_stock_log);
        }

        if($stock_xxl > 0){
            $data_stock_log = array(
                'id_stock' => $id_stock,
                'action' => $tipe,
                'action_date' => $action_date,
                'size' => 'xxl',
                'qty' => $stock_xxl,
                'id_admin' => $id_admin
            );

            $this->db->insert($this->table_stock_log, $data_stock_log);
        }

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    public function get_list_produk()
    {
        $this->db->where('status', '1');
        $query = $this->db->get($this->table_produk);
        return $query;
    }

    public function get_stock($keyword)
    {
        $sql = "SELECT p.nama_produk, s.stock_s, s.stock_m, s.stock_l, s.stock_xl, s.stock_xxl ";
        $sql .= "FROM apm_stock AS s ";
        $sql .= "LEFT JOIN apm_produk AS p ";
            $sql .= "ON p.id_produk = s.id_produk ";
        $sql .= "WHERE p.`status` = '1' ";
        if(!empty($keyword)){
            $sql .= "AND ";
            $sql .= "(p.nama_produk LIKE '%".$keyword."%' ";
            $sql .= "OR s.stock_s LIKE '%".$keyword."%' ";
            $sql .= "OR s.stock_m LIKE '%".$keyword."%' ";
            $sql .= "OR s.stock_l LIKE '%".$keyword."%' ";
            $sql .= "OR s.stock_xl LIKE '%".$keyword."%' ";
            $sql .= "OR s.stock_xxl LIKE '%".$keyword."%') ";
        }
        $sql .= "ORDER BY p.nama_produk DESC ";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_produk_by_id($id_produk)
    {
        $sql = "SELECT p.id_produk, p.nama_produk, k.id_kategori, k.kategori, p.berat, p.harga ";
        $sql .= "FROM apm_produk AS p ";
        $sql .= "LEFT JOIN apm_kategori AS k ";
            $sql .= "ON k.id_kategori = p.id_kategori ";
        $sql .= "WHERE p.id_produk = '".$id_produk."' AND p.status = '1'";
        $query = $this->db->query($sql);
        return $query;
    }
        
}