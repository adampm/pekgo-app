<?php
class M_admin_order_success extends CI_Model {

    private $table = 'apm_admin';
    private $table_order = 'apm_order';
    public function resume_login_check($id_admin, $username, $session_id, $browser)
    {
        $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'username' => $username, 'session_id' => $session_id, 'browser' => $browser, 'status' => '1'), 1, 0);
        $row = $query->num_rows();
        return $row;
    }

    public function get_admin_info($id_admin)
    {
        $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'status' => '1'), 1, 0);
        return $query;
    }

    public function get_order_success()
    {
        $sql = "SELECT o.id_order, m.nama_lengkap, o.no_invoice, o.sub_total, o.grand_total, o.ongkir, o.kode_unik, o.grand_total, o.ekspedisi, o.status, o.created_date, o.updated_date, o.created_by, o.updated_by, o.status_payment, o.status_print ";
        $sql .= "FROM apm_order AS o ";
        $sql .= "LEFT JOIN apm_member AS m ";
            $sql .= "ON m.id_member = o.id_member ";
        $sql .= "WHERE o.status = '1' ";
        $sql .= "ORDER BY o.id_order DESC ";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_order_success2($keyword, $start, $length)
    {
        $sql = "SELECT o.id_order, m.nama_lengkap, o.no_invoice, o.sub_total, o.grand_total, o.ongkir, o.tambahan, o.kode_unik, o.grand_total, o.ekspedisi, o.status, o.created_date, o.updated_date, o.created_by, o.updated_by, o.status_payment, o.status_print ";
        $sql .= "FROM apm_order AS o ";
        $sql .= "LEFT JOIN apm_member AS m ";
            $sql .= "ON m.id_member = o.id_member ";
        $sql .= "WHERE o.status = '1' ";
        if(!empty($keyword)){
            $sql .= "AND (m.nama_lengkap LIKE '%".$keyword."%' ";
            $sql .= "OR o.no_invoice LIKE '%".$keyword."%') ";
        }
        $sql .= "ORDER BY o.id_order DESC ";
        /*if($length > '-1'){
            $sql .= "LIMIT ".$start.", ".$length." ";
        }*/
        
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_order_information($id_order)
    {
        $sql = "SELECT o.id_order, m.nama_lengkap, o.no_invoice, o.sub_total, o.grand_total, o.ongkir, o.tambahan, o.kode_unik, o.grand_total, o.total_berat, o.ekspedisi, o.status, o.created_date, o.updated_date, created_by, o.status_payment, o.status_print, o.catatan ";
        $sql .= "FROM apm_order AS o ";
        $sql .= "LEFT JOIN apm_member AS m ";
            $sql .= "ON m.id_member = o.id_member ";
        $sql .= "WHERE o.status = '1' ";
        $sql .= "AND o.id_order = '".$id_order."' ";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_customer_information($id_order)
    {
        $sql = "SELECT m.id_member, m.nama_lengkap, m.email, m.nama_lengkap, m.phone, m.alamat, m.kode_pos ";
        $sql .= "FROM apm_order AS o ";
        $sql .= "LEFT JOIN apm_member AS m ";
            $sql .= "ON m.id_member = o.id_member ";
        $sql .= "WHERE o.status = '1' ";
        $sql .= "AND o.id_order = '".$id_order."' ";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_produk_information($id_order)
    {
        $sql = "SELECT op.id_order_produk, p.nama_produk, op.size, op.qty, op.harga, op.berat_produk * op.qty AS total_berat, (op.qty * op.harga) as subtotal ";
        $sql .= "FROM apm_order_produk AS op ";
        $sql .= "LEFT JOIN apm_produk AS p ";
            $sql .= "ON p.id_produk = op.id_produk ";
        $sql .= "WHERE op.id_order = '".$id_order."' ";
        $query = $this->db->query($sql);
        return $query;
    }

}