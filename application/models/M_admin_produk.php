<?php
class M_admin_produk extends CI_Model {

        private $table = 'apm_admin';
        private $table_produk = 'apm_produk';
        private $table_stock = 'apm_stock';
        private $table_kategori = 'apm_kategori';

        public function resume_login_check($id_admin, $username, $session_id, $browser)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'username' => $username, 'session_id' => $session_id, 'browser' => $browser, 'status' => '1'), 1, 0);
            $row = $query->num_rows();
            return $row;
        }

        public function get_admin_info($id_admin)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'status' => '1'), 1, 0);
            return $query;
        }

        public function add($nama_produk, $id_kategori, $berat, $harga, $created_date, $created_by)
        {
            $this->db->trans_start();
            $data = array(
                'nama_produk' => $nama_produk,
                'id_kategori' => $id_kategori,
                'berat' => $berat,
                'harga' => $harga,
                'status' => '1',
                'created_date' => $created_date,
                'created_by' => $created_by
            );
            
            
            $this->db->insert($this->table_produk, $data);
            $id_produk = $this->db->insert_id();

            $data_stock = array(
                'stock_s' => '0',
                'stock_m' => '0',
                'stock_l' => '0',
                'stock_xl' => '0',
                'stock_xxl' => '0',
                'id_produk' => $id_produk
            );

            $this->db->insert($this->table_stock, $data_stock);
            $this->db->trans_complete();
            return $this->db->trans_status();
        }

        public function get_list_kategori()
        {
            $this->db->where('status', '1');
            $query = $this->db->get($this->table_kategori);
            return $query;
        }

        public function get_produk($keyword)
        {
            $sql = "SELECT p.id_produk, p.nama_produk, k.kategori, p.berat, p.harga ";
            $sql .= "FROM apm_produk AS p ";
            $sql .= "LEFT JOIN apm_kategori AS k ";
                $sql .= "ON k.id_kategori = p.id_kategori ";
            $sql .= "WHERE p.`status` = '1' ";
            if(!empty($keyword)){
                $sql .= "AND ";
                $sql .= "(p.nama_produk LIKE '%".$keyword."%' ";
                $sql .= "OR k.kategori LIKE '%".$keyword."%' ";
                $sql .= "OR p.berat LIKE '%".$keyword."%') ";
            }
            $sql .= "ORDER BY p.id_produk DESC ";
            $query = $this->db->query($sql);
            return $query;
        }

        public function get_produk_by_id($id_produk)
        {
            $sql = "SELECT p.id_produk, p.nama_produk, k.id_kategori, k.kategori, p.berat, p.harga ";
            $sql .= "FROM apm_produk AS p ";
            $sql .= "LEFT JOIN apm_kategori AS k ";
                $sql .= "ON k.id_kategori = p.id_kategori ";
            $sql .= "WHERE p.id_produk = '".$id_produk."' AND p.status = '1'";
            $query = $this->db->query($sql);
            return $query;
        }

        public function update($data, $id_produk)
        {
            $this->db->where('id_produk', $id_produk);
            $this->db->update($this->table_produk, $data);
        }

        public function delete($id_produk)
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('id_produk', $id_produk);
            $query = $this->db->update($this->table_produk, $data);
            if($query){
                return "berhasil";
            }else{
                return "gagal";
            }
        }
}