<?php
class M_admin_kategori extends CI_Model {

        private $table = 'apm_admin';
        private $table_kategori = 'apm_kategori';

        public function resume_login_check($id_admin, $username, $session_id, $browser)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'username' => $username, 'session_id' => $session_id, 'browser' => $browser, 'status' => '1'), 1, 0);
            $row = $query->num_rows();
            return $row;
        }

        public function get_admin_info($id_admin)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'status' => '1'), 1, 0);
            return $query;
        }

        public function add($kategori)
        {
            $data = array('kategori' => $kategori, 'status' => '1');
            $proses_add = $this->db->insert($this->table_kategori, $data);
            return $proses_add;
        }

        public function get_kategori($kategori)
        {
            $this->db->like('kategori', $kategori);
            $this->db->or_like('status', $kategori);
            $this->db->order_by('id_kategori', 'DESC');
            return $this->db->get($this->table_kategori);
        }

        public function get_kategori_by_id($id_kategori)
        {
            $query = $this->db->get_where($this->table_kategori, array('id_kategori' => $id_kategori), 1, 0);
            return $query;
        }

        public function update($data, $id_kategori)
        {
            $this->db->where('id_kategori', $id_kategori);
            $this->db->update($this->table_kategori, $data);
        }

        public function delete($id_kategori)
        {
            $this->db->trans_start();
            $this->db->query("DELETE FROM ".$this->table_kategori." WHERE id_kategori = '".$id_kategori."'");
            $this->db->trans_complete();
            $query = $this->db->trans_status();
            return $query;
        }
}