<?php
class M_admin_rekening extends CI_Model {

        private $table = 'apm_admin';
        private $table_bank = 'apm_bank';

        public function resume_login_check($id_admin, $username, $session_id, $browser)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'username' => $username, 'session_id' => $session_id, 'browser' => $browser, 'status' => '1'), 1, 0);
            $row = $query->num_rows();
            return $row;
        }

        public function get_admin_info($id_admin)
        {
            $query = $this->db->get_where($this->table, array('id_admin' => $id_admin, 'status' => '1'), 1, 0);
            return $query;
        }

        public function add($no_rekening, $nama_bank, $atas_nama)
        {
            $data = array(
                'no_rekening' => $no_rekening,
                'nama_bank' => $nama_bank,
                'atas_nama' => $atas_nama,
                'status' => '1'
            );
            $this->db->trans_start();
            $proses_add = $this->db->insert($this->table_bank, $data);
            $this->db->trans_complete();
            return $this->db->trans_status();
        }

        public function get_rekening($keyword)
        {
            $sql = "SELECT b.id_bank, b.nama_bank, b.no_rekening, b.atas_nama ";
            $sql .= "FROM apm_bank AS b ";
            $sql .= "WHERE b.`status` = '1' ";
            if(!empty($keyword)){
                $sql .= "AND ";
                $sql .= "(b.nama_bank LIKE '%".$keyword."%' ";
                $sql .= "OR b.no_rekening LIKE '%".$keyword."%' ";
                $sql .= "OR b.atas_nama LIKE '%".$keyword."%') ";
            }
            $sql .= "ORDER BY b.id_bank DESC ";
            $query = $this->db->query($sql);
            return $query;
        }

        public function get_rekening_by_id($id_bank)
        {
            $sql = "SELECT b.id_bank, b.nama_bank, b.no_rekening, b.atas_nama ";
            $sql .= "FROM apm_bank AS b ";
            $sql .= "WHERE b.id_bank = '".$id_bank."' AND b.status = '1'";
            $query = $this->db->query($sql);
            return $query;
        }

        public function update($data, $id_bank)
        {
            $this->db->where('id_bank', $id_bank);
            $this->db->update($this->table_bank, $data);
        }

        public function delete($id_bank)
        {
            $data = array(
                'status' => '0'
            );
            $this->db->where('id_bank', $id_bank);
            $query = $this->db->update($this->table_bank, $data);
            if($query){
                return "berhasil";
            }else{
                return "gagal";
            }
        }
}