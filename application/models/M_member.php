<?php
class M_member extends CI_Model {

    private $table = 'apm_member';
    private $table_member_log = 'apm_member_log';
    private $table_member_loyal_log = 'apm_member_loyal_log';
    private $table_order = 'apm_order';
    private $table_order_produk = 'apm_order_produk';

    public function resume_login_check($id_member, $email, $session_id, $browser)
    {
        $query = $this->db->get_where($this->table, array('id_member' => $id_member, 'email' => $email, 'session_id' => $session_id, 'browser' => $browser), 1, 0);
        $row = $query->num_rows();
        return $row;
    }

    public function check_email_login($email)
    {
        $query = $this->db->get_where($this->table, array('email' => $email), 1, 0);
        return $query;
    }

    public function check_pass_login($email, $pass)
    {
        $query = $this->db->get_where($this->table, array('email' => $email), 1, 0);
        $row = $query->num_rows();
        if($row > 0){
                $res = $query->row();
                $pass_hash = $res->pass_hash;
                $check_pass = password_verify($pass, $res->pass_hash);
                
                if($check_pass === TRUE){
                    return '1';
                }else{
                    return '0'; 
                }
                
        }else{
            return '0';
        }
    }

    public function get_member_data($email){
        $this->db->where('email',$email);
        return $this->db->get('apm_member');

    }

    public function get_single_member_data($email)
    {
        $query = $this->db->get_where($this->table, array('email' => $email), 1, 0);
        $row = $query->num_rows();
        if($row > 0){
          $res = $query->row();
          $result['status'] = 200;
          $data['id_member'] = $res->id_member;
          $data['email'] = $res->email;
          $data['nama_lengkap'] = $res->nama_lengkap;
          $data['phone'] = $res->phone;
          $data['birth_place'] = $res->birth_place;
          $data['birth_date'] = $res->birth_date;
          $data['session_id'] = $res->session_id; 
          $data['email_status'] = $res->status; 
          $result['data'] = $data;
        }else{
          $result['status'] = 400;
          $data = [];
          $result['data'] = $data;
        }

        return $result;
    }

    public function get_member_info($id_member)
    {
        $query = $this->db->get_where($this->table, array('id_member' => $id_member, 'status' => '1'), 1, 0);
        return $query;
    }

    public function get_member_loyal_point($id_member)
    {
        $query = $this->db->get_where($this->table_member_loyal_log, array('id_member' => $id_member), 1, 0)->result();
        return $query;
    }

    public function update_member($data, $where)
    {
        $this->db->update($this->table, $data);
        $this->db->where('id_member', $where);
    }

    public function insert_member_log($data)
    {
        $this->db->insert($this->table_member_log, $data);
    }

    public function daftar($email, $pass, $nama_lengkap, $phone, $birth_place, $birth_date, $id_provice, $id_city, $id_district, $agent_browser, $ip, $token, $session_id, $created_date, $alamat, $kode_pos)
    {
        $this->db->trans_start();
        $array_member = array(
            'email' => $email,
            'pass_hash' => $pass,
            'nama_lengkap' => $nama_lengkap,
            'phone' => $phone,
            'alamat' => $alamat,
            'kode_pos' => $kode_pos,
            'birth_place' => $birth_place,
            'birth_date' => $birth_date,
            'id_province' => $id_provice,
            'id_city' => $id_city,
            'id_district' => $id_district,
            'token' => $token,
            'session_id' => $session_id,
            'created_date' => $created_date,
            'status' => '0',
            'browser' => $agent_browser
        );

        $this->db->insert($this->table, $array_member);
        $insert_id = $this->db->insert_id();

        $array_member_log = array(
            'id_member' => $insert_id,
            'action' => 'created',
            'action_date' => $created_date,
            'session_id' => $session_id,
            'browser' => $agent_browser,
            'ip_address' => $ip
        );
        $this->db->insert($this->table_member_log, $array_member_log);

        $array_member_loyal_log = array(
            'id_member' => $insert_id,
            'loyal_point' => '0',
            'updated_date' => $created_date
        );
        $this->db->insert($this->table_member_loyal_log, $array_member_loyal_log);
        $this->db->trans_complete();
    }

    public function check_aktivasi($email, $token)
    {
        $query = $this->db->get_where($this->table, array('email' => $email, 'token' => $token, 'status' => '0'), 1, 0);
        $row = $query->num_rows();
        return $row;
    }

    public function aktivasi($data, $email)
    {
        $this->db->update($this->table, $data);
        $this->db->where('email', $email);
    }

    public function count_total_belanja($id_member)
    {
        $sql = "SELECT SUM(ao.grand_total) AS grand_total FROM apm_order AS ao WHERE ao.id_member = '$id_member' AND ao.status IN ('0','1') ";
        $query = $this->db->query($sql);
        return $query;
    }

    public function count_total_order_success($id_member)
    {
        $sql = "SELECT COUNT(*) AS total_order FROM apm_order AS ao WHERE ao.id_member = '$id_member' AND ao.status = '1'";
        $query = $this->db->query($sql);
        return $query;
    }

    public function count_total_order_pending($id_member)
    {
        $sql = "SELECT COUNT(*) AS total_order FROM apm_order AS ao WHERE ao.id_member = '$id_member' AND ao.status = '0'";
        $query = $this->db->query($sql);
        return $query;
    }

    public function count_total_order_cancel($id_member)
    {
        $sql = "SELECT COUNT(*) AS total_order FROM apm_order AS ao WHERE ao.id_member = '$id_member' AND ao.status = '2'";
        $query = $this->db->query($sql);
        return $query;
    }

    public function last_ten_order_member($id_member)
    {
        $sql =  "SELECT 
                    o.id_order, 
                    o.created_date, 
                    o.no_invoice, 
                    o.grand_total, 
                    o.status,
                    o.status_payment
                FROM apm_order AS o
                WHERE o.id_member = '$id_member'
                ORDER BY o.id_order DESC
                LIMIT 10 ";
        $query = $this->db->query($sql);
        return $query;
    }

    public function save_manage($data_update, $data_update_log, $id_member)
    {
        $this->db->trans_start();
        $this->db->where('id_member', $id_member);
        $this->db->update('apm_member', $data_update);
        $this->db->insert('apm_member_log', $data_update_log);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    public function log_logout($data_logout)
    {
        $this->db->trans_start();
        $this->db->insert('apm_member_log', $data_logout);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    public function ganti_pass($a_hash, $id_member)
    {
        $this->db->trans_start();
        $this->db->where('id_member', $id_member);
        $this->db->update('apm_member', array('pass_hash' => $a_hash));
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    public function ganti_passs($a, $b)
    {
    	$this->db->trans_start();
    	$a_hash = password_hash($a, PASSWORD_BCRYPT);
      $sql = "UPDATE apm_member SET pass_hash = '".$a_hash."' WHERE email = '".$b."' ";
      $query = $this->db->query($sql);
      $this->db->trans_complete();
      return $this->db->trans_status();
    }

    public function update_token($email, $data_update_token)
    {
        $this->db->where('email', $email);
        $this->db->update('apm_member', $data_update_token);
    }

    public function get_detail_order($id_order){
        $this->db->select('*');
        $this->db->where('id_order', $id_order);
        return $this->db->get('apm_order_produk');
    }

}