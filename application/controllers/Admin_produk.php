<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_produk extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin_produk', 'm_admin');
    }

    public function resume_login()
	{
		$id_admin = $this->session->userdata('id_admin');
		$username = $this->session->userdata('username');
		$session_id = $this->session->userdata('session_id');
		$browser = $this->session->userdata('browser');

		$resume_login_check = $this->m_admin->resume_login_check($id_admin, $username, $session_id, $browser);
		if($resume_login_check == 0){
			$this->logout();
		}else{
			return true;
		}
	}

	public function logout()
	{
		$array_unset = array(
			'id_admin',
			'username',
			'nama_lengkap',
			'session_id',
			'browser'
		);
		$this->session->unset_userdata($array_unset);
		$this->session->set_flashdata('temp_sess', 'logout');
		redirect('admin');
	}

    public function template($data = null)
	{
		$id_admin = $this->session->userdata('id_admin');
		$session_id = $this->session->userdata('session_id');

		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$data['nama_lengkap'] = $arr_admin_info[0]->nama_lengkap;
		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$tipe = $arr_admin_info[0]->tipe;
		($tipe == 1) ? $tipe = "Master Admin" : $tipe = "Staff";
		$data['tipe'] = $tipe;

		if(!empty($session_id) && !empty($id_admin)){
			$this->load->view('admin/t_main', $data);
		}else{
			$this->logout();
		}
	}

	public function index()
	{
		$resume_login = $this->resume_login();
		$this->form_validation->set_rules('nama_produk', 'Nama Produk', 'trim|required|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('id_kategori', 'Kategori', 'trim|required');
		$this->form_validation->set_rules('berat', 'Berat', 'trim|required|greater_than_equal_to[100]|less_than_equal_to[99999]|numeric');

		$data['form_attr'] = array('class' => 'form', 'id' => 'produkform', 'method' => 'POST' );

		$data['list_kategori'] = $this->m_admin->get_list_kategori();

		if ($this->form_validation->run() === FALSE)
        {
            $id_admin = $this->session->userdata('id_admin');
			
			$data['page'] = 'produk';
			$data['title'] = 'Produk Admin - Pekgo Apparel';
			$this->template($data);
        }else{
        	$this->add();
        }
		
	}

	public function add()
	{
		$nama_produk = $this->input->post('nama_produk');
		$id_kategori = $this->input->post('id_kategori');
		$berat = $this->input->post('berat');
		$harga = $this->input->post('harga');
		$created_date = date('Y-m-d H:i:s');
		$created_by = $this->session->userdata('id_admin');
		$proses_add = $this->m_admin->add($nama_produk, $id_kategori, $berat, $harga, $created_date, $created_by);
		if($proses_add === true){
            $this->session->set_flashdata('temp_sess', 'successprodukadd');
			redirect('admin_produk');
        }else{
        	$this->session->set_flashdata('temp_sess', 'errorprodukadd');
			redirect('admin_produk');
        }
	}

	public function json_produk()
	{
		// Datatables Variables
		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));

		$requestData = $this->input->get('search');
		if($requestData['value'] != null){
			$keyword = $requestData['value'];
			if($keyword == "show"){
				$keyword = "1";
			}elseif($keyword == "hide"){
				$keyword = "0";
			}
		}else{
			$keyword = "";
		}

		$json_data = $this->m_admin->get_produk($keyword);

		$data = array();
		$no = 0;

		foreach($json_data->result() as $res){
			$no++;
			$data[] = array(
				'no' => $no,
				'id_produk' => $res->id_produk,
				'nama_produk' => $res->nama_produk,
				'kategori' => $res->kategori,
				'berat' => number_format($res->berat,0,',','.'),
				'harga' => number_format($res->harga,0,',','.')
			);
		}

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $json_data->num_rows(),
			"recordsFiltered" => $json_data->num_rows(),
			"data" => $data
		);

		echo json_encode($output);
		exit();
	}

	public function edit()
	{
		$id_produk = $this->input->get('id');
		$data_produk = $this->m_admin->get_produk_by_id($id_produk);
		foreach($data_produk->result() as $res){
			$id_produk = $res->id_produk;
			$nama_produk = $res->nama_produk;
			$id_kategori = $res->id_kategori;
			$kategori = $res->kategori;
			$berat = $res->berat;
			$harga = $res->harga;
		}

		$data = array(
			'id_produk' => $id_produk,
			'nama_produk' => $nama_produk,
			'id_kategori' => $id_kategori,
			'kategori' => $kategori,
			'berat' => $berat,
			'harga' => $harga
		);

		$data['list_kategori'] = $this->m_admin->get_list_kategori();
		$this->load->view('admin/edit_produk', $data);
	}

	public function update()
	{
		$id_produk = $this->input->post('id_produk_edit');
		$nama_produk = $this->input->post('nama_produk_edit');
		$id_kategori = $this->input->post('id_kategori_edit');
		$berat = $this->input->post('berat_edit');
		$harga = $this->input->post('harga');
		$data = array(
			'id_produk' => $id_produk,
			'nama_produk' => $nama_produk,
			'id_kategori' => $id_kategori,
			'berat' => $berat,
			'harga' => $harga
		);

		$proses_update = $this->m_admin->update($data, $id_produk);
	}

	public function delete()
	{
		$id_produk = $this->input->get('id');
		$proses_delete = $this->m_admin->delete($id_produk);
		echo $proses_delete;
	}

}
