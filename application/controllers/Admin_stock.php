<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_stock extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin_stock', 'm_admin');
    }

    public function resume_login()
	{
		$id_admin = $this->session->userdata('id_admin');
		$username = $this->session->userdata('username');
		$session_id = $this->session->userdata('session_id');
		$browser = $this->session->userdata('browser');

		$resume_login_check = $this->m_admin->resume_login_check($id_admin, $username, $session_id, $browser);
		if($resume_login_check == 0){
			$this->logout();
		}else{
			return true;
		}
	}

	public function logout()
	{
		$array_unset = array(
			'id_admin',
			'username',
			'nama_lengkap',
			'session_id',
			'browser'
		);
		$this->session->unset_userdata($array_unset);
		$this->session->set_flashdata('temp_sess', 'logout');
		redirect('admin');
	}

    public function template($data = null)
	{
		$id_admin = $this->session->userdata('id_admin');
		$session_id = $this->session->userdata('session_id');

		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$data['nama_lengkap'] = $arr_admin_info[0]->nama_lengkap;
		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$tipe = $arr_admin_info[0]->tipe;
		($tipe == 1) ? $tipe = "Master Admin" : $tipe = "Staff";
		$data['tipe'] = $tipe;

		if(!empty($session_id) && !empty($id_admin)){
			$this->load->view('admin/t_main', $data);
		}else{
			$this->logout();
		}
	}

	public function index()
	{
		$resume_login = $this->resume_login();
		$this->form_validation->set_rules('stock_s', 'Stock S', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[9999]|numeric');
		$this->form_validation->set_rules('stock_m', 'Stock M', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[9999]|numeric');
		$this->form_validation->set_rules('stock_l', 'Stock L', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[9999]|numeric');
		$this->form_validation->set_rules('stock_xl', 'Stock XL', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[9999]|numeric');
		$this->form_validation->set_rules('stock_xxl', 'Stock XXL', 'trim|required|greater_than_equal_to[0]|less_than_equal_to[9999]|numeric');

		$data['form_attr'] = array('class' => 'form', 'id' => 'stockform', 'method' => 'POST' );

		$data['list_produk'] = $this->m_admin->get_list_produk();

		if ($this->form_validation->run() === FALSE)
        {
            $id_admin = $this->session->userdata('id_admin');
			
			$data['page'] = 'stock';
			$data['title'] = 'Stock Admin - Pekgo Apparel';
			$this->template($data);
        }else{
        	$this->add();
        }
		
	}

	public function add()
	{
		$id_produk = $this->input->post('id_produk');
		$tipe = $this->input->post('tipe'); // action
		$stock_s = $this->input->post('stock_s');
		$stock_m = $this->input->post('stock_m');
		$stock_l = $this->input->post('stock_l');
		$stock_xl = $this->input->post('stock_xl');
		$stock_xxl = $this->input->post('stock_xxl');
		$action_date = date('Y-m-d H:i:s');
		$id_admin = $this->session->userdata('id_admin');

		$proses_add = $this->m_admin->add($id_produk, $tipe, $stock_s, $stock_m, $stock_l, $stock_xl, $stock_xxl, $action_date, $id_admin);
		if($proses_add === true){
            $this->session->set_flashdata('temp_sess', 'successstockadd');
			redirect('admin_stock');
        }else{
        	$this->session->set_flashdata('temp_sess', 'errorstockadd');
			redirect('admin_stock');
        }
	}

	public function json_stock()
	{
		// Datatables Variables
		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));

		$requestData = $this->input->get('search');
		if($requestData['value'] != null){
			$keyword = $requestData['value'];
		}else{
			$keyword = "";
		}

		$json_data = $this->m_admin->get_stock($keyword);

		$data = array();
		$no = 0;

		foreach($json_data->result() as $res){
			$no++;
			$data[] = array(
				'no' => $no,
				'nama_produk' => $res->nama_produk,
				'stock_s' => number_format($res->stock_s,0,',','.'),
				'stock_m' => number_format($res->stock_m,0,',','.'),
				'stock_l' => number_format($res->stock_l,0,',','.'),
				'stock_xl' => number_format($res->stock_xl,0,',','.'),
				'stock_xxl' => number_format($res->stock_xxl,0,',','.')
			);
		}

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $json_data->num_rows(),
			"recordsFiltered" => $json_data->num_rows(),
			"data" => $data
		);

		echo json_encode($output);
		exit();
	}

}
