<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_order_pending extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin_order_pending', 'm_admin');
    }

    public function resume_login()
	{
		$id_admin = $this->session->userdata('id_admin');
		$username = $this->session->userdata('username');
		$session_id = $this->session->userdata('session_id');
		$browser = $this->session->userdata('browser');

		$resume_login_check = $this->m_admin->resume_login_check($id_admin, $username, $session_id, $browser);
		if($resume_login_check == 0){
			$this->logout();
		}else{
			return true;
		}
	}

	public function logout()
	{
		$array_unset = array(
			'id_admin',
			'username',
			'nama_lengkap',
			'session_id',
			'browser'
		);
		$this->session->unset_userdata($array_unset);
		$this->session->set_flashdata('temp_sess', 'logout');
		redirect('admin');
	}

    public function template($data = null)
	{
		$id_admin = $this->session->userdata('id_admin');
		$session_id = $this->session->userdata('session_id');

		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$data['nama_lengkap'] = $arr_admin_info[0]->nama_lengkap;
		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$tipe = $arr_admin_info[0]->tipe;
		($tipe == 1) ? $tipe = "Master Admin" : $tipe = "Staff";
		$data['tipe'] = $tipe;

		if(!empty($session_id) && !empty($id_admin)){
			$this->load->view('admin/t_main', $data);
		}else{
			$this->logout();
		}
	}

	public function index()
	{
		$resume_login = $this->resume_login();
		$id_admin = $this->session->userdata('id_admin');
			
		$data['page'] = 'order_pending';
		$data['title'] = 'Order Pending - Pekgo Apparel';
		$this->template($data);
		
	}

	public function json_order_pending()
	{
		// Datatables Variables
		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));

		$requestData = $this->input->get('search');
		if($requestData['value'] != null){
			$keyword = $requestData['value'];
		}else{
			$keyword = "";
		}

		$json_data = $this->m_admin->get_order_pending();
		$json_data2 = $this->m_admin->get_order_pending2($keyword);

		$data = array();
		$no = 0;

		foreach($json_data2->result() as $res){
			$no++;

			$arr_admin_info = $this->m_admin->get_admin_info($res->created_by)->result();
			$created_by = $arr_admin_info[0]->nama_lengkap;

			$data[] = array(
				'no' => $no,
				'id_order' => $res->id_order,
				'nama_lengkap' => $res->nama_lengkap,
				'no_invoice' => $res->no_invoice,
				'sub_total' => number_format($res->sub_total,0,',', '.'),
				'ongkir' => number_format($res->ongkir,0,',', '.'),
				'tambahan' => number_format($res->tambahan,0,',', '.'),
				'kode_unik' => number_format($res->kode_unik,0,',', '.'),
				'grand_total' => number_format($res->grand_total,0,',', '.'),
				'ekspedisi' => $res->ekspedisi,
				'status' => $res->status,
				'created_date' => $res->created_date,
				'updated_date' => $res->updated_date,
				'created_by' => $created_by,
				'status_payment' => $res->status_payment,
				'status_print' => $res->status_print
			);
		}

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $json_data->num_rows(),
			"recordsFiltered" => $json_data2->num_rows(),
			"data" => $data
		);

		echo json_encode($output);
		exit();
	}

	public function edit()
	{
		$id_order = $this->input->post('id_order');
		$data['arr_order'] = $this->m_admin->get_order_information($id_order);
		$data['arr_customer'] = $this->m_admin->get_customer_information($id_order);
		$data['arr_produk'] = $this->m_admin->get_produk_information($id_order);
		$this->load->view('admin/edit_order_pending.php', $data);
	}

	public function confirm()
	{
		$data['id_order'] = $this->input->post('id_order');
		$data['arr_customer'] = $this->m_admin->get_customer_information($data['id_order']);
		$data['arr_bank'] = $this->m_admin->get_bank_information();
		$data['id_admin'] = $this->session->userdata('id_admin');
		$this->load->view('admin/confirm_order_pending.php', $data);
	}

	public function detail()
	{
		$id_order = $this->input->get('id_order');
		$data['arr_order'] = $this->m_admin->get_order_information($id_order);
		$data['arr_customer'] = $this->m_admin->get_customer_information($id_order);
		$data['arr_produk'] = $this->m_admin->get_produk_information($id_order);
		$this->load->view('admin/detail_order_pending.php', $data);
	}

	public function save_confirm()
	{
		$id_member = $this->input->post('id_member_conf');
		$id_order = $this->input->post('id_order_conf');
		$created_date = date('Y-m-d H:i:s');
		$status = '1';
		$id_bank = $this->input->post('id_bank_conf');
		$updated_date = date('Y-m-d H:i:s');
		$updated_by = $this->input->post('id_admin_conf');
		$save_confirm_process = $this->m_admin->save_confirm($id_member, $id_order, $created_date, $status, $id_bank, $updated_date, $updated_by);
		if($save_confirm_process == true){
			$this->session->set_flashdata('temp_sess', 'confirmpaymentsuccess');
			redirect('admin_order_pending');
		}else{
			$this->session->set_flashdata('temp_sess', 'confirmpaymentfailed');
			redirect('admin_order_pending');
		}
	}

}
