<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_rekening extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin_rekening', 'm_admin');
    }

    public function resume_login()
	{
		$id_admin = $this->session->userdata('id_admin');
		$username = $this->session->userdata('username');
		$session_id = $this->session->userdata('session_id');
		$browser = $this->session->userdata('browser');

		$resume_login_check = $this->m_admin->resume_login_check($id_admin, $username, $session_id, $browser);
		if($resume_login_check == 0){
			$this->logout();
		}else{
			return true;
		}
	}

	public function logout()
	{
		$array_unset = array(
			'id_admin',
			'username',
			'nama_lengkap',
			'session_id',
			'browser'
		);
		$this->session->unset_userdata($array_unset);
		$this->session->set_flashdata('temp_sess', 'logout');
		redirect('admin');
	}

    public function template($data = null)
	{
		$id_admin = $this->session->userdata('id_admin');
		$session_id = $this->session->userdata('session_id');

		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$data['nama_lengkap'] = $arr_admin_info[0]->nama_lengkap;
		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$tipe = $arr_admin_info[0]->tipe;
		($tipe == 1) ? $tipe = "Master Admin" : $tipe = "Staff";
		$data['tipe'] = $tipe;

		if(!empty($session_id) && !empty($id_admin)){
			$this->load->view('admin/t_main', $data);
		}else{
			$this->logout();
		}
	}

	public function index()
	{
		$resume_login = $this->resume_login();
		$this->form_validation->set_rules('no_rekening', 'No Rekening', 'trim|required|min_length[5]|max_length[20]|numeric|is_unique[apm_bank.no_rekening]');
		$this->form_validation->set_rules('nama_bank', 'Nama Bank', 'trim|required|min_length[5]|max_length[50]|alpha_numeric_spaces');
		$this->form_validation->set_rules('atas_nama', 'atas_nama', 'trim|required|min_length[3]|max_length[50]|alpha_numeric_spaces');

		$data['form_attr'] = array('class' => 'form', 'id' => 'rekeningform', 'method' => 'POST' );

		if ($this->form_validation->run() === FALSE)
        {
            $id_admin = $this->session->userdata('id_admin');
			
			$data['page'] = 'rekening';
			$data['title'] = 'Setting No Rekening - Pekgo Apparel';
			$this->template($data);
        }else{
        	$this->add();
        }
		
	}

	public function add()
	{
		$no_rekening = $this->input->post('no_rekening');
		$nama_bank = $this->input->post('nama_bank');
		$atas_nama = $this->input->post('atas_nama');
		$proses_add = $this->m_admin->add($no_rekening, $nama_bank, $atas_nama);
		if($proses_add === true){
            $this->session->set_flashdata('temp_sess', 'successbankadd');
			redirect('admin_rekening');
        }else{
        	$this->session->set_flashdata('temp_sess', 'errorbankadd');
			redirect('admin_rekening');
        }
	}

	public function json_rekening()
	{
		// Datatables Variables
		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));

		$requestData = $this->input->get('search');
		if($requestData['value'] != null){
			$keyword = $requestData['value'];
		}else{
			$keyword = "";
		}

		$json_data = $this->m_admin->get_rekening($keyword);

		$data = array();
		$no = 0;

		foreach($json_data->result() as $res){
			$no++;
			$data[] = array(
				'no' => $no,
				'id_bank' => $res->id_bank,
				'nama_bank' => $res->nama_bank,
				'no_rekening' => $res->no_rekening,
				'atas_nama' => $res->atas_nama
			);
		}

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $json_data->num_rows(),
			"recordsFiltered" => $json_data->num_rows(),
			"data" => $data
		);

		echo json_encode($output);
		exit();
	}

	public function edit()
	{
		$id_bank = $this->input->get('id');
		$data_rekening = $this->m_admin->get_rekening_by_id($id_bank);
		foreach($data_rekening->result() as $res){
			$id_bank = $res->id_bank;
			$nama_bank = $res->nama_bank;
			$no_rekening = $res->no_rekening;
			$atas_nama = $res->atas_nama;
		}

		$data = array(
			'id_bank' => $id_bank,
			'nama_bank' => $nama_bank,
			'no_rekening' => $no_rekening,
			'atas_nama' => $atas_nama
		);

		$this->load->view('admin/edit_rekening', $data);
	}

	public function update()
	{
		$id_bank = $this->input->post('id_bank_edit');
		$nama_bank = $this->input->post('nama_bank_edit');
		$no_rekening = $this->input->post('no_rekening_edit');
		$atas_nama = $this->input->post('atas_nama_edit');
		$data = array(
			'id_bank' => $id_bank,
			'nama_bank' => $nama_bank,
			'no_rekening' => $no_rekening,
			'atas_nama' => $atas_nama
		);

		$proses_update = $this->m_admin->update($data, $id_bank);
	}

	public function delete()
	{
		$id_bank = $this->input->get('id');
		$proses_delete = $this->m_admin->delete($id_bank);
		echo $proses_delete;
	}

}
