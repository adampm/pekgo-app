<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin', 'm_admin');
    }

	public function index()
	{
		if(!empty($this->session->userdata('id_admin'))){
			$resume_login = $this->resume_login();
			if($resume_login == true){
				$this->session->set_flashdata('temp_sess', 'firstlogin');
				redirect('admin/dashboard');
			}
		}else{
			$this->form_validation->set_rules('apm_username_inp', 'Username', 'trim|required|min_length[4]|max_length[20]');
	        $this->form_validation->set_rules('apm_pass_inp', 'Password', 'trim|required|min_length[5]|max_length[60]');

	        $data['agent_browser'] = $this->agent->browser(TRUE);
	        $data['agent_version'] = $this->agent->version(TRUE);

	        if($this->agent->browser(TRUE) == 'Chrome'){
				$data['browser_icon'] = 'chrome';
			}elseif($this->agent->browser(TRUE) == 'Firefox'){
				$data['browser_icon'] = 'firefox';
			}elseif($this->agent->browser(TRUE) == 'Safari'){
				$data['browser_icon'] = 'safari';
			}elseif($this->agent->browser(TRUE) == 'Edge'){
				$data['browser_icon'] = 'edge';
			}else{
				$data['browser_icon'] = 'question-circle';
			}

			$data['ip'] = $this->input->ip_address();
			$data['form_attr'] = array('class' => 'form-signin', 'id' => 'loginform', 'method' => 'POST' );
			$data['username_labl_attr'] = array('class' => 'sr-only');

			$data['pass_labl_attr'] = array('class' => 'sr-only');

			$sv_pass = set_value('apm_pass_inp') == true ? set_value('apm_pass_inp') : "";
			$data['pass_attr'] = array(
				'class' => 'form-control', 
				'id' => 'id_pass_inp',
				'name' => 'apm_pass_inp',
				'value' => $sv_pass,
				'maxlength' => '50',
				'placeholder' => 'Password'
			);

			$data['submit_attr'] = array(
				'class' => 'btn btn-lg btn-primary btn-block', 
				'value' => 'Log in'
			);

	        if ($this->form_validation->run() === FALSE)
	        {
	            $this->load->view('admin/login', $data);
	        }
	        else
	        {
	        	$username = $this->input->post('apm_username_inp');
				$pass = $this->input->post('apm_pass_inp');
	        	$this->auth($username, $pass);
	        }
		}
	}

	public function resume_login()
	{
		$id_admin = $this->session->userdata('id_admin');
		$username = $this->session->userdata('username');
		$session_id = $this->session->userdata('session_id');
		$browser = $this->session->userdata('browser');

		$resume_login_check = $this->m_admin->resume_login_check($id_admin, $username, $session_id, $browser);
		if($resume_login_check == 0){
			$this->logout();
		}else{
			return true;
		}
	}

	public function template($data = null)
	{
		$id_admin = $this->session->userdata('id_admin');
		$session_id = $this->session->userdata('session_id');

		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$data['nama_lengkap'] = $arr_admin_info[0]->nama_lengkap;
		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$tipe = $arr_admin_info[0]->tipe;
		($tipe == 1) ? $tipe = "Master Admin" : $tipe = "Staff";
		$data['tipe'] = $tipe;

		if(!empty($session_id) && !empty($id_admin)){
			$this->load->view('admin/t_main', $data);
		}else{
			$this->logout();
		}
	}

	public function auth($username, $pass)
	{
		$check_username = $this->m_admin->check_username_login($username);
		$check_pass = $this->m_admin->check_pass_login($username, $pass);
		
		if($check_username == 0){
			$this->session->set_flashdata('temp_sess', 'username');
			redirect('admin');
		}elseif($check_pass == 0){
			$this->session->set_flashdata('temp_sess', 'pass');
			redirect('admin');
		}else{
			$arr_admin_data = $this->m_admin->get_single_admin_data($username);
			$status = array_values($arr_admin_data)[0];
			if($status == 200){
				foreach($arr_admin_data['data'] as $key => $val){
                    $id_admin = $arr_admin_data['data']['id_admin'];
                    $username = $arr_admin_data['data']['username'];
                    $nama_lengkap = $arr_admin_data['data']['nama_lengkap'];
                    $session_id = $arr_admin_data['data']['session_id'];
                    $username_status = $arr_admin_data['data']['username_status'];
				}

				if($username_status == 0){
					$this->session->set_flashdata('temp_sess', 'username');
					redirect('admin');
				}else{
					$data_admin['session_id'] = session_id();
					$data_admin['updated_date'] = date('Y-m-d');
		            $data_admin['browser'] = $this->agent->browser();
		            $this->m_admin->update_admin($data_admin, $id_admin);

		            $data_log['id_admin'] = $id_admin;
		            $data_log['action'] = 'login';
		            $data_log['action_date'] = date('Y-m-d');
		            $data_log['browser'] = $this->agent->browser();
		            $data_log['ip_address'] = $this->input->ip_address();
		            $this->m_admin->insert_admin_log($data_log);

					$this->session->set_userdata($data_sess);
					$this->session->set_flashdata('temp_sess', 'firstlogin');

					$data_sess['id_admin'] = $id_admin;
					$data_sess['username'] = $username;
					$data_sess['nama_lengkap'] = $nama_lengkap;
					$data_sess['session_id'] = session_id();
					$data_sess['browser'] = $this->agent->browser();
					$this->session->set_userdata($data_sess);
					redirect('admin/dashboard');
				}

			}else{
				$this->session->set_flashdata('temp_sess', 'username');
				redirect('admin');
			}
		}
	}

	public function dashboard()
	{
		$resume_login = $this->resume_login();
		$id_admin = $this->session->userdata('id_admin');

		$data['page'] = 'dashboard';
		$data['title'] = 'Dashboard Admin - Pekgo Apparel';
		$this->template($data);
	}

	public function registrasi()
	{
		$this->form_validation->set_rules('apm_username', 'Username','trim|required|min_length[5]|max_length[100]|valid_username|is_unique[apm_admin.username]'
		);
        $this->form_validation->set_rules('apm_password', 'Password', 'trim|required|min_length[5]|max_length[60]');
        $this->form_validation->set_rules('apm_repassword', 'Confirm Password', 'trim|required|min_length[5]|max_length[60]|matches[apm_password]');
        $this->form_validation->set_rules('apm_nama_lengkap', 'Nama Lengkap', 'trim|required|min_length[3]|max_length[50]|alpha_numeric_spaces');
        $this->form_validation->set_rules('apm_phone', 'Phone', 'trim|required|min_length[9]|max_length[20]|numeric');
        $this->form_validation->set_rules('apm_birth_place', 'Kota Kelahiran', 'trim|required|min_length[5]|max_length[50]|alpha');
        $this->form_validation->set_rules('apm_birth_date', 'Tanggal Lahir', 'trim|required|exact_length[10]');

		$data['form_attr'] = array('class' => 'form-signin', 'id' => 'loginform', 'method' => 'POST' );

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/registrasi', $data);
        }
        else
        {
        	$username = $this->input->post('apm_username');
			$pass = $this->input->post('apm_pass');
			$pass = password_hash($pass, PASSWORD_BCRYPT);
			$nama_lengkap = $this->input->post('apm_nama_lengkap');
			$phone = $this->input->post('apm_phone');
			$birth_place = $this->input->post('apm_birth_place');
			$birth_date = $this->input->post('apm_birth_date');
			$birth_date = date('Y-m-d', strtotime($birth_date));
			$id_provice = $this->input->post('apm_province');
			$id_city = $this->input->post('apm_city');
			$id_district = $this->input->post('apm_district');
			$agent_browser = $this->agent->browser(TRUE);
	        $ip = $this->input->ip_address();
	        $token = md5($username);
	        $session_id = session_id();
	        $created_date = date('Y-m-d H:i:s');
			$proses_daftar = $this->m_admin->daftar($username, $pass, $nama_lengkap, $phone, $birth_place, $birth_date, $id_provice, $id_city, $id_district, $agent_browser, $ip, $token, $session_id, $created_date);

        	// SEND EMAIL //
        	$data_username['username'] = $username;
			$data_username['nama_lengkap'] = $nama_lengkap;
			$data_username['token'] = $token;

        	$config = 	Array(
								'protocol' => 'smtp',
						        'smtp_host' => 'ssl://mail.pekgo-app.com',
						        'smtp_port' => '465',
						        'smtp_user' => 'noreply@pekgo-app.com',
						        'smtp_pass' => 'PKG20181478963',
						        'smtp_crypto' => 'ssl',
						        'mailtype'  => 'html', 
						        'charset' => 'iso-8859-1',
						        'wordwrap' => TRUE,
						        'starttls'  => TRUE
						);
        	$this->load->library('username');
        	$this->username->from('noreply@pekgo-app.com', 'System No Reply - Pekgo Apparel', 'noreply@pekgo-app.com');
			$this->username->to($username);
			$this->username->cc('');
			$this->username->bcc('adam@pekgo-app.com');

			$this->username->subject('Activation Account - Pekgo Apparel');
			$this->username->message($this->load->view('admin/username_activation', $data_username, true));

			if($this->username->send()){
				$this->registrasi_berhasil();
			}else{
				$this->username->print_debugger();
				//$this->registrasi_gagal();
			}
        }
	}

	public function registrasi_berhasil()
	{
		$this->session->set_flashdata('temp_sess', 'success');
		redirect('admin/registrasi');
	}

	public function registrasi_gagal()
	{
		$this->session->set_flashdata('temp_sess', 'error');
		redirect('admin/registrasi');
	}

	public function logout()
	{
		$array_unset = array(
			'id_admin',
			'username',
			'nama_lengkap',
			'session_id',
			'browser'
		);
		$this->session->unset_userdata($array_unset);
		$this->session->set_flashdata('temp_sess', 'logout');
		redirect('admin');
	}

	public function json_province()
	{
		$json_province = $this->rajaongkir->province();
		echo $json_province;
	}

	public function json_city()
	{
		$id_province = $this->input->post('id_province');
		$json_cities = $this->rajaongkir->city($id_province);
		echo $json_cities;
	}

	public function json_subdistrict()
	{
		$id_city = $this->input->post('id_city');
		$json_district = $this->rajaongkir->subdistrict($id_city);
		echo $json_district;
	}

	public function test()
	{
    	// SEND EMAIL //
    	$username = 'adam.pm77@gmail.com';
    	$data_username['username'] = $username;
		$data_username['nama_lengkap'] = 'adam';
		$data_username['token'] = '123456789';

    	$config = 	Array(
							'protocol' => 'smtp',
					        'smtp_host' => 'ssl://mail.pekgo-app.com',
					        'smtp_port' => '465',
					        'smtp_user' => 'noreply@pekgo-app.com',
					        'smtp_pass' => 'PKG20181478963',
					        'smtp_crypto' => 'ssl',
					        'mailtype'  => 'html', 
					        'charset' => 'utf-8',
					        'wordwrap' => TRUE,
					        'starttls'  => TRUE
					);
    	$this->load->library('username', $config);
    	//$this->username->initialize($config);
    	$this->username->from('noreply@pekgo-app.com', 'System No Reply - Pekgo Apparel', 'noreply@pekgo-app.com');
		$this->username->to($username);
		$this->username->cc('');
		$this->username->bcc('adam@pekgo-app.com');

		$this->username->subject('Activation Account - Pekgo Apparel');
		$this->username->message($this->load->view('admin/username_activation', $data_username, true));

		if($this->username->send()){
			$this->registrasi_berhasil();
		}else{
			$this->username->print_debugger();
			//$this->registrasi_gagal();
		}
	}

	public function aktivasi_code()
	{
		$username = urldecode($this->uri->segment(3));
		$token = urldecode($this->uri->segment(4));
		$check_username = $this->m_admin->check_username_login($username);
		if($check_username == 0){
			$this->session->set_flashdata('temp_sess', 'username');
			redirect('admin');
		}else{
			$check_aktivasi = $this->m_admin->check_aktivasi($username, $token);
			if($check_aktivasi == 0){
				$this->session->set_flashdata('temp_sess', 'wrongtoken');
				redirect('admin');
			}else{
				$data_aktivasi['status'] = '1';
				$proses_aktivasi = $this->m_admin->aktivasi($data_aktivasi, $username);
				$this->session->set_flashdata('temp_sess', 'activationok');
				redirect('admin');
			}
		}
	}

}
