<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_new_order extends CI_Controller {

	private $kode_unik = '';
	public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin_new_order', 'm_admin');
        $this->load->library('rajaongkir');
    }

    public function resume_login()
	{
		$id_admin = $this->session->userdata('id_admin');
		$username = $this->session->userdata('username');
		$session_id = $this->session->userdata('session_id');
		$browser = $this->session->userdata('browser');

		$resume_login_check = $this->m_admin->resume_login_check($id_admin, $username, $session_id, $browser);
		if($resume_login_check == 0){
			$this->logout();
		}else{
			return true;
		}
	}

	public function logout()
	{
		$array_unset = array(
			'id_admin',
			'username',
			'nama_lengkap',
			'session_id',
			'browser'
		);
		$this->session->unset_userdata($array_unset);
		$this->session->set_flashdata('temp_sess', 'logout');
		redirect('admin');
	}

    public function template($data = null)
	{
		$id_admin = $this->session->userdata('id_admin');
		$session_id = $this->session->userdata('session_id');

		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$data['nama_lengkap'] = $arr_admin_info[0]->nama_lengkap;
		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$tipe = $arr_admin_info[0]->tipe;
		($tipe == 1) ? $tipe = "Master Admin" : $tipe = "Staff";
		$data['tipe'] = $tipe;

		if(!empty($session_id) && !empty($id_admin)){
			$this->load->view('admin/t_main', $data);
		}else{
			$this->logout();
		}
	}

	public function index()
	{
		$resume_login = $this->resume_login();
		$id_admin = $this->session->userdata('id_admin');
		$data['form_attr'] = array('class' => 'form', 'id' => 'neworderform', 'method' => 'POST' );

		$data['page'] = 'new_order';
		$data['title'] = 'Bikin Order Baru - Pekgo Apparel';
		$this->template($data);
	}

	public function load_produk()
	{
		$this->load->view('admin/load_produk');
	}

	public function load_produk_footer()
	{
		$data['ongkir'] = $this->input->post('ong');
		$data['tambahan'] = $this->input->post('tam');
		$data['catatan'] = $this->input->post('cat');
		$this->load->view('admin/load_produk_footer', $data);
	}

	public function add()
	{
		$id_produk = $this->input->post('id_produk');
		$size = $this->input->post('size');
		$arr_produk = $this->m_admin->get_produk_info($id_produk);
		foreach($arr_produk->result() as $res){
			$harga = $res->harga;
			$nama_produk = $res->nama_produk;
			$berat = $res->berat;
		}
		
		$qty = $this->input->post('qty');
		$data = array(
			'id' => $id_produk.'_'.$size,
			'qty' => $qty,
			'price' => $harga,
			'name' => $nama_produk,
			'size' => $size,
			'berat' => $berat
		);
		$this->cart->insert($data);
		echo "berhasil";
	}

	public function remove()
	{
		$id_cart = $this->input->post('id_cart');
		$proses_remove = $this->cart->remove($id_cart);
		if($proses_remove){
			echo "berhasil";
		}else{
			echo "gagal";
		}
	}

	public function json_member()
	{
		$json_data = $this->m_admin->get_member();
		foreach($json_data->result() as $res){
			$data[] = array(
				'id' => $res->id_member,
				'text' => $res->nama_lengkap.' - '.$res->phone
			);
		}
		echo json_encode($data);
		exit();
	}

	public function json_customer_information()
	{
		$id_member = $this->input->get('id_member');
		$arr_customer = $this->m_admin->get_member_info($id_member)->result();
		echo json_encode($arr_customer);
		exit();
	}

	public function json_produk()
	{
		if(empty($_GET['q'])){
			$q = null;
		}else{
			$q = $_GET['q'];
		}
		$json_data = $this->m_admin->get_produk($q);
		foreach($json_data->result() as $res){
			$data[] = array(
				'id' => $res->id_produk,
				'text' => $res->nama_produk
			);
		}
		echo json_encode($data);
		exit();
	}

	public function json_ekspedisi()
	{
		$id_member = $this->input->get('id_member');
		$arr_member =$this->m_admin->get_member_info($id_member);
		foreach($arr_member->result() as $res){
			$id_district_member = $res->id_district;
		}

		$json_data = $this->rajaongkir->cost('2106', $id_district_member, '100', 'sicepat');
		$json_cost = json_decode($json_data);

		foreach($json_cost->rajaongkir->results[0]->costs as $res){
			$data[] = array(
				'id' => $res->cost[0]->value.'|'.$res->service,
				'text' => $res->service
			);		
		}

		echo json_encode($data);
		exit();
	}

	public function create_order()
	{
		$sub_total = 0;
		$total_data = 0;
		foreach($this->cart->contents() as $res){
			$sub_total += $res['subtotal'];
			$total_data += $res['qty'];
		}
		$data['catatan'] = $this->input->post('hidden_catatan');
		$data['tambahan'] = $this->input->post('hidden_tambahan');
		$data['id_member'] = $this->input->post('id_member_new_order');
		$data['no_invoice'] = $this->generate_invoice();
		$data['sub_total'] = $sub_total;
		$data['ongkir'] = $this->input->post('hidden_ongkir');
		$data['kode_unik'] = $this->kode_unik;
		$data['grand_total'] = $data['sub_total'] + $data['ongkir'] + $data['kode_unik']+ $data['tambahan'];
		$data['total_berat'] = $this->input->post('hidden_berat');
		$data['ekspedisi'] = $this->input->post('hidden_ekspedisi');
		$data['status'] = '0';
		$data['session_id'] = session_id();
		$data['created_date'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata('id_admin');
		$data['status_print'] = '0';
		$data['waktu_batasan'] = $this->input->post('waktu_batasan_new_order');
		$total_check_stock = $this->m_admin->check_stock();

		if($total_data == $total_check_stock){
			$proses_create = $this->m_admin->create_new_order($data);
			$this->session->set_flashdata('temp_sess', 'createorderberhasil');
			$this->cart->destroy();
			redirect('admin_new_order');
		}else{
			$this->session->set_flashdata('temp_sess', 'stockprodukorderhabis');
			redirect('admin_new_order');
		}

	}

	public function generate_invoice()
	{
		$m_rom = $this->generate_month_romawi();
		$cur_year = date('Y');
		$kode_unik = $this->m_admin->get_kode_unik();
		$this->kode_unik = $kode_unik;
		if($kode_unik < 10){
			$kode_unik = '000'.$kode_unik;
		}elseif($kode_unik < 100 ){
			$kode_unik = '00'.$kode_unik;
		}elseif($kode_unik < 1000 ){
			$kode_unik = '0'.$kode_unik;
		}elseif($kode_unik < 10000 ){
			$kode_unik = $kode_unik;
		}
		$no_invoice = 'INV/'.$m_rom.'/'.$cur_year.'/'.$kode_unik;
		return $no_invoice;
	}

	public function generate_month_romawi()
	{
		$cur_month = date('m');
		if($cur_month == '1'){
			$m_rom = 'I';
		}elseif($cur_month == '2'){
			$m_rom = 'II';
		}elseif($cur_month == '3'){
			$m_rom = 'III';
		}elseif($cur_month == '4'){
			$m_rom = 'IV';
		}elseif($cur_month == '5'){
			$m_rom = 'V';
		}elseif($cur_month == '6'){
			$m_rom = 'VI';
		}elseif($cur_month == '7'){
			$m_rom = 'VII';
		}elseif($cur_month == '8'){
			$m_rom = 'VIII';
		}elseif($cur_month == '9'){
			$m_rom = 'IX';
		}elseif($cur_month == '10'){
			$m_rom = 'X';
		}elseif($cur_month == '11'){
			$m_rom = 'XI';
		}elseif($cur_month == '12'){
			$m_rom = 'XII';
		}

		return $m_rom;
	}

}
