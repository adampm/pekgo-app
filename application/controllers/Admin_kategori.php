<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_kategori extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin_kategori', 'm_admin');
    }

    public function resume_login()
	{
		$id_admin = $this->session->userdata('id_admin');
		$username = $this->session->userdata('username');
		$session_id = $this->session->userdata('session_id');
		$browser = $this->session->userdata('browser');

		$resume_login_check = $this->m_admin->resume_login_check($id_admin, $username, $session_id, $browser);
		if($resume_login_check == 0){
			$this->logout();
		}else{
			return true;
		}
	}

	public function logout()
	{
		$array_unset = array(
			'id_admin',
			'username',
			'nama_lengkap',
			'session_id',
			'browser'
		);
		$this->session->unset_userdata($array_unset);
		$this->session->set_flashdata('temp_sess', 'logout');
		redirect('admin');
	}

    public function template($data = null)
	{
		$id_admin = $this->session->userdata('id_admin');
		$session_id = $this->session->userdata('session_id');

		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$data['nama_lengkap'] = $arr_admin_info[0]->nama_lengkap;
		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$tipe = $arr_admin_info[0]->tipe;
		($tipe == 1) ? $tipe = "Master Admin" : $tipe = "Staff";
		$data['tipe'] = $tipe;

		if(!empty($session_id) && !empty($id_admin)){
			$this->load->view('admin/t_main', $data);
		}else{
			$this->logout();
		}
	}

	public function index()
	{
		$resume_login = $this->resume_login();
		$id_admin = $this->session->userdata('id_admin');
		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|min_length[3]|max_length[50]');
		$data['form_attr'] = array('class' => 'form', 'id' => 'kategoriform', 'method' => 'POST' );

		$data['page'] = 'kategori';
		$data['title'] = 'Kategori Admin - Pekgo Apparel';
		$this->template($data);
	}

	public function add()
	{
		$kategori = $this->input->post('kategori');
		$proses_add = $this->m_admin->add($kategori);
		if($proses_add === true){
            $this->session->set_flashdata('temp_sess', 'successkategoriadd');
			redirect('admin_kategori');
        }else{
        	$this->session->set_flashdata('temp_sess', 'errorkategoriadd');
			redirect('admin_kategori');
        }
	}

	public function json_kategori()
	{
		// Datatables Variables
		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));

		$requestData = $this->input->get('search');
		if($requestData['value'] != null){
			$keyword = $requestData['value'];
			if($keyword == "show"){
				$keyword = "1";
			}elseif($keyword == "hide"){
				$keyword = "0";
			}
		}else{
			$keyword = "";
		}

		$json_data = $this->m_admin->get_kategori($keyword);

		$data = array();
		$no = 0;

		foreach($json_data->result() as $res){
			$no++;
			$data[] = array(
				'no' => $no,
				'id_kategori' => $res->id_kategori,
				'kategori' => $res->kategori,
				'status' => $res->status
			);
		}

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $json_data->num_rows(),
			"recordsFiltered" => $json_data->num_rows(),
			"data" => $data
		);

		echo json_encode($output);
		exit();
	}

	public function edit()
	{
		$id_kategori = $this->input->get('id');
		$data_kategori = $this->m_admin->get_kategori_by_id($id_kategori);
		foreach($data_kategori->result() as $res){
			$kategori = $res->kategori;
			$status = $res->status;
		}

		if($status == 1){
			$selected1 = 'selected';
			$selected0 = '';
		}else{
			$selected1 = '';
			$selected0 = 'selected';
		}

		$data = array(
			'id_kategori' => $id_kategori,
			'kategori' => $kategori,
			'selected1' => $selected1,
			'selected0' => $selected0
		);

		$this->form_validation->set_rules('kategori', 'Kategori', 'trim|required|min_length[3]|max_length[50]');
		$data['form_attr'] = array('class' => 'form', 'id' => 'edit_form', 'method' => 'POST' );
		$this->load->view('admin/edit_kategori', $data);
	}

	public function update()
	{
		$id_kategori = $this->input->post('id_kategori');
		$kategori = $this->input->post('kategori');
		$status = $this->input->post('status');
		$data = array(
			'id_kategori' => $id_kategori,
			'kategori' => $kategori,
			'status' => $status
		);

		$proses_update = $this->m_admin->update($data, $id_kategori);
	}

	public function delete()
	{
		$id_kategori = $this->input->get('id');
		$proses_delete = $this->m_admin->delete($id_kategori);
		if($proses_delete === FALSE){
			echo "gagal"; 
		}else{
			echo "berhasil";
		}
	}

}
