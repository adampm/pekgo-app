<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('rajaongkir');
        $this->load->model('M_member', 'm_member');
    }

	public function index()
	{
		if(!empty($this->session->userdata('id_member'))){
			$resume_login = $this->resume_login();
			if($resume_login == true){
				$this->session->set_flashdata('temp_sess', 'firstlogin');
				redirect('member/dashboard');
			}
		}else{
      $data['agent_browser'] = $this->agent->browser(TRUE);
      $data['agent_version'] = $this->agent->version(TRUE);

      if($this->agent->browser(TRUE) == 'Chrome'){
				$data['browser_icon'] = 'chrome';
			}elseif($this->agent->browser(TRUE) == 'Firefox'){
				$data['browser_icon'] = 'firefox';
			}elseif($this->agent->browser(TRUE) == 'Safari'){
				$data['browser_icon'] = 'safari';
			}elseif($this->agent->browser(TRUE) == 'Edge'){
				$data['browser_icon'] = 'edge';
			}else{
				$data['browser_icon'] = 'question-circle';
			}

			$data['ip'] = $this->input->ip_address();

			if( $this->input->post('apm_email_inp') &&  $this->input->post('apm_pass_inp') ){
				$email = $this->input->post('apm_email_inp');
				$pass = $this->input->post('apm_pass_inp');
      	$this->auth($email, $pass);
			}else{
				$this->load->view('member/login', $data);
			}
		}
	}

	public function resume_login()
	{
		$id_member = $this->session->userdata('id_member');
		$email = $this->session->userdata('email');
		$session_id = $this->session->userdata('session_id');
		$browser = $this->session->userdata('browser');

		$resume_login_check = $this->m_member->resume_login_check($id_member, $email, $session_id, $browser);
		if($resume_login_check == 0){
			$this->logout();
		}else{
			return true;
		}
	}

	public function template($data)
	{
		$id_member = $this->session->userdata('id_member');
		$session_id = $this->session->userdata('session_id');

		$arr_member_info = $this->m_member->get_member_info($id_member)->result();
		$data['nama_lengkap'] = $arr_member_info[0]->nama_lengkap;
		$arr_loyal_point = $this->m_member->get_member_loyal_point($id_member);
		$data['loyal_point'] = $arr_loyal_point[0]->loyal_point;

		if(!empty($session_id) && !empty($id_member)){
			$this->load->view('member/t_main', $data);
		}else{
			$this->logout();
		}
	}

	public function auth($email, $pass)
	{
		$check_email = $this->m_member->check_email_login($email);
		$row_email = $check_email->num_rows();

		foreach ($check_email->result() as $res) {
			$pass_db = $res->pass_hash;
		}

		if(password_verify($pass, $pass_db)){
			$check_pass = 1;
		}else{
			$check_pass = 0;
		}
		
		if($row_email == 0){
			$this->session->set_flashdata('temp_sess', 'email');
			redirect('member');
		}elseif($check_pass == 0){
			$this->session->set_flashdata('temp_sess', 'pass');
			redirect('member');
		}else{
			$arr_member_data = $this->m_member->get_single_member_data($email);
			$status = array_values($arr_member_data)[0];
			if($status == 200){
				foreach($arr_member_data['data'] as $key => $val){
          $id_member = $arr_member_data['data']['id_member'];
          $email = $arr_member_data['data']['email'];
          $nama_lengkap = $arr_member_data['data']['nama_lengkap'];
          $phone = $arr_member_data['data']['phone'];
          $birth_place = $arr_member_data['data']['birth_place'];
          $birth_date = $arr_member_data['data']['birth_date'];
          $session_id = $arr_member_data['data']['session_id'];
          $email_status = $arr_member_data['data']['email_status'];
				}

				if($email_status == 0){
					$this->session->set_flashdata('temp_sess', 'notactive');
					redirect('member');
				}else{
					$data_member['session_id'] = session_id();
					$data_member['updated_date'] = date('Y-m-d');
          $data_member['browser'] = $this->agent->browser();
          $this->m_member->update_member($data_member, $id_member);

          $data_log['id_member'] = $id_member;
          $data_log['action'] = 'login';
          $data_log['action_date'] = date('Y-m-d');
          $data_log['session_id'] = session_id();
          $data_log['browser'] = $this->agent->browser();
          $this->m_member->insert_member_log($data_log);

					$this->session->set_userdata($data_sess);
					$this->session->set_flashdata('temp_sess', 'firstlogin');

					$data_sess['id_member'] = $id_member;
					$data_sess['email'] = $email;
					$data_sess['nama_lengkap'] = $nama_lengkap;
					$data_sess['session_id'] = session_id();
					$data_sess['browser'] = $this->agent->browser();
					$this->session->set_userdata($data_sess);
					redirect('member/dashboard');
				}

			}else{
				$this->session->set_flashdata('temp_sess', 'email');
				redirect('member');
			}
		}
	}

	public function dashboard()
	{
		$resume_login = $this->resume_login();
		$id_member = $this->session->userdata('id_member');
		$data['total_belanja'] = $this->count_total_belanja($id_member);
		$data['total_order_success'] = $this->count_total_order_success($id_member);
		$data['total_order_pending'] = $this->count_total_order_pending($id_member);
		$data['total_order_cancel'] = $this->count_total_order_cancel($id_member);
		$data['last_ten_order_member'] = $this->m_member->last_ten_order_member($id_member)->result();

		$data['page'] = 'dashboard';
		$data['title'] = 'Dashboard Member - Pekgo Apparel';
		$this->template($data);
	}

	public function count_total_belanja($id_member)
	{
		$total_belanja = $this->m_member->count_total_belanja($id_member)->row('grand_total');
		$total_belanja == '' ? $total_belanja = 0 : $total_belanja;
		return $total_belanja;
	}

	public function count_total_order_success($id_member)
	{
		$total_order = $this->m_member->count_total_order_success($id_member)->row('total_order');
		$total_order == '' ? $total_order = 0 : $total_order;
		return $total_order;
	}

	public function count_total_order_pending($id_member)
	{
		$total_order = $this->m_member->count_total_order_pending($id_member)->row('total_order');
		$total_order == '' ? $total_order = 0 : $total_order;
		return $total_order;
	}

	public function count_total_order_cancel($id_member)
	{
		$total_order = $this->m_member->count_total_order_cancel($id_member)->row('total_order');
		$total_order == '' ? $total_order = 0 : $total_order;
		return $total_order;
	}

	public function registrasi()
	{
		$this->form_validation->set_rules('apm_email', 'Email','trim|required|min_length[5]|max_length[100]|valid_email|is_unique[apm_member.email]'
		);
    $this->form_validation->set_rules('apm_password', 'Password', 'trim|required|min_length[5]|max_length[60]');
    $this->form_validation->set_rules('apm_repassword', 'Confirm Password', 'trim|required|min_length[5]|max_length[60]|matches[apm_password]');
    $this->form_validation->set_rules('apm_nama_lengkap', 'Nama Lengkap', 'trim|required|min_length[3]|max_length[50]|alpha_numeric_spaces');
    $this->form_validation->set_rules('apm_phone', 'Phone', 'trim|required|min_length[9]|max_length[20]|numeric');
    $this->form_validation->set_rules('apm_birth_place', 'Kota Kelahiran', 'trim|required|min_length[5]|max_length[50]|alpha');
    $this->form_validation->set_rules('apm_birth_date', 'Tanggal Lahir', 'trim|required|exact_length[10]');

		$data['form_attr'] = array('class' => 'form-signin', 'id' => 'loginform', 'method' => 'post' );

    if ($this->form_validation->run() === FALSE)
    {
      $this->load->view('member/registrasi', $data);
    }
    else
    {
    	$email = $this->input->post('apm_email');
			$pass = $this->input->post('apm_password');
			$pass = password_hash($pass, PASSWORD_BCRYPT);
			$nama_lengkap = $this->input->post('apm_nama_lengkap');
			$phone = $this->input->post('apm_phone');
			$alamat = $this->input->post('apm_alamat');
			$kode_pos = $this->input->post('apm_kode_pos');
			$birth_place = $this->input->post('apm_birth_place');
			$birth_date = $this->input->post('apm_birth_date');
			$birth_date = date('Y-m-d', strtotime($birth_date));
			$id_provice = $this->input->post('apm_province');
			$id_city = $this->input->post('apm_city');
			$id_district = $this->input->post('apm_district');
			$agent_browser = $this->agent->browser(TRUE);
      $ip = $this->input->ip_address();
      $token = md5($email);
      $session_id = session_id();
      $created_date = date('Y-m-d H:i:s');
			$proses_daftar = $this->m_member->daftar($email, $pass, $nama_lengkap, $phone, $birth_place, $birth_date, $id_provice, $id_city, $id_district, $agent_browser, $ip, $token, $session_id, $created_date, $alamat, $kode_pos);

        	// SEND EMAIL //
        	$data_email['email'] = $email;
			$data_email['nama_lengkap'] = $nama_lengkap;
			$data_email['token'] = $token;

        	$config = 	Array(
								'protocol' => 'smtp',
						        'smtp_host' => 'ssl://mail.pekgo-app.com',
						        'smtp_port' => '465',
						        'smtp_user' => 'noreply@pekgo-app.com',
						        'smtp_pass' => 'PKG20181478963',
						        'smtp_crypto' => 'ssl',
						        'mailtype'  => 'html', 
						        'charset' => 'iso-8859-1',
						        'wordwrap' => TRUE,
						        'starttls'  => TRUE
						);
        	$this->load->library('email');
        	$this->email->from('noreply@pekgo-app.com', 'System No Reply - Pekgo Apparel', 'noreply@pekgo-app.com');
			$this->email->to($email);
			$this->email->cc('');
			$this->email->bcc('adam@pekgo-app.com');

			$this->email->subject('Activation Account - Pekgo Apparel');
			$this->email->message($this->load->view('member/email_activation', $data_email, true));

			if($this->email->send()){
				$this->registrasi_berhasil();
			}else{
				$this->email->print_debugger();
				//$this->registrasi_gagal();
			}
        }
	}

	public function registrasi_berhasil()
	{
		$this->session->set_flashdata('temp_sess', 'success');
		redirect('member/registrasi');
	}

	public function registrasi_gagal()
	{
		$this->session->set_flashdata('temp_sess', 'error');
		redirect('member/registrasi');
	}

	public function logout()
	{
		$id_member = $this->session->userdata('id_member');
		$data_logout = array(
			'id_member' => $id_member,
			'action' => 'logout',
			'action_date' => date('Y-m-d H:i:s'),
			'session_id' => session_id(),
			'browser' => $this->agent->browser(TRUE),
			'ip_address' => $this->input->ip_address()
		);
		$log_logout = $this->m_member->log_logout($data_logout);
		if($log_logout == true){
			$array_unset = array(
				'id_member',
				'email',
				'nama_lengkap',
				'session_id',
				'browser'
			);
			$this->session->unset_userdata($array_unset);
			$this->session->set_flashdata('temp_sess', 'logout');
			redirect('member');
		}else{
			$this->session->destroy();
			redirect('member');
		}
		
	}

	public function json_province()
	{
		$json_province = $this->rajaongkir->province();
		echo $json_province;
	}

	public function json_city()
	{
		$id_province = $this->input->post('id_province');
		$json_cities = $this->rajaongkir->city($id_province);
		echo $json_cities;
	}

	public function json_subdistrict()
	{
		$id_city = $this->input->post('id_city');
		$json_district = $this->rajaongkir->subdistrict($id_city);
		echo $json_district;
	}

	public function test()
	{
		$this->load->library('email');
		// SEND EMAIL //
		$email = 'adam.pm77@gmail.com';
		$data_email['email'] = $email;
		$data_email['nama_lengkap'] = 'adam';
		$data_email['token'] = '123456789';

		$config = 	Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://mail.pekgo-app.com',
			'smtp_port' => '465',
			'smtp_user' => 'noreply@pekgo-app.com',
			'smtp_pass' => 'PKG20181478963',
			'smtp_crypto' => 'ssl',
			'mailtype'  => 'html', 
			'charset' => 'utf-8',
			'wordwrap' => TRUE,
			'starttls'  => TRUE
		);

		$this->load->library('email', $config);
		//$this->email->initialize($config);
		$this->email->from('noreply@pekgo-app.com', 'System No Reply - Pekgo Apparel', 'noreply@pekgo-app.com');
		$this->email->to($email);
		$this->email->cc('');
		$this->email->bcc('adam@pekgo-app.com');

		$this->email->subject('Activation Account - Pekgo Apparel');
		$this->email->message($this->load->view('member/email_activation', $data_email, true));

		if($this->email->send()){
			echo "Berhasil";
		}else{
			$this->email->print_debugger();
			echo "Gagal";
			//$this->registrasi_gagal();
		}
	}

	public function aktivasi_code()
	{
		$email = urldecode($this->uri->segment(3));
		$token = urldecode($this->uri->segment(4));
		$check_email = $this->m_member->check_email_login($email);
		if($check_email == 0){
			$this->session->set_flashdata('temp_sess', 'email');
			redirect('member');
		}else{
			$check_aktivasi = $this->m_member->check_aktivasi($email, $token);
			if($check_aktivasi == 0){
				$this->session->set_flashdata('temp_sess', 'wrongtoken');
				redirect('member');
			}else{
				$data_aktivasi['status'] = '1';
				$proses_aktivasi = $this->m_member->aktivasi($data_aktivasi, $email);
				$this->session->set_flashdata('temp_sess', 'activationok');
				redirect('member');
			}
		}	
	}

	public function manage()
	{
		$resume_login = $this->resume_login();
		$id_member = $this->session->userdata('id_member');
		$data['arr_member'] = $this->m_member->get_member_info($id_member);
		$data['page'] = 'manage';
		$data['title'] = 'Manage Account - Pekgo Apparel';
		$this->template($data);
	}

	public function save_manage()
	{
		$resume_login = $this->resume_login();
		$id_member = $this->session->userdata('id_member');
		$email = $this->input->post('email');
		$nama_lengkap = $this->input->post('nama_lengkap');
		$phone = $this->input->post('phone');
		$birth_place = $this->input->post('birth_place');
		$birth_date = $this->input->post('birth_date');
		$id_province = $this->input->post('id_province');
		$id_city = $this->input->post('id_city');
		$id_district = $this->input->post('id_district');
		$alamat = $this->input->post('alamat');
		$kode_pos = $this->input->post('kode_pos');
		$updated_date = date('Y-m-d H:i:s');
		$browser = $this->agent->browser(TRUE);
		$session = session_id();
		$ip = $this->input->ip_address();

		$data_update = array(
			'email' => $email,
			'nama_lengkap' => $nama_lengkap,
			'phone' => $phone,
			'birth_place' => $birth_place,
			'birth_date' => $birth_date,
			'id_province' => $id_province,
			'id_city' => $id_city,
			'id_district' => $id_district,
			'alamat' => $alamat,
			'kode_pos' => $kode_pos,
			'updated_date' => $updated_date,
			'browser' => $browser
		);

		$data_update_log = array(
			'id_member' => $id_member,
			'action' => 'updated',
			'action_date' => $updated_date,
			'session_id' => $session,
			'browser' => $browser,
			'ip_address' => $ip
		);
		$proses_save_manage = $this->m_member->save_manage($data_update, $data_update_log, $id_member);
		if($proses_save_manage == true){
			$this->session->set_flashdata('temp_sess', 'updateacc1');
			redirect('member/dashboard');
		}else{
			$this->session->set_flashdata('temp_sess', 'updateacc0');
			redirect('member/dashboard');
		}
	}

	public function change_password()
	{
		$resume_login = $this->resume_login();
		$id_member = $this->session->userdata('id_member');
		$data['arr_member'] = $this->m_member->get_member_info($id_member);
		$data['page'] = 'change_password';
		$data['title'] = 'Change Password - Pekgo Apparel';
		$this->template($data);
	}

	public function check_cur_pass()
	{
		$id_member = $this->session->userdata('id_member');
		$arr_member = $this->m_member->get_member_info($id_member);
		$cur_pass = $this->input->post('cur_pass');
		foreach($arr_member->result() as $res){
			$pass_hash = $res->pass_hash;
		}

		if(password_verify($cur_pass, $pass_hash)){
			echo "benar";
		}else{
			echo "salah";
		}
	}

	public function save_password()
	{
		$id_member = $this->session->userdata('id_member');
		$a = $this->input->post('new_pass');
		$a_hash = password_hash($a, PASSWORD_BCRYPT);
		$proses_save = $this->m_member->ganti_pass($a_hash, $id_member);
		if($proses_save == true){
			$this->session->set_flashdata('temp_sess', 'updatepass1');
			redirect('member/dashboard');
		}else{
			$this->session->set_flashdata('temp_sess', 'updatepass0');
			redirect('member/dashboard');
		}
	}

	public function forgotstep1()
	{
		$this->load->view('member/forgotstep1');
	}

	public function check_email()
  {
      $email = $this->input->post('email');
      $chk = $this->m_member->get_member_data($email);
      $tr = $chk->num_rows();
      if($tr == 1){
      	echo "benar";
      }else{
      	echo "gagal";
      }
  }

  public function email_token_reset()
  {
  	$email = $this->input->post('email');
  	$token = md5($email);

  	$data_update_token = array(
  		'token' => $token
  	);
  	$update_token = $this->m_member->update_token($email, $data_update_token);
  	$send_email = $this->send_email_reset_pass($email, $token);
  	if($send_email == "berhasil"){
  		$this->session->set_flashdata('temp_sess', 'sendemailresetpass1');
			redirect('member/forgotstep1');
  	}else{
  		$this->session->set_flashdata('temp_sess', 'sendemailresetpass0');
			redirect('member/forgotstep1');
  	}
  }

  public function send_email_reset_pass($email, $token)
  {
  	// SEND EMAIL //
  	$this->load->library('email');
		$data_email['email'] = $email;
		$data_email['token'] = $token;

		$config = 	Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://mail.pekgo-app.com',
			'smtp_port' => '465',
			'smtp_user' => 'noreply@pekgo-app.com',
			'smtp_pass' => 'PKG20181478963',
			'smtp_crypto' => 'ssl',
			'mailtype'  => 'html', 
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE,
			'starttls'  => TRUE
		);

		$this->load->library('email');
		$this->email->from('noreply@pekgo-app.com', 'System No Reply - Pekgo Apparel', 'noreply@pekgo-app.com');
		$this->email->to($email);
		$this->email->cc('');
		$this->email->bcc('adam@pekgo-app.com');

		$this->email->subject('Reset Password - Pekgo Apparel');
		$this->email->message($this->load->view('member/email_reset_password', $data_email, true));

		if($this->email->send()){
			return "berhasil";
		}else{
			$this->email->print_debugger();
			return "gagal";
			echo "gagal";
			exit();
		}
  }

  public function reset_password()
  {
  	$email = urldecode($this->uri->segment(3));
  	$data['email'] = $email;
		$token = urldecode($this->uri->segment(4));
		$check = $this->m_member->get_member_data($email);
		foreach ($check->result() as $res) {
			$token_db = $res->token;
			if($token_db == $token){
				$this->load->view('member/forgotstep2.php', $data);
			}else{
				echo "Email dan Token tidak ditemukan, Terima Kasih.<br>ERR#0001/APM";
			}
		}
  }

  public function reset_new_password()
	{
		$a = $this->input->post('new_pass');
		$b = $this->input->post('email');
		$proses_save = $this->m_member->ganti_passs($a, $b);
		if($proses_save == true){
			$this->session->set_flashdata('temp_sess', 'resetpass1');
			redirect('member');
		}else{
			$this->session->set_flashdata('temp_sess', 'resetpass0');
			redirect('member');
		}
	}

	public function resend()
	{
		$this->load->view('member/resend');
	}

	public function resend_email()
	{
		$email = $this->input->post('email');
		$check = $this->m_member->get_member_data($email);
		foreach ($check->result() as $res) {
			$nama_lengkap = $res->nama_lengkap;
			$token = $res->token;
			$status = $res->status;
		}

		if($status == '1'){
			$this->session->set_flashdata('temp_sess', 'resend2');
			redirect('member');
		}else{
			// SEND EMAIL //
			$data_email['email'] = $email;
			$data_email['nama_lengkap'] = $nama_lengkap;
			$data_email['token'] = $token;

			$config = 	Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://mail.pekgo-app.com',
				'smtp_port' => '465',
				'smtp_user' => 'noreply@pekgo-app.com',
				'smtp_pass' => 'PKG20181478963',
				'smtp_crypto' => 'ssl',
				'mailtype'  => 'html', 
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE,
				'starttls'  => TRUE
			);

			$this->load->library('email');
			$this->email->from('noreply@pekgo-app.com', 'System No Reply - Pekgo Apparel', 'noreply@pekgo-app.com');
			$this->email->to($email);
			$this->email->cc('');
			$this->email->bcc('adam@pekgo-app.com');

			$this->email->subject('Activation Account - Pekgo Apparel');
			$this->email->message($this->load->view('member/email_activation', $data_email, true));

			if($this->email->send()){
				$this->session->set_flashdata('temp_sess', 'resend1');
				redirect('member');
			}else{
				//$this->email->print_debugger();
				$this->session->set_flashdata('temp_sess', 'resend0');
				redirect('member');
			}
		}
	}

	public function load_detail_order()
	{
		$id_order = $this->input->post('id_order');
		$data['arr_produk'] = $this->m_member->get_detail_order($id_order);
		$this->load->view('member/detail_order', $data);
	}

}
