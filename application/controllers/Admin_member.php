<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_member extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin_member', 'm_admin');
    }

    public function resume_login()
	{
		$id_admin = $this->session->userdata('id_admin');
		$username = $this->session->userdata('username');
		$session_id = $this->session->userdata('session_id');
		$browser = $this->session->userdata('browser');

		$resume_login_check = $this->m_admin->resume_login_check($id_admin, $username, $session_id, $browser);
		if($resume_login_check == 0){
			$this->logout();
		}else{
			return true;
		}
	}

	public function logout()
	{
		$array_unset = array(
			'id_admin',
			'username',
			'nama_lengkap',
			'session_id',
			'browser'
		);
		$this->session->unset_userdata($array_unset);
		$this->session->set_flashdata('temp_sess', 'logout');
		redirect('admin');
	}

    public function template($data = null)
	{
		$id_admin = $this->session->userdata('id_admin');
		$session_id = $this->session->userdata('session_id');

		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$data['nama_lengkap'] = $arr_admin_info[0]->nama_lengkap;
		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$tipe = $arr_admin_info[0]->tipe;
		($tipe == 1) ? $tipe = "Master Admin" : $tipe = "Staff";
		$data['tipe'] = $tipe;

		if(!empty($session_id) && !empty($id_admin)){
			$this->load->view('admin/t_main', $data);
		}else{
			$this->logout();
		}
	}

	public function index()
	{
		$resume_login = $this->resume_login();
		$id_admin = $this->session->userdata('id_admin');
			
		$data['page'] = 'member';
		$data['title'] = 'List Member - Pekgo Apparel';
		$this->template($data);
		
	}

	public function json_member()
	{
		// Datatables Variables
		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));

		$requestData = $this->input->get('search');
		if($requestData['value'] != null){
			$keyword = $requestData['value'];
		}else{
			$keyword = "";
		}

		$json_data = $this->m_admin->get_member($keyword);

		$data = array();
		$no = 0;

		foreach($json_data->result() as $res){
			$this->load->library('rajaongkir');
			$json_province = $this->rajaongkir->province($res->id_province);
			$data_province = json_decode($json_province);
			$nama_province = $data_province->rajaongkir->results->province;

			$json_city = $this->rajaongkir->city($res->id_province, $res->id_city);
			$data_city = json_decode($json_city);
			$nama_city = $data_city->rajaongkir->results->type.' '.$data_city->rajaongkir->results->city_name;

			$json_district = $this->rajaongkir->subdistrict($res->id_city, $res->id_district);
			$data_district = json_decode($json_district);
			$nama_district = $data_district->rajaongkir->results->subdistrict_name;

			$no++;
			$status = '';
			if($res->status == '0'){
				$status = 'Not Verified';
			}elseif($res->status == '1'){
				$status = 'Active';
			}else{
				$status = 'Lock';
			}

			$data[] = array(
				'no' => $no,
				'id_member' => $res->id_member,
				'nama_lengkap' => $res->nama_lengkap,
				'email' => $res->email,
				'phone' => $res->phone,
				'birth_day' => $res->birth_place.', '.$res->birth_date,
				'id_province' => $nama_province,
				'id_city' => $nama_city,
				'id_district' => $nama_district,
				'created_date' => $res->created_date,
				'last_login' => $res->last_login,
				'status' => $status
			);
		}

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $json_data->num_rows(),
			"recordsFiltered" => $json_data->num_rows(),
			"data" => $data
		);

		echo json_encode($output);
		exit();
	}

	public function lock_member()
	{
		$id_member = $this->input->get('id_member');
		$proses_lock = $this->m_admin->lock_member($id_member);
		echo $proses_lock;
	}

	public function unlock_member()
	{
		$id_member = $this->input->get('id_member');
		$proses_unlock = $this->m_admin->unlock_member($id_member);
		echo $proses_unlock;
	}

}
