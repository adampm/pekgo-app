<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_stock_log extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin_stock_log', 'm_admin');
    }

    public function resume_login()
	{
		$id_admin = $this->session->userdata('id_admin');
		$username = $this->session->userdata('username');
		$session_id = $this->session->userdata('session_id');
		$browser = $this->session->userdata('browser');

		$resume_login_check = $this->m_admin->resume_login_check($id_admin, $username, $session_id, $browser);
		if($resume_login_check == 0){
			$this->logout();
		}else{
			return true;
		}
	}

	public function logout()
	{
		$array_unset = array(
			'id_admin',
			'username',
			'nama_lengkap',
			'session_id',
			'browser'
		);
		$this->session->unset_userdata($array_unset);
		$this->session->set_flashdata('temp_sess', 'logout');
		redirect('admin');
	}

    public function template($data = null)
	{
		$id_admin = $this->session->userdata('id_admin');
		$session_id = $this->session->userdata('session_id');

		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$data['nama_lengkap'] = $arr_admin_info[0]->nama_lengkap;
		$arr_admin_info = $this->m_admin->get_admin_info($id_admin)->result();
		$tipe = $arr_admin_info[0]->tipe;
		($tipe == 1) ? $tipe = "Master Admin" : $tipe = "Staff";
		$data['tipe'] = $tipe;

		if(!empty($session_id) && !empty($id_admin)){
			$this->load->view('admin/t_main', $data);
		}else{
			$this->logout();
		}
	}

	public function index()
	{
		$resume_login = $this->resume_login();
		$id_admin = $this->session->userdata('id_admin');
			
		$data['page'] = 'stock_log';
		$data['title'] = 'Stock Log Admin - Pekgo Apparel';
		$this->template($data);
		
	}

	public function json_stock_log()
	{
		// Datatables Variables
		$draw = intval($this->input->get('draw'));
		$start = intval($this->input->get('start'));
		$length = intval($this->input->get('length'));

		$requestData = $this->input->get('search');
		if($requestData['value'] != null){
			$keyword = $requestData['value'];
		}else{
			$keyword = "";
		}

		$json_data = $this->m_admin->get_stock_log($keyword);

		$data = array();
		$no = 0;

		foreach($json_data->result() as $res){
			$no++;
			$data[] = array(
				'no' => $no,
				'tanggal' => $res->tanggal,
				'nama_produk' => $res->nama_produk,
				'tipe' => strtoupper($res->tipe),
				'size' => strtoupper($res->size),
				'qty' => number_format($res->qty,0,',','.'),
				'no_invoice' => $res->no_invoice,
				'admin' => $res->admin
			);
		}

		$output = array(
			"draw" => $draw,
			"recordsTotal" => $json_data->num_rows(),
			"recordsFiltered" => $json_data->num_rows(),
			"data" => $data
		);

		echo json_encode($output);
		exit();
	}

}
