<?php
foreach($arr_member->result() as $res){
?>
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-6 grid-margin">
      <div class="card">
        <form class="form" id="manage_form" action="<?=site_url('member/save_manage');?>" method="post">
          <div class="card-body">
            <h4 class="card-title">Manage Account</h4>
            <hr>
            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" value="<?=$res->email;?>" class="form-control" id="email" name="email" placeholder="Email" readonly>
            </div>
            <div class="form-group">
              <label for="nama_lengkap">Nama Lengkap</label>
              <input type="text" value="<?=$res->nama_lengkap;?>" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Nama Lengkap" minlength="3" maxlength="50" required>
            </div>
            <div class="form-group">
              <label for="phone">Phone</label>
                <input type="text" value="<?=$res->phone;?>" class="form-control" id="phone" name="phone" placeholder="phone" minlength="9" maxlength="20" required>
            </div>
            <div class="form-group">
              <label for="birth_place">Kota Kelahiran</label>
              <input type="text" value="<?=$res->birth_place;?>" class="form-control" id="birth_place" name="birth_place" placeholder="Kota Kelahiran" minlength="5" maxlength="50" required>
            </div>
            <div class="form-group">
              <label for="birth_date">Tanggal Lahir</label>
              <input type="date" timezone="Asia/Jakarta" value="<?=$res->birth_date;?>" class="form-control" id="birth_date" name="birth_date" placeholder="Tanggal Lahir" required>
            </div>
            <div class="form-group">
              <label for="id_province">Province</label>
              <select id="id_province_inp" name="id_province" class="form-control" required>
              </select>
            </div>
            <div class="form-group">
              <label for="id_city">City</label>
              <select id="id_city_inp" name="id_city" class="form-control" disabled required>
                <option value="">-Select City-</option>
              </select>
            </div>
            <div class="form-group">
              <label for="id_district">District</label>
              <select id="id_district_inp" name="id_district" class="form-control" disabled required>
                <option value="">-Select District-</option>
              </select>
            </div>
            <div class="form-group">
              <label for="alamat">Alamat</label>
              <input type="text" id="alamat" name="alamat" class="form-control" placeholder="Alamat" value="<?=$res->alamat;?>" required>
            </div>
            <div class="form-group">
              <label for="kode_pos">Kode POS</label>
              <input type="text" id="kode_pos" name="kode_pos" class="form-control" placeholder="Kode POS" maxlength="5" value="<?=$res->kode_pos;?>" required>
            </div>

          </div>
          <div class="card-footer">
            <input type="hidden" id="idp" class="form-control" value="<?=$res->id_province;?>" readonly>
            <input type="hidden" id="idc" class="form-control" value="<?=$res->id_city;?>" readonly>
            <input type="hidden" id="idd" class="form-control" value="<?=$res->id_district;?>" readonly>
            <button type="submit" class="btn btn-primary btn-block">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<?php } ?>