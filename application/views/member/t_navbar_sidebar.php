<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <div class="nav-link">
        <div class="user-wrapper">
          <div class="text-wrapper">
            <p class="profile-name"><i class="fas fa-user"></i> <?=$nama_lengkap;?></p>
            <div>
              <small class="designation"><i class="fas fa-star"></i> <?=$loyal_point;?> Loyal Point</small>
            </div>
          </div>
        </div>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=base_url('member/dashboard');?>">
        <i class="menu-icon fas fa-desktop"></i> <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#" onClick="notava();">
        <i class="menu-icon fas fa-history"></i> <span class="menu-title">Order History</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#" onClick="notava();">
        <i class="menu-icon fas fa-camera"></i> <span class="menu-title">Konfirmasi Pembayaran</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=base_url('member/logout');?>">
        <i class="menu-icon fas fa-sign-out-alt"></i> <span class="menu-title">Log out</span>
      </a>
    </li>
  </ul>
</nav>
<!-- partial -->