<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Pekgo Apparel Application">
  <meta name="author" content="@manasama77">
  <link rel="shortcut icon" href="<?=base_url('assets/img/logo_fqv_icon.ico');?>" />
  <title>Member Log In - Pekgo Apparel</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/iconfonts/mdi/css/materialdesignicons.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.base.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.addons.css');?>">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/style.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/fontawesome/css/all.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/toast/dist/jquery.toast.min.css');?>">

  <script type="text/javascript" src="<?=base_url('vendor/jquery/jquery-3.3.1.min.js');?>"></script>
  <script type="text/javascript" src="<?=base_url('vendor/fontawesome/js/all.min.js');?>"></script>
  <!-- endinject -->
</head>

<body>
<?php
if($this->session->flashdata('temp_sess'))
{
	$tipe_alert = $this->session->flashdata('temp_sess');
}else{
	$tipe_alert = 0;
}
?>
<input type="hidden" id="temp_sess" name="temp_sess" value="<?=$tipe_alert;?>">
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <!-- Background Photo by Arkadiy Parovoz from Pexels https://www.pexels.com/photo/silhouette-photo-of-trees-during-golden-hour-1470589/ -->
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-three">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
              <form class="form" method="post" action="<?=site_url('member');?>">
                <div class="text-center">
              	  <img class="mb-4" src="<?=base_url('assets/img/logo.svg');?>" alt="Pekgo Apparel Logo" width="200">
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text bg-dark text-white sr-only">Email</span>
                  </div>
                  <input type="text" class="form-control" id="apm_email_inp" name="apm_email_inp" placeholder="Email">
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text bg-dark text-white sr-only">Password</span>
                  </div>
                  <input type="password" class="form-control" id="apm_pass_inp" name="apm_pass_inp" placeholder="Password">
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-lg btn-primary btn-block">Log In</button>
                </div>
                <div class="form-group d-flex justify-content-between text-center">
                  <a href="<?=site_url('member/forgotstep1');?>" class="btn btn-danger btn-xs btn-block">
                    Forgot Password
                  </a>
                </div>
                <div class="text-block text-center my-3">
                  <span class="text-small font-weight-semibold">Not a member ?</span>
                  <a href="<?=base_url('member/registrasi');?>" class="btn btn-info btn-xs">Create new account</a>
                </div>
                <div class="text-block text-center my-3">
                  <span class="text-small font-weight-semibold">Didn't Get Email Verification ?</span>
                  <a href="<?=base_url('member/resend');?>" class="btn btn-warning btn-xs">Re-send Email Account Activation</a>
                </div>
              </form>
            </div>
            <div class="text-center">
	            <span class="text-small font-weight-semibold">
                <button type="button" onClick="viewTerm();" class="btn btn-dark btn-xs">
                  <i class="fas fa-file-contract"></i> Terms & Rules
                </button>
	            </span>
        	</div>
            <p class="mb-3 text-muted text-center"></p>
            <p class="footer-text text-center text-muted">
            	<small>
            		<i class="fab fa-<?=$browser_icon;?>"></i> 
            		<?=$agent_browser.PHP_EOL;?> <?=$agent_version.PHP_EOL;?> - <i class="fas fa-desktop"></i> <?=$ip.PHP_EOL;?>
            	</small>
            	<br>
            	Copyright © 2018 Pekgo Apparel<br> 
				Powered by <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.<br> 
				Modified by <a href="https://twitter.com/adampm" target="_blank">APM Web Dev</a> - 
				Version 2.0.0
				</span>
            </p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>

  <div class="modal fade" id="term">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Pekgo Apparel Terms & Rules</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-12">
              <h4>Semua customer yang akan menjadi member pekgo-apparel.com wajib memenuhi peraturan sebagai berikut:</h4>
              <ul>
                <li>
                  <h5>Pada saat mendaftar menjadi member, customer wajib mengisi semua form register / pendaftaran dengan data yang valid</h5>
                </li>
                <li>
                  <h5>Customer tidak diperkenankan untuk melakukan share akun. Apabila terjadi penyalahgunaan akun dikarenakan customer berbagi akses account yang sama dengan orang lain, diluar tanggung jawab dari team pekgo-appare.com</h5>
                </li>
                <li>
                  <h5>Apabila customer melakukan pembelian pastikan dengan segera melakukan transfer pembayaran dalam sebelum yang telah disepakati dengan admin Pekgo Apparel dan langsung melakukan mengirimkan bukti pembayaran lewat fitur Konfirmasi Pembayaran yang telah disediakan pada halaman member</h5>
                </li>
                <li>
                  <h5>Apabila customer transfer melebihi dari waktu yang telah ditentukan maka order otomatis ter-cancel, dan refund akan dilakukan maksimal 7 hari ke rekening terkait</h5>
                </li>
                <li>
                  <h5>Apabila jumlah transfer / pembayaran kurang dari nominal tertagih yang ada di invoice maka customer diharuskan melakukan sisa transfer sesuai dengan jumlah tertagih pada invoice tertagih sebelum waktu yang telah ditentukan jika tidak ingin order ter-cancel otomatis oleh sistem</h5>
                </li>
                <li>
                  <h5>Pengiriman order akan diproses apabila bukti transfer yang kamu kirimkan lewat fitur Confirm Payment telah di cek dan dinyatakan valid oleh admin</h5>
                </li>
                <li>
                  <h5>Apabila customer memiliki 3 order yang tercancel oleh sistem, maka otomatis ID terkait akan mendapatkan penalty berupa Banned Permanent ID</h5>
                </li>
                <li>
                  <h5>Jika terjadi kesalahan sistem pada saat proses berbelanja / melakukan order / melakukan konfirmasi pembayaran, segera melakukan contact maksimal 1x24 jam dengan Admin Pekgo Apparel di nomor whatsapp 081296754696</h5>
                </li>
                <li>
                  <h5>Peraturan dapat berubah sewaktu-waktu tanpa pemberitahuan sebelumnya guna menyesuikan kondisi tertentu.</h5>
                </li>
                <li>
                  <h5>Dengan ini customer telah membaca Terms & Rules dengan lengkap dan menyetujinya.</h5>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?=base_url('vendor/star-admin/js/vendor.bundle.base.js');?>"></script>
  <script src="<?=base_url('vendor/star-admin/js/vendor.bundle.addons.js');?>"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?=base_url('vendor/star-admin/js/off-canvas.js');?>"></script>
  <script src="<?=base_url('vendor/star-admin/js/misc.js');?>"></script>
  <!-- endinject -->

<script type="text/javascript" src="<?=base_url('vendor/toast/dist/jquery.toast.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function(){
	var tempSess = $('#temp_sess').val();
	if(tempSess != 0){
		showToast(tempSess);
	}
	
});

/* -------------------------------------------------------------------------------------------- */

function viewTerm(){
  $('#term').modal('show');
}

function showToast(tipe){
	if(tipe == 'email'){
		var heading = 'Email not found!';
		var message = 'Kamu belum terdaftar, untuk mendaftar silahkan klik <a href="member/daftar">Disini</a>';
		var color = 'warning';
	}else if(tipe == 'pass'){
		var heading = 'Password Salah!';
		var message = 'Silahkan kamu cek kembali password kamu';
		var color = 'warning';
	}else if(tipe == 'logout'){
		var heading = 'Log out Berhasil!';
		var message = 'Terima kasih';
		var color = 'success';
	}else if(tipe == 'notactive'){
		var heading = 'Email Belum Aktif!';
		var message = 'Silahkan aktivasi account kamu dengan mengecek Inbox Email / Spam Email kamu. Terima kasih';
		var color = 'warning';
	}else if(tipe == 'wrongtoken'){
		var heading = 'Token Salah!';
		var message = 'Proses aktivasi gagal dikarenakan Token Salah. Terima kasih';
		var color = 'danger';
	}else if(tipe == 'activationok'){
		var heading = 'Proses Aktivasi Akun Berhasil!';
		var message = 'Silahkan kamu masukan Email & Password account kamu. Terima Kasih';
		var color = 'success';
	}else if(tipe == 'resetpass1'){
    var heading = 'Success!';
    var message = 'Reset Password Berhasil, silahkan kamu login dengan password baru kamu.';
    var color = 'success';
  }else if(tipe == 'resetpass0'){
    var heading = 'Something Wrong!';
    var message = 'Reset Password Gagal, silahkan coba kembali';
    var color = 'danger';
  }else if(tipe == 'resend1'){
    var heading = 'Success!';
    var message = 'Re-Send Email Activation Berhasil, silahkan kamu cek kotak masuk email kamu.';
    var color = 'success';
  }else if(tipe == 'resend0'){
    var heading = 'Something Wrong!';
    var message = 'Re-Send Email Activation Gagal, silahkan coba kembali';
    var color = 'danger';
  }else if(tipe == 'resend2'){
    var heading = 'Ooops!';
    var message = 'Akun kamu telah aktif, silahkan kamu login. Terima kasih';
    var color = 'info';
  }

	$.toast({
	    text: message,
	    heading: heading,
	    icon: color,
	    showHideTransition: 'fade',
	    allowToastClose: false,
	    hideAfter: 10000,
	    stack: 1,
	    position: 'top-left',
	    textAlign: 'left',
	    loader: true,
	    loaderBg: '#9EC600',    
	});
}
</script>
</body>

</html>