<?php
foreach($arr_member->result() as $res){
?>
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-6 grid-margin">
      <div class="card">
        <form class="form" id="manage_form" action="<?=site_url('member/save_password');?>" method="post">
          <div class="card-body">
            <h4 class="card-title">Change Password</h4>
            <hr>
            <div class="form-group">
              <label for="cur_pass">Current Password</label>
              <input type="password" class="form-control" id="cur_pass" name="cur_pass" placeholder="Current Password" onChange="checkCurPass()" maxlength="50" required>
            </div>
            <div class="form-group">
              <label for="new_pass">New Password</label>
              <input type="password" class="form-control" id="new_pass" name="new_pass" placeholder="New Password" maxlength="50" onKeyUp="comparePass()" required>
            </div>
            <div class="form-group">
              <label for="new_pass_re">Re-type New Password</label>
              <input type="password" class="form-control" id="new_pass_re" name="new_pass_re" placeholder="Re-type New Password" maxlength="50" onKeyUp="comparePass()" required>
            </div>

          </div>
          <div class="card-footer">
            <input type="hidden" id="ah" value="0">
            <input type="hidden" id="ahah" value="0">
            <input type="hidden" id="ahahah" value="0">
            <button type="submit" id="sbmtpass" class="btn btn-primary btn-block" disabled>Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->
<?php } ?>