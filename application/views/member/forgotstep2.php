<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Pekgo Apparel Application">
  <meta name="author" content="@manasama77">
  <link rel="shortcut icon" href="<?=base_url('assets/img/logo_fqv_icon.ico');?>" />
  <title>Member Log In - Pekgo Apparel</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/iconfonts/mdi/css/materialdesignicons.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.base.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.addons.css');?>">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/style.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/fontawesome/css/all.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/toast/dist/jquery.toast.min.css');?>">

  <script type="text/javascript" src="<?=base_url('vendor/jquery/jquery-3.3.1.min.js');?>"></script>
  <script type="text/javascript" src="<?=base_url('vendor/fontawesome/js/all.min.js');?>"></script>
  <!-- endinject -->
</head>

<body>
	<?php
	if($this->session->flashdata('temp_sess')){
		$temp_sess = $this->session->flashdata('temp_sess');
	}else{
		$temp_sess = '';
	}
	?>
	<input type="hidden" id="temp_sess" value="<?=$temp_sess;?>">
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <!-- Background Photo by Arkadiy Parovoz from Pexels https://www.pexels.com/photo/silhouette-photo-of-trees-during-golden-hour-1470589/ -->
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-three">
        <div class="row w-100">
          <div class="col-lg-3 mx-auto">
            <div class="auto-form-wrapper">
              <form class="form" id="forpass1form" method="post" action="<?=site_url('member/reset_new_password');?>">
                <div class="text-center">
              	  <img class="mb-4" src="<?=base_url('assets/img/logo.svg');?>" alt="Pekgo Apparel Logo" width="200">
              	  <h4>Forgot Password</h4>
                </div>

                	<input type="password" class="form-control" id="new_pass" name="new_pass" placeholder="New Password" maxlength="50" required>
                  <input type="password" class="form-control" id="conf_pass" name="conf_pass" placeholder="Confirm New Password" maxlength="50" required>
                	<input type="hidden" class="form-control" id="email" name="email" value="<?=$email;?>" placeholder="Email" required>
                	<button id="submit" type="submit" class="btn btn-primary btn-block" disabled>Reset Password</button>

              </form>
            </div>
          </div>
          <div class="col-lg-12 text-center">
          	<div class="text-block text-center mt-10">
          		<br>
	            <span class="text-small font-weight-semibold">Already have Account ?</span>
	            <a href="<?=site_url('member');?>" class="btn btn-info btn-xs">Back to Login Page</a>
	          </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?=base_url('vendor/star-admin/js/vendor.bundle.base.js');?>"></script>
  <script src="<?=base_url('vendor/star-admin/js/vendor.bundle.addons.js');?>"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?=base_url('vendor/star-admin/js/off-canvas.js');?>"></script>
  <script src="<?=base_url('vendor/star-admin/js/misc.js');?>"></script>
  <!-- endinject -->

	<script type="text/javascript" src="<?=base_url('vendor/toast/dist/jquery.toast.min.js');?>"></script>
	<script type="text/javascript">
	$(document).ready(function(){

		$('#new_pass').on('keyup', function(res){
			comparePass();
		});

		$('#conf_pass').on('keyup', function(res){
			comparePass();
		});
		
	});

	/* -------------------------------------------------------------------------------------------- */

	function comparePass(){
		var new_pass = $('#new_pass').val();
		var conf_pass = $('#conf_pass').val();
		if(new_pass == conf_pass){
      var heading = 'Yes!';
      var message = 'Password match.';
      var color = 'success';
      generateToast(message, heading, color);
			$('#submit').attr('disabled', false);
		}else{
			var heading = 'Something Wrong!';
			var message = 'New Password & Confirm New Password tidak sama, Silahkan cek kembali.';
			var color = 'warning';
			generateToast(message, heading, color);
			$('#submit').attr('disabled', true);
		}
	}

	function generateToast(message, heading, color){
		/* TEMPLATE
		var heading = 'Proses Aktivasi Akun Berhasil!';
		var message = 'Silahkan kamu masukan Email & Password account kamu. Terima Kasih';
		var color = 'success';
		*/

		$.toast({
		    text: message,
		    heading: heading,
		    icon: color,
		    showHideTransition: 'fade',
		    allowToastClose: false,
		    hideAfter: 10000,
		    stack: 1,
		    position: 'top-left',
		    textAlign: 'left',
		    loader: true,
		    loaderBg: '#9EC600',    
		});
	}
	</script>
</body>
</html>