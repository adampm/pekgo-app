<div class="content-wrapper">
  <div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="fas fa-money-bill text-primary icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Total Belanja</p>
              <div class="fluid-container">
                <h4 class="font-weight-medium text-right mb-0">Rp. <?=number_format($total_belanja,0,',','.');?></h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="fas fa-receipt text-success icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Orders Success</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"><?=number_format($total_order_success,0,',','.');?></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="fas fa-tshirt text-default icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Order Pending</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"><?=number_format($total_order_pending,0,',','.');?></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="fas fa-receipt text-danger icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Order Cancel</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"><?=number_format($total_order_cancel,0,',','.');?></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Your Last 10 Orders</h4>
          <div class="table-responsive">
            <table class="table table-bordered table-sm">
              <thead class="thead-dark">
                <tr>
                  <th class="text-center" style="width:50px;">#</th>
                  <th class="text-left">Date Order</th>
                  <th class="text-center">Invoice</th>
                  <th class="text-right">Grand Total</th>
                  <th class="text-center">Status</th>
                </tr>
              </thead>
              <tbody>
              <?php
              $no = 0;
              foreach($last_ten_order_member as $key){
                $no++;
                $id_order = $key->id_order;
              ?>
                <tr>
                  <td class="text-center"><?=$no;?></td>
                  <td class="text-left"><?=$key->created_date;?></td>
                  <td class="text-center">
                    <button type="button" data-id="<?=$key->id_order;?>" class="btn btn-primary btn-flat btn-block btn-xs" onClick="viewDetailOrder(<?=$id_order;?>)">
                      <?=$key->no_invoice;?>
                    </button>
                  </td>
                  <td class="text-right">Rp. <?=number_format($key->grand_total, 0, ',', '.');?></td>
                  <td class="text-center">
                    <?php
                    if($key->status == '0'){
                      echo '<span class="badge badge-dark">Pending</span>';
                    }elseif($key->status == '1'){
                      echo '<span class="badge badge-success">Success</span>';
                    }elseif($key->status == '2'){
                      echo '<span class="badge badge-danger">Canceled</span>';
                    }
                    ?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer">
          <a href="#" class="btn btn-info">View More</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->

<!-- Modal -->
<div class="modal fade" id="detailOrderModal" tabindex="-1" role="dialog" aria-labelledby="detailOrderModalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detailOrderModalTitle">Detail Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="load_detail_order">...</div>
      </div>
    </div>
  </div>
</div>