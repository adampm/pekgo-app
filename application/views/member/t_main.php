<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Pekgo Apparel Application">
  <meta name="author" content="@manasama77">
  <link rel="icon" href="<?=base_url('assets/img/logo_fqv_icon.ico');?>">
  <title><?=$title;?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/iconfonts/mdi/css/materialdesignicons.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.base.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.addons.css');?>">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/style.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/fontawesome/css/all.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/toast/dist/jquery.toast.min.css');?>">
  <!-- endinject -->
  <style>
  </style>
</head>

<body>
<?php
if($this->session->flashdata('temp_sess'))
{
  $tipe_alert = $this->session->flashdata('temp_sess');
}else{
  $tipe_alert = 0;
}
?>
  <input type="hidden" id="temp_sess" name="temp_sess" value="<?=$tipe_alert;?>">
  <div class="container-scroller">
    <?php $this->load->view('member/t_navbar_top'); ?>
    <div class="container-fluid page-body-wrapper">
      <?php $this->load->view('member/t_navbar_sidebar'); ?>
      <div class="main-panel">
        <?php $this->load->view('member/'.$page);?>
        <?php $this->load->view('member/t_footer');?>
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
</body>

</html>

<!-- plugins:js -->
<script src="<?=base_url('vendor/star-admin/js/vendor.bundle.base.js');?>"></script>
<script src="<?=base_url('vendor/star-admin/js/vendor.bundle.addons.js');?>"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="<?=base_url('vendor/star-admin/js/off-canvas.js');?>"></script>
<script src="<?=base_url('vendor/star-admin/js/misc.js');?>"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<!--script src="<?=base_url('vendor/star-admin/js/dashboard.js');?>"></script-->
<script type="text/javascript" src="<?=base_url('vendor/fontawesome/js/all.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('vendor/toast/dist/jquery.toast.min.js');?>"></script>
<!-- End custom js for this page-->
<script>
  $(document).ready(function(){
    //manage account
    var idp = $('#idp').val();
    var idc = $('#idc').val();
    var idd = $('#idd').val();
    //loadProvince(idp);

    $('#id_province_inp').on('change', function(){
      var id_province = $(this).val();
      if(id_province == ''){
        $('#id_city_inp').attr('disabled', true).val('').change();
      }else{
        loadCity(id_province, idc);
      }
      
    });

    $('#id_city_inp').on('change', function(){
      var id_city = $(this).val();
      if(id_city == ''){
        $('#id_district_inp').attr('disabled', true).val('').change();
      }else{
        loadSubDistrict(id_city, idd);
      }
      
    });

    var tempSess = $('#temp_sess').val();
    if(tempSess != 0){
      showToast(tempSess);
    }
  });

  function viewDetailOrder(id_order){
    alert(id_order);
    $.ajax({
      url: '<?=site_url('member/load_detail_order');?>',
      method: 'POST',
      data: { id_order:id_order },
      dataType: 'html'
    })
    .done(function(result){
      $('#load_detail_order').html(result);
    });
    $('#detailOrderModal').modal('show');

  }

  function loadProvince(idp){
    $.ajax({
      url : '<?=base_url('member/json_province');?>',
      method : 'POST',
      dataType : 'json',
    }).
    done(function(result){
      $('#id_province_inp').append('<option value="">-Select Provnice-</option>');
      $.each(result['rajaongkir']['results'], function(key, value){
        $('#id_province_inp').append('<option value="'+ value['province_id'] +'">'+ value['province'] +'</option>');
      });
      $('#id_province_inp').val(idp).change();
    });
  }

  function loadCity(id_province, idc){
    $.ajax({
      url : '<?=base_url('member/json_city');?>',
      method : 'POST',
      data : { id_province : id_province },
      dataType : 'json'
    }).
    done(function(result){
      $('#id_city_inp').attr('disabled', false);
      $('#id_city_inp').empty();
      $('#id_city_inp').append('<option value="">-Select City-</option>');
      $.each(result['rajaongkir']['results'], function(key, value){
        $('#id_city_inp').append('<option value="'+ value['city_id'] +'">' + value['type'] + ' '+ value['city_name'] +'</option>');
      });
      $('#id_city_inp').val(idc).change();
    });
  }

  function loadSubDistrict(id_city, idd){
    $.ajax({
      url : '<?=base_url('member/json_subdistrict');?>',
      method : 'POST',
      data : { id_city : id_city },
      dataType : 'json'
    })
    .done(function(result){
      $('#id_district_inp').attr('disabled', false);
      $('#id_district_inp').empty();
      $('#id_district_inp').append('<option value="">-Select District-</option>');
      $.each(result['rajaongkir']['results'], function(key, value){
        $('#id_district_inp').append('<option value="'+ value['subdistrict_id'] +'">'+ value['subdistrict_name'] +'</option>');
      });
      $('#id_district_inp').val(idd).change();
    });
  }

  function notava(){
    var heading = 'Tidak tersedia saat ini!';
    var message = 'Fitur ini akan disediakan dalam waktu dekat. Terima Kasih.';
    var color = 'info';
    generateToast(message, heading, color);
  }

  function showToast(tipe){
    if(tipe == 'firstlogin'){
      var heading = 'Login Berhasil!';
      var message = 'Pastikan kamu menjaga Email & Password kamu. Terima Kasih.';
      var color = 'success';
    }else if(tipe == 'error'){
      var heading = 'Registrasi Gagal!';
      var message = 'Silahkan Coba Kembali. Terima Kasih.';
      var color = 'danger';
    }else if(tipe == 'updateacc1'){
      var heading = 'Success!';
      var message = 'Update Account Berhasil.';
      var color = 'success';
    }else if(tipe == 'updateacc0'){
      var heading = 'Something Wrong!';
      var message = 'Update Account Gagal, silahkan coba kembali.';
      var color = 'danger';
    }else if(tipe == 'updatepass1'){
      var heading = 'Success!';
      var message = 'Update Password Berhasil.';
      var color = 'success';
    }else if(tipe == 'updatepass0'){
      var heading = 'Something Wrong!';
      var message = 'Update Password Gagal, silahkan coba kembali.';
      var color = 'danger';
    }

    generateToast(message, heading, color);
  }

  function generateToast(message, heading, color){
    $.toast({
        text: message,
        heading: heading,
        icon: color,
        showHideTransition: 'fade',
        allowToastClose: false,
        hideAfter: 10000,
        stack: 1,
        position: 'top-left',
        textAlign: 'left',
        loader: true,
        loaderBg: '#9EC600',    
    });
  }

  function checkCurPass(){
    cur_pass = $('#cur_pass').val();
    $.ajax({
      url : '<?=base_url('member/check_cur_pass');?>',
      method : 'POST',
      data : { cur_pass : cur_pass },
    })
    .done(function(res){
      if(res == "benar"){
        $('#ah').val('1');
      }else{
        $('#ah').val('');
        var heading = 'Something Wrong!';
        var message = 'Current Password Salah, silahkan coba kembali.';
        var color = 'warning';
        generateToast(message, heading, color);
      }
      attrSubmitPass();
    });
  }

  function comparePass(){
    var b = $('#new_pass').val();
    var c = $('#new_pass_re').val();
    console.log(b, c);
    if(b == c){
       $('#ahah').val('1');
       $('#ahahah').val('1');
       var heading = 'Yes!';
        var message = 'New Password & Re-type Password sama.';
        var color = 'success';
        generateToast(message, heading, color);
    }else{
      $('#ahah').val('0');
      $('#ahahah').val('0');
      var heading = 'Something Wrong!';
      var message = 'New Password & Re-type Password tidak sama, silahkan coba kembali.';
      var color = 'warning';
      generateToast(message, heading, color);
    }
    attrSubmitPass();
  }

  function attrSubmitPass(){
    var a = $('#ah').val();
    var b = $('#ahah').val();
    var c = $('#ahahah').val();

    if(a == 1 && b == 1 && c == 1){
      $('#sbmtpass').attr("disabled", false);
    }else{
      $('#sbmtpass').attr("disabled", true);
    }
  }

</script>