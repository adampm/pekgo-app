<?php
/*foreach($arr_order->result() as $res){
	$no_invoice = $res->no_invoice;
	$order_date = $res->created_date;
	$ongkir = $res->ongkir;
	$tambahan = $res->tambahan;
	$catatan = $res->catatan;
	$kode_unik = $res->kode_unik;
	$grand_total = $res->grand_total;
}
foreach($arr_customer->result() as $res){
	$nama_lengkap = $res->nama_lengkap;
	$email = $res->email;
	$phone = $res->phone;
	$alamat = $res->alamat;
	$kode_pos = $res->kode_pos;
}*/
?>
<div class="row">
	<!--div class="col-12">
		<h5>Template Whatsapp</h5>
		<textarea class="form-control" rows=5><?=$copycat;?></textarea>
		<hr>
	</div-->

	<!--div class="col-6">
		<p class="font-weight-bold text-left">No Invoice <?=$no_invoice;?></p>
	</div>
	<div class="col-6">
		<p class="font-weight-bold text-right">Order Date <?=$order_date;?></p>
	</div>
	<div class="col-12">
		<hr>
		<h4>Member Information</h4>
		<table class="table table-bordered table-striped table-sm">
			<tbody>
				<tr>
					<th style="width:100px;">Nama</th>
					<td><?=$nama_lengkap;?></td>
				</tr>
				<tr>
					<th>Email</th>
					<td><?=$email;?></td>
				</tr>
				<tr>
					<th>Phone</th>
					<td><?=$phone;?></td>
				</tr>
				<tr>
					<th>Alamat</th>
					<td><?=$alamat;?> - <?=$kode_pos;?></td>
				</tr>
			</tbody>
		</table>
	</div-->
	<div class="col-12">
		<hr>
		<h4>Produk</h4>
		<table class="table table-bordered table-striped table-sm">
			<thead class="thead-dark">
				<tr>
					<th>Produk</th>
					<th class="text-center">Size</th>
					<th class="text-right">Harga</th>
					<th class="text-right">Subtotal</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach($arr_produk->result() as $res){
			?>
				<tr>
					<td><?=$res->nama_produk;?></td>
					<td class="text-center"><?=strtoupper($res->size);?></td>
					<td class="text-right"><?=number_format($res->harga,0,'.',',');?></td>
					<td class="text-right"><?=number_format($res->subtotal,0,'.',',');?></td>
				</tr>
			<?php
			}
			?>
			</tbody>
			<tfoot>
				<tr>
					<th class="text-right" colspan="3">Ongkir</th>
					<th class="text-right"><?=number_format($ongkir,0,'.',',');?></th>
				</tr>
				<tr>
					<th class="text-right" colspan="3">Kode Unik</th>
					<th class="text-right"><?=number_format($kode_unik,0,'.',',');?></th>
				</tr>
				<tr>
					<th class="text-right" colspan="3">Tambahan</th>
					<th class="text-right"><?=number_format($tambahan,0,'.',',');?></th>
				</tr>
				<tr>
					<th class="text-right" colspan="3">Grand Total</th>
					<th class="text-right"><?=number_format($grand_total,0,'.',',');?></th>
				</tr>
				<tr>
					<th colspan="4">
						Catatan:<br>
						<?=$catatan;?>
					</th>
				</tr>
			</tfoot>
		</table>
		<hr>
	</div>

</div>