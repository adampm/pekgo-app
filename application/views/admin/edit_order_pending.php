<div class="container-fluid">
	<form>

		<div class="row">
			<div class="col-md-6">
				<table class="table table-sm">
					<thead>
						<tr>
							<th colspan="2" class="text-left">
								<span class="font-weight-bold h4">Order Information</span>
							</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach($arr_order->result() as $res){
					?>
						<tr>
							<th>No Invoice</th>
							<td><?=$res->no_invoice;?></td>
						</tr>
						<tr>
							<th>Sub Total</th>
							<td><?=number_format($res->sub_total,0,'.',',');?></td>
						</tr>
						<tr>
							<th>Ongkir</th>
							<td><?=number_format($res->ongkir,0,'.',',');?></td>
						</tr>
						<tr>
							<th>Kode Unik</th>
							<td><?=number_format($res->kode_unik,0,'.',',');?></td>
						</tr>
						<tr>
							<th>Grand Total</th>
							<td><?=number_format($res->grand_total,0,'.',',');?></td>
						</tr>
						<tr>
							<th>Ekspedisi</th>
							<td><?=$res->ekspedisi;?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>

			</div>

			<div class="col-md-6">
				<table class="table table-sm">
					<thead>
						<tr>
							<th colspan="2" class="text-left">
								<span class="font-weight-bold h4">Customer Information</span>
							</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach($arr_customer->result() as $res){
					?>
						<tr>
							<th style="width:100px;">Nama</th>
							<td><?=$res->nama_lengkap;?></td>
						</tr>
						<tr>
							<th>Email</th>
							<td><?=$res->email;?></td>
						</tr>
						<tr>
							<th>Phone</th>
							<td><?=$res->phone;?></td>
						</tr>
						<tr>
							<th>Alamat</th>
							<td><?=$res->alamat;?></td>
						</tr>
						<tr>
							<th>Kode POS</th>
							<td><?=$res->kode_pos;?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>

			<div class="col-md-12 text-center">
				<hr><hr>
				<span class="text-center font-weight-bold h4">Produk</span>
				<table class="table table-sm table-bordered table-striped table-hover">
					<thead class="thead-dark">
						<tr>
							<th class="mw-50"><i class="fas fa-cog"></i></th>
							<th>Nama Produk</th>
							<th class="text-right">S</th>
							<th class="text-right">M</th>
							<th class="text-right">L</th>
							<th class="text-right">XL</th>
							<th class="text-right">XXL</th>
							<th class="mw-120 text-right">Sub Total</th>
						</tr>
					</thead>
					<tbody>
						<tr>
						<?php
						foreach($arr_produk->result() as $res){
						?>
							<td class="mw-50">
								<button class="btn btn-xs btn-danger" title="Delete Produk" onClick="reduceProdukEdit(<?=$res->id_order_produk;?>);">
									<i class="fas fa-minus"></i>
								</button>
							</td>
							<td><?=$res->nama_produk;?></td>
							<td class="text-right" style="width:70px;">
								<?=number_format($res->size_s,0,'.',',');?>
							</td>
							<td class="text-right" style="width:70px;">
								<?=number_format($res->size_m,0,'.',',');?>
							</td>
							<td class="text-right" style="width:70px;">
								<?=number_format($res->size_l,0,'.',',');?>
							</td>
							<td class="text-right" style="width:70px;">
								<?=number_format($res->size_xl,0,'.',',');?>
							</td>
							<td class="text-right" style="width:70px;">
								<?=number_format($res->size_xxl,0,'.',',');?>
							</td>
							<td class="mw-120 text-right">
								<?=number_format($res->sub_total,0,'.',',');?>
							</td>
						</tr>
					<?php } ?>
					</tbody>
					<tfoot class="thead-dark">
					<?php
					foreach($arr_order->result() as $res){
					?>
						<tr>
							<th class="text-right" colspan="7">Ongkir</th>
							<th class="text-right"><?=number_format($res->ongkir,0,'.',',');?></th>
						</tr>
						<tr>
							<th class="text-right" colspan="7">Kode Unik</th>
							<th class="text-right"><?=number_format($res->kode_unik,0,'.',',');?></th>
						</tr>
						<tr>
							<th class="text-right" colspan="7">Grand Total</th>
							<th class="text-right"><?=number_format($res->grand_total,0,'.',',');?></th>
						</tr>
					<?php } ?>
					</tfoot>
				</table>
			</div>

		</div>

	</form>
</div>