<form id="edit_form" method="POST" action="admin_rekening/update" class="form">
	<div class="form-group">
		<input type="text" class="form-control" id="id_bank_edit" name="id_bank_edit" value="<?=$id_bank;?>" readonly required>
	</div>
	<div class="form-group">
		<label for="nama_bank_edit">Nama Bank</label>
		<input type="text" class="form-control" id="nama_bank_edit" name="nama_bank_edit" value="<?=$nama_bank;?>" placeholder="Nama Bank" required>
	</div>
	<div class="form-group">
		<label for="no_rekening_edit">No Rekening</label>
		<input type="text" class="form-control" id="no_rekening_edit" name="no_rekening_edit" value="<?=$no_rekening;?>" placeholder="No Rekening" required>
	</div>
	<div class="form-group">
		<label for="atas_nama_edit">Atas Nama</label>
		<input type="text" class="form-control" id="atas_nama_edit" name="atas_nama_edit" value="<?=$atas_nama;?>" placeholder="Atas Nama" required>
	</div>
</form>