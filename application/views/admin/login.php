<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Pekgo Apparel Application">
  <meta name="author" content="@manasama77">
  <link rel="shortcut icon" href="<?=base_url('assets/img/logo_fqv_icon.ico');?>" />
  <title>Admin Log In - Pekgo Apparel</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/iconfonts/mdi/css/materialdesignicons.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.base.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.addons.css');?>">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/style.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/fontawesome/css/all.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/toast/dist/jquery.toast.min.css');?>">

  <script type="text/javascript" src="<?=base_url('vendor/jquery/jquery-3.3.1.min.js');?>"></script>
  <script type="text/javascript" src="<?=base_url('vendor/fontawesome/js/all.min.js');?>"></script>
  <!-- endinject -->
</head>

<body>
<?php
if($this->session->flashdata('temp_sess'))
{
	$tipe_alert = $this->session->flashdata('temp_sess');
}else{
	$tipe_alert = 0;
}
?>
<input type="hidden" id="temp_sess" name="temp_sess" value="<?=$tipe_alert;?>">
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <!-- Background Photo by Arkadiy Parovoz from Pexels https://www.pexels.com/photo/silhouette-photo-of-trees-during-golden-hour-1470589/ -->
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-three">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
              <?=form_open('admin', $form_attr); ?>
                <div class="text-center">
              	  <img class="mb-4" src="<?=base_url('assets/img/logo.svg');?>" alt="Pekgo Apparel Logo" width="200">
                </div>
                <div class="form-group">
                  <?=form_label('Username', 'id_username_inp', $username_labl_attr);?>
                  <div class="input-group">
                    <input type="text" id="id_username_inp" name="apm_username_inp" class="form-control" placeholder="Username" value="<?=set_value('apm_username_inp');?>">
                  </div>
                </div>
                <div class="form-group">
                  <?=form_label('Password', 'id_pass_inp', $pass_labl_attr);?>
                  <div class="input-group">
                    <?=form_password($pass_attr);?>
                  </div>
                </div>
                <div class="form-group">
                  <?=form_submit($submit_attr);?>
                </div>
                <?=validation_errors('<div class="h6 form-group text-center text-danger justify-content-between text-center"><i class="fas fa-exclamation-circle"></i> ', '</div>');?>
              <?=form_close();?>
            </div>
            <p class="mb-3 text-muted text-center"></p>
            <p class="footer-text text-center text-muted">
            	<small>
            		<i class="fab fa-<?=$browser_icon;?>"></i> 
            		<?=$agent_browser.PHP_EOL;?> <?=$agent_version.PHP_EOL;?> - <i class="fas fa-desktop"></i> <?=$ip.PHP_EOL;?>
            	</small>
            	<br>
            	Copyright © 2018 Pekgo Apparel<br> 
				Powered by <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.<br> 
				Modified by <a href="https://twitter.com/adampm" target="_blank">APM Web Dev</a> - 
				Version 2.0.0
				</span>
            </p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?=base_url('vendor/star-admin/js/vendor.bundle.base.js');?>"></script>
  <script src="<?=base_url('vendor/star-admin/js/vendor.bundle.addons.js');?>"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?=base_url('vendor/star-admin/js/off-canvas.js');?>"></script>
  <script src="<?=base_url('vendor/star-admin/js/misc.js');?>"></script>
  <!-- endinject -->

<script type="text/javascript" src="<?=base_url('vendor/toast/dist/jquery.toast.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function(){
	var tempSess = $('#temp_sess').val();
	if(tempSess != 0){
		showToast(tempSess);
	}
	
});

/* -------------------------------------------------------------------------------------------- */

function showToast(tipe){
	if(tipe == 'username'){
		var heading = 'Username not found!';
		var message = 'Kamu belum terdaftar. Terima Kasih';
		var color = 'danger';
	}else if(tipe == 'pass'){
		var heading = 'Password Salah!';
		var message = 'Silahkan kamu cek kembali password kamu';
		var color = 'danger';
	}else if(tipe == 'logout'){
		var heading = 'Log out Berhasil!';
		var message = 'Terima kasih';
		var color = 'success';
	}

	$.toast({
	    text: message,
	    heading: heading,
	    icon: color,
	    showHideTransition: 'fade',
	    allowToastClose: false,
	    hideAfter: 10000,
	    stack: 1,
	    position: 'top-left',
	    textAlign: 'left',
	    loader: true,
	    loaderBg: '#9EC600',    
	});
}
</script>
</body>

</html>