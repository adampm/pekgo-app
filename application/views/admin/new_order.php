<div class="content-wrapper">
  <div class="row">

    <div class="col-lg-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Bikin Order Baru</h4>
          <?php
          if(!empty($this->session->userdata('arr_nama_produk'))){
          ?>
            <div class="alert alert-danger" role="alert">
             <?=substr($this->session->userdata('arr_nama_produk'), 0, -1);?> Stock Habis
            </div>
          <?php
            //$this->session->unset_userdata('arr_nama_produk');
          }
          ?>
          <?=form_open('admin_new_order/create_order', $form_attr); ?>
            <div class="row">
              <div class="col-md-8">
                <label class="font-weight-bold" for="id_member_new_order">Member</label>
                <select class="form-control" id="id_member_new_order" name="id_member_new_order" onChange="loadCustomerInformation();" required>
                </select>
              </div>
              <div class="col-md-4">
                <label class="font-weight-bold" for="waktu_batasan_new_order">Batas Waktu Pembayaran</label>
                <input type="text" class="form-control" id="waktu_batasan_new_order" name="waktu_batasan_new_order" required>
              </div>
            </div>
            <div class="col-md-12">
              <hr>
              <span class="h4">Customer Information</span>
              <table class="table">
                <tbody>
                  <tr>
                    <th style="width:100px;">Nama Lengkap</th>
                    <td id="nonl"></td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td id="noem"></td>
                  </tr>
                  <tr>
                    <th>Phone</th>
                    <td id="noph"></td>
                  </tr>
                  <tr>
                    <th>Alamat</th>
                    <td id="noal"></td>
                  </tr>
                  <tr>
                    <th>Kode POS</th>
                    <td id="nokp"></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-12">
              <hr>
              <span class="h4">Produk</span>
            </div>
            <div class="col-12">
              <div class="input-group input-group-sm mt-3 mb-3">
                <div class="input-group-prepend">
                 <span class="input-group-text"><i class="fas fa-tshirt"></i></span>
                </div>
                <select class="form-control col-4" id="id_produk_new_order" name="id_produk_new_order">
                </select>
                <input type="number" id="qty_new_order" name="qty_new_order" class="form-control col-2" placeholder="Qty">
                <select class="form-control col-2" id="size_new_order" name="size_new_order">
                  <option value=""></option>
                  <option value="s">S</option>
                  <option value="m">M</option>
                  <option value="l">L</option>
                  <option value="xl">XL</option>
                  <option value="xxl">XXL</option>
                </select>
                <button type="button" class="btn btn-dark btn-xs btn-flat" onClick="addProdukNewOrder();">
                  <i class="fas fa-plus"></i> Add Produk
                </button>
              </div>
            </div>
            <div class="col-12">
              <hr>
              <h4>Biaya Tambahan</h4>
              <div class="input-group">
                <input type="text" id="catatan_new_order" name="catatan_new_order" class="form-control col-4" placeholder="Catatan">
                <input type="number" id="tambahan_new_order" name="tambahan_new_order" class="form-control col-2" placeholder="Biaya Tambahan">
                <button type="button" class="btn btn-sm btn-info" onClick="biayaTambahanNewOrder();">
                  <i class="fas fa-level-down-alt"></i>
                </button>
              </div>
            </div>
            <div class="col-12">
              <hr>
              <span class="h4">Order Produk</span>
              <div class="table-responsive">
                <table class="table table-sm table-bordered table-striped">
                  <thead class="thead-dark">
                    <tr>
                      <th class="text-center"><i class="fas fa-cog"></i></th>
                      <th>Nama Produk</th>
                      <th class="text-right">Qty</th>
                      <th class="text-right">Harga</th>
                      <th class="text-center">Size</th>
                      <th class="text-right">Sub Total</th>
                    </tr>
                  </thead>
                  <tbody id="nolp">
                  </tbody>
                  <tfoot class="thead-dark" id="nofo">
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="col-12">
              <img src="https://res-5.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_256,w_256,f_auto,q_auto:eco/mtolncn9plutnkrwtvqy" width="100px">
              <br>
              <span class="h4">Jenis Pengiriman Sicepat Express</span>
            </div>
            <div class="col-12">
              <div class="input-group input-group-sm mt-3 mb-3">
                <div class="input-group-prepend">
                 <span class="input-group-text"><i class="fas fa-truck"></i></span>
                </div>
                <select class="form-control col-8" id="ekspedisi_new_order" name="ekspedisi_new_order" onChange="generateGrandTotal();" required disabled>
                </select>
              </div>
            </div>
            <div class="col-12">
              <hr>
              <input type="hidden" class="form-control" id="hidden_ongkir" name="hidden_ongkir" value="0" required>
              <input type="hidden" class="form-control" id="hidden_catatan" name="hidden_catatan" value="-" required>
              <input type="hidden" class="form-control" id="hidden_tambahan" name="hidden_tambahan" value="0" required>
              <input type="hidden" class="form-control" id="hidden_ekspedisi" name="hidden_ekspedisi" value="" required>
              <button type="submit" class="btn btn-primary btn-block">Save</button>
            </div>
          <?=form_close();?>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- content-wrapper ends -->