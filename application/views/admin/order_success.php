<div class="content-wrapper">

  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table id="list_order_success" class="table table-bordered table-sm" style="width:100%;">
              <thead>
                <tr>
                  <th class="text-center" style="width:30px;">#</th>
                  <th class="text-center" style="width:80px;"><i class="fas fa-cog"></i></th>
                  <th class="text-left">Member</th>
                  <th class="text-center">No Invoice</th>
                  <th class="text-right">Sub Total</th>
                  <th class="text-right">Ongkir</th>
                  <th class="text-right">Tambahan</th>
                  <th class="text-right">Kode Unik</th>
                  <th class="text-right">Grand Total</th>
                  <th class="text-center">Ekspedisi</th>
                  <th class="text-center">Created Date</th>
                  <th class="text-center">Updated Date</th>
                  <th class="text-center">Created By</th>
                  <th class="text-center">Updated By</th>
                  <th class="text-center">Status Print</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div> 
  </div> 

</div> 
<!-- content-wrapper ends -->

<!-- Modal -->
<div class="modal fade" id="detailOrderSuccessModal" tabindex="-1" role="dialog" aria-labelledby="detailOrderuccessModalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detailOrderSuccessModalTitle">Detail Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="load_detail_order">...</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onClick="printOrderSuccess()">
          <i class="fas fa-print"></i> Print
        </button>
      </div>
    </div>
  </div>
</div>