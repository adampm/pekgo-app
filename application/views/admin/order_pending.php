<div class="content-wrapper">

  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table id="list_order_pending" class="table table-bordered table-sm" style="width:100%;">
              <thead>
                <tr>
                  <th class="text-center" style="width:30px;">#</th>
                  <th class="text-center" style="width:80px;"><i class="fas fa-cog"></i></th>
                  <th class="text-left">Member</th>
                  <th class="text-center">No Invoice</th>
                  <th class="text-right">Sub Total</th>
                  <th class="text-right">Ongkir</th>
                  <th class="text-right">Tambahan</th>
                  <th class="text-right">Kode Unik</th>
                  <th class="text-right">Grand Total</th>
                  <th class="text-center">Ekspedisi</th>
                  <th class="text-center">Created Date</th>
                  <th class="text-center">Created By</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div> 
  </div> 

</div> 
<!-- content-wrapper ends -->

<!-- Modal -->
<div class="modal fade" id="editOrderPendingModal" tabindex="-1" role="dialog" aria-labelledby="editOrderPendingModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editOrderPendingModalTitle">Edit Order Pending</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="editOrderPendingModalData">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirmOrderPendingModal" tabindex="-1" role="dialog" aria-labelledby="confirmOrderPendingModalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmOrderPendingModalTitle">Manual Confirm Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="confirm_payment" class="form" method="post" action="<?=site_url('admin_order_pending/save_confirm');?>">
        <div class="modal-body" id="confirmOrderPendingModalData">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="detailOrderPendingModal" tabindex="-1" role="dialog" aria-labelledby="detailOrderPendingModalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detailOrderPendingModalTitle">Detail Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="load_detail_order">...</div>
      </div>
    </div>
  </div>
</div>