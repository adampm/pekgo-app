<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">List Rekening</h4>
          <!--div class="table-responsive"-->
            <table id="list_rekening" class="table table-bordered">
              <thead>
                <tr>
                  <th class="text-center" style="width:30px;">#</th>
                  <th class="text-center" style="width:70px;">Bank</th>
                  <th class="text-left">No Rekening</th>
                  <th class="text-left">Atas Nama</th>
                  <th class="text-center" style="width:70px;"><i class="fas fa-cog"></i></th>
                </tr>
              </thead>
            </table>
          <!--/div-->
        </div>
      </div>
    </div>

    <div class="col-lg-4 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Tambah Rekening Baru</h4>
          <?=form_open('admin_rekening/add', $form_attr); ?>
            <div class="form-group">
              <label for="no_rekening">No Rekening</label>
              <input type="text" id="no_rekening" name="no_rekening" class="form-control" value="<?=set_value('no_rekening');?>" placeholder="No Rekening">
              <div class="help-block text-danger text-left">
                <?php echo form_error('no_rekening'); ?>
              </div>
            </div>
            <div class="form-group">
              <label for="no_rekening">Nama Bank</label>
              <input type="text" id="nama_bank" name="nama_bank" class="form-control" value="<?=set_value('nama_bank');?>" placeholder="Nama Bank">
              <div class="help-block text-danger text-left">
                <?php echo form_error('nama_bank'); ?>
              </div>
            </div>
            <div class="form-group">
              <label for="no_rekening">Atas Nama</label>
              <input type="text" id="atas_nama" name="atas_nama" class="form-control" value="<?=set_value('atas_nama');?>" placeholder="Atas Nama">
              <div class="help-block text-danger text-left">
                <?php echo form_error('atas_nama'); ?>
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">Save</button>
            </div>
          <?=form_close();?>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- content-wrapper ends -->

<!-- Modal -->
<div class="modal fade" id="edit_rekening_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edit_rekening_label">Edit Rekening</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="edit_rekening_data"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit_rekening_save">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->