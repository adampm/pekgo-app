<?php
foreach($this->cart->contents() as $res){
?>
<tr>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-danger" onClick="deleteProdukOrder('<?=$res['rowid'];?>')" title="Delete <?=$res['id'];?>">
			<i class="fas fa-minus"></i>
		</button>
	</td>
	<td><?=$res['name'];?></td>
	<td class="text-right"><?=number_format($res['qty'],0,',','.');?></td>
	<td class="text-right"><?=number_format($res['price'],0,',','.');?></td>
	<td class="text-center"><?=strtoupper($res['size']);?></td>
	<td class="text-right"><?=number_format($res['subtotal'],0,',','.');?></td>
</tr>
<?php
}
?>