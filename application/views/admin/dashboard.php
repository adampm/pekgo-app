<div class="content-wrapper">
  <div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="fas fa-money-bill text-primary icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Total Belanja</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">Rp. </h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="fas fa-receipt text-success icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Orders Success</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="fas fa-receipt text-default icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Order Pending</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="fas fa-receipt text-danger icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Order Cancel</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0"></h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Last 10 Orders</h4>
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th class="text-center" style="width:50px;">#</th>
                  <th class="text-left">Date Order</th>
                  <th class="text-center">Invoice</th>
                  <th class="text-right">Grand Total</th>
                  <th class="text-right">Qty</th>
                  <th class="text-center">Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="text-center"></td>
                  <td class="text-left"></td>
                  <td class="text-center">
                      <button class="btn btn-primary btn-flat btn-block btn-xs">
                        
                      </button  >
                  </td>
                  <td class="text-right">Rp. </td>
                  <td class="text-right"> Pcs</td>
                  <td class="text-center">
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer">
          <a href="#" class="btn btn-info">View More</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->