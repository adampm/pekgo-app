<div class="content-wrapper">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#tab_list">List Produk</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#tab_add">Add Produk</a>
    </li>
  </ul>

  <div class="row">
    <div class="col-lg-12">
      <div class="card">

        <div class="tab-content">

          <div class="tab-pane container active" id="tab_list">
            <div class="card-body">
              <div class="table-responsive">
                <table id="list_produk" class="table table-bordered" style="width:100%;">
                  <thead>
                    <tr>
                      <th class="text-center" style="width:30px;">#</th>
                      <th class="text-left">Nama Produk</th>
                      <th class="text-left">Kategori</th>
                      <th class="text-left">Berat</th>
                      <th class="text-left">Harga</th>
                      <th class="text-center" style="width:70px;"><i class="fas fa-cog"></i></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>

          <div class="tab-pane container fade" id="tab_add">
            <div class="card-body">
              <?=form_open('admin_produk', $form_attr); ?>
                <div class="form-group">
                  <label for="nama_produk">Nama Produk</label>
                  <input type="text" id="nama_produk" name="nama_produk" class="form-control" value="<?=set_value('nama_produk');?>" placeholder="Nama Produk">
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('nama_produk'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="id_kategori">Kategori</label>
                  <select id="id_kategori" name="id_kategori" class="form-control">
                    <option value="">- Pilih Kategori -</option>
                  <?php
                  foreach($list_kategori->result() as $res_kategori){
                  ?>
                    <option value="<?=$res_kategori->id_kategori;?>" <?=set_value('id_kategori', $res_kategori->id_kategori);?>>
                      <?=$res_kategori->kategori;?>
                    </option>
                  <?php
                  }
                  ?>
                  </select>
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('id_kategori'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="berat">Berat</label>
                  <input type="number" id="berat" name="berat" class="form-control" value="<?=set_value('berat');?>" placeholder="Berat Produk">
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('berat'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="harga">Harga</label>
                  <input type="number" id="harga" name="harga" class="form-control" value="<?=set_value('harga');?>" placeholder="Harga">
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('harga'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block">Save</button>
                </div>
              <?=form_close();?>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>

</div>
<!-- content-wrapper ends -->

<!-- Modal -->
<div class="modal fade" id="edit_produk_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edit_label">Edit Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="edit_produk_data"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit_produk_save">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->