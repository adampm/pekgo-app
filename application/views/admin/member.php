<div class="content-wrapper">

  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table id="list_member" class="table table-bordered" style="width:100%;">
              <thead>
                <tr>
                  <th class="text-center" style="width:30px;">#</th>
                  <th class="text-left">Nama Lengkap</th>
                  <th class="text-left">Email</th>
                  <th class="text-left">Phone</th>
                  <th class="text-left">Birth Day</th>
                  <th class="text-center">Province</th>
                  <th class="text-center">City</th>
                  <th class="text-center">District</th>
                  <th class="text-left">Created Date</th>
                  <th class="text-left">Last Login</th>
                  <th class="text-left">Status</th>
                  <th class="text-center" style="width:50px;"><i class="fas fa-cog"></i></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div> 
  </div> 

</div> 
<!-- content-wrapper ends -->