<?php
$total_berat = 0;
$grand_total = 0;
foreach($this->cart->contents() as $res){
	$grand_total += $res['subtotal'];
	$total_berat += $res['berat'] * $res['qty'];
}
$grand_total = $grand_total + $ongkir + $tambahan;
?>
<tr>
	<th class="text-right" colspan="5">Ongkir</th>
	<th class="text-right">
		<?=number_format($ongkir,0,',','.');?>
	</th>
</tr>
<tr>
	<th class="text-right" colspan="5">Tambahan</th>
	<th class="text-right">
		<?=number_format($tambahan,0,',','.');?>
	</th>
</tr>
<tr>
	<th class="text-right" colspan="5">Grand Total</th>
	<th class="text-right">
		<?=number_format($grand_total,0,',','.');?>
		<input type="hidden" id="hidden_berat" name="hidden_berat" value="<?=$total_berat;?>">
	</th>
</tr>
<tr>
	<th colspan="6">
		Catatan<br>
		<?=$catatan;?>
	</th>
</tr>
