<form id="edit_form" method="POST" action="admin_kategori/update" class="form">
	<div class="form-group">
		<input type="text" class="form-control" id="id_kategori_edit" name="id_kategori_edit" value="<?=$id_kategori;?>" readonly required>
	</div>
	<div class="form-group">
		<label for="kategori">Kategori</label>
		<input type="text" class="form-control" id="kategori_edit" name="kategori_edit" value="<?=$kategori;?>" placeholder="Nama Kategori" required>
	</div>
	<div class="form-group">
		<label for="kategori">Status</label>
		<select class="form-control" id="status_edit" name="status_edit" required>
			<option value="1" <?=$selected1;?>>Show</option>
			<option value="0" <?=$selected0;?>>Hide</option>
		</select>
	</div>
</form>