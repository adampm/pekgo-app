<div class="content-wrapper">

  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table id="list_stock_log" class="table table-bordered" style="width:100%;">
              <thead>
                <tr>
                  <th class="text-center" style="width:30px;">#</th>
                  <th class="text-left">Tanggal</th>
                  <th class="text-left">Nama Produk</th>
                  <th class="text-left">Tipe</th>
                  <th class="text-left">Size</th>
                  <th class="text-left">Qty</th>
                  <th class="text-left">No Invoice</th>
                  <th class="text-left">Admin</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div> 
  </div> 

</div> 
<!-- content-wrapper ends -->