<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-8">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">List Kategori</h4>
          <!--div class="table-responsive"-->
            <table id="list_kategori" class="table table-bordered">
              <thead>
                <tr>
                  <th class="text-center" style="width:30px;">#</th>
                  <th class="text-left">Kategori</th>
                  <th class="text-center" style="width:70px;">Status</th>
                  <th class="text-center" style="width:70px;"><i class="fas fa-cog"></i></th>
                </tr>
              </thead>
            </table>
          <!--/div-->
        </div>
      </div>
    </div>

    <div class="col-lg-4 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Tambah Kategori</h4>
          <?=form_open('admin_kategori/add', $form_attr); ?>
            <div class="form-group">
              <label for="">Kategori</label>
              <input type="text" id="kategori" name="kategori" class="form-control" value="<?=set_value('kategori');?>" placeholder="Nama Kategori">
              <div class="help-block text-danger text-left">
                <?php echo form_error('kategori'); ?>
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">Save</button>
            </div>
          <?=form_close();?>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- content-wrapper ends -->

<!-- Modal -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="edit_label">Edit Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="edit_data"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit_save">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->