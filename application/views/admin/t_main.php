<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Pekgo Apparel Application">
  <meta name="author" content="@manasama77">
  <link rel="icon" href="<?=base_url('assets/img/logo_fqv_icon.ico');?>">
  <title><?=$title;?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/iconfonts/mdi/css/materialdesignicons.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.base.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.addons.css');?>">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/style.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/fontawesome/css/all.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/toast/dist/jquery.toast.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/datetime/jquery.datetimepicker.min.css');?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/fc-3.2.5/fh-3.1.4/rg-1.1.0/datatables.min.css"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <!-- endinject -->
  <style>
  </style>
</head>

<body>
  <div class="container-scroller">
    <?php $this->load->view('admin/t_navbar_top'); ?>
    <div class="container-fluid page-body-wrapper">
      <?php $this->load->view('admin/t_navbar_sidebar'); ?>
      <div class="main-panel">
        <?php $this->load->view('admin/'.$page);?>
        <?php $this->load->view('admin/t_footer');?>
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <!--script src="<?=base_url('vendor/popper/popper.min.js');?>"></script-->
  <script src="<?=base_url('vendor/star-admin/js/vendor.bundle.base.js');?>"></script>
  <script src="<?=base_url('vendor/star-admin/js/vendor.bundle.addons.js');?>"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?=base_url('vendor/star-admin/js/off-canvas.js');?>"></script>
  <script src="<?=base_url('vendor/star-admin/js/misc.js');?>"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!--script src="<?=base_url('vendor/star-admin/js/dashboard.js');?>"></script-->
  <!--script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script-->
  <!--script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script-->
  <script type="text/javascript" src="<?=base_url('vendor/fontawesome/js/all.min.js');?>"></script>
  <?php
  if($this->session->flashdata('temp_sess'))
  {
    $tipe_alert = $this->session->flashdata('temp_sess');
  }else{
    $tipe_alert = 0;
  }
  ?>
  <input type="hidden" id="temp_sess" name="temp_sess" value="<?=$tipe_alert;?>">
  <script type="text/javascript" src="<?=base_url('vendor/toast/dist/jquery.toast.min.js');?>"></script>
  <script type="text/javascript" src="<?=base_url('vendor/datetime/jquery.datetimepicker.full.min.js');?>"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/fc-3.2.5/fh-3.1.4/rg-1.1.0/datatables.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.14.0/printThis.min.js"></script>


  <!-- End custom js for this page-->
  <script>
    // Adam Ganteng :v
    $(document).ready(function(){
      $('#waktu_batasan_new_order').datetimepicker({
        format:'Y-m-d H:i:s',
        lang:'id',
        closeOnDateSelect:true
      });

      $('#id_member_new_order').select2({
        theme: "bootstrap",
        placeholder: "Pilih Member"
      });

      $.ajax({
        url: '<?=site_url('admin_new_order/json_member');?>',
        dataType: 'json'
      })
      .done(function(result){
        $('#id_member_new_order').empty();
        $('#id_member_new_order').append('<option value=""></option>');
        $.each(result, function(key, value) {
          $('#id_member_new_order').append('<option value='+ value.id +'>'+ value.text +'</option>');
        });
      });

      $('#id_produk_new_order').select2({
        theme: "bootstrap",
        placeholder: "Pilih Produk",
        allowClear: true,
        ajax: {
          url: '<?=site_url('admin_new_order/json_produk');?>',
          dataType: 'json',
          processResults: function(data){
            return {
              results: data
            }
          },
          cache: true
        }
      });

      $('#size_new_order').select2({
        theme: "bootstrap",
        placeholder: "Pilih Size",
        allowClear: true
      });

      var tempSess = $('#temp_sess').val();

      var renderTableKategori = generateTableKategori();
      var renderTableProduk = generateTableProduk();
      var renderTableRekening = generateTableRekening();
      var renderTableStock = generateTableStock();
      var renderTableStockLog = generateTableStockLog();
      var renderTableMember = generateTableMember();
      var renderTableOrderSuccess = generateTableOrderSuccess();
      var renderTableOrderCancel = generateTableOrderCancel();
      var renderTableOrderPending = generateTableOrderPending();
      loadProdukInformation();
      loadProdukFooterInformation();

      if(tempSess != 0){ showToast(tempSess); }

    });

    function showToast(tipe){
      if(tipe == 'firstlogin'){
        var heading = 'Login Berhasil!';
        var message = 'Pastikan kamu menjaga Username & Password kamu. Terima Kasih.';
        var color = 'success';
      }else if(tipe == 'error'){
        var heading = 'Registrasi Gagal!';
        var message = 'Silahkan Coba Kembali. Terima Kasih.';
        var color = 'danger';
      }else if(tipe == 'successkategoriadd'){
        var heading = 'Berhasil!';
        var message = 'Tambah Kategori Berhasil.';
        var color = 'success';
      }else if(tipe == 'errorkategoriadd'){
        var heading = 'Gagal!';
        var message = 'Tambah Kategori Gagal, silahkan coba kembali.';
        var color = 'danger';
      }else if(tipe == 'successprodukadd'){
        var heading = 'Berhasil!';
        var message = 'Tambah Produk Berhasil.';
        var color = 'success';
      }else if(tipe == 'errorprodukadd'){
        var heading = 'Gagal!';
        var message = 'Tambah Produk Gagal, silahkan coba kembali.';
        var color = 'danger';
      }else if(tipe == 'successbankadd'){
        var heading = 'Berhasil!';
        var message = 'Tambah Rekening Berhasil.';
        var color = 'success';
      }else if(tipe == 'errorbankadd'){
        var heading = 'Gagal!';
        var message = 'Tambah Rekening Gagal, silahkan coba kembali.';
        var color = 'danger';
      }else if(tipe == 'successstockadd'){
        var heading = 'Berhasil!';
        var message = 'Manage Stock Berhasil.';
        var color = 'success';
      }else if(tipe == 'errorstockadd'){
        var heading = 'Gagal!';
        var message = 'Manage Stock Gagal, silahkan coba kembali.';
        var color = 'danger';
      }else if(tipe == 'confirmpaymentsuccess'){
        var heading = 'Berhasil!';
        var message = 'Confirm Payment Berhasil.';
        var color = 'success';
      }else if(tipe == 'confirmpaymentfailed'){
        var heading = 'Gagal!';
        var message = 'Confirm Payment Gagal, silahkan coba kembali.';
        var color = 'danger';
      }else if(tipe == 'stockprodukorderhabis'){
        var heading = 'Something Wrong!';
        var message = 'Stock Produk Habis, order tidak dapat dibuat.';
        var color = 'warning';
      }else if(tipe == 'createorderberhasil'){
        var heading = 'Success!';
        var message = 'Create Order Berhasl.';
        var color = 'success';
      }

    generateToast(heading, message, color);

  }

  function generateToast(heading, message, color){
    $.toast({
        text: message,
        heading: heading,
        icon: color,
        showHideTransition: 'fade',
        allowToastClose: false,
        hideAfter: 10000,
        stack: 5,
        position: 'top-left',
        textAlign: 'left',
        loader: true,
        loaderBg: '#9EC600',    
    });
  }
  </script>

  <!-- KATEGORI SCRIPT -->
  <script>
    function generateTableKategori(){
    var renderTableKategori = $('#list_kategori').dataTable({
      "pageLength": 10,
      "dom": '<"top"f>rt<"bottom"lip>',
      "lengthMenu": [ [10,25,50,-1], [ '10', '25', '50', 'All' ] ],
      "processing": true,
      "serverSide": true,
      "ajax"      : {
        url     : '<?=site_url('admin_kategori/json_kategori');?>',
        type    : 'GET',
        dataSrc : 'data'
      },
      "columns" :[
        { data: "no" },
        { data: "kategori" },
        { 
          data: null,
          searchable: true,
          render : function (data){
            var status = '';
            if(data.status == 1){ 
              status = '<span class="badge badge-success">Show</span>'; 
            }else{ 
              status = '<span class="badge badge-warning">Hide</span>'; 
            }
            return status;
          }
        },
        {
          data: null,
          searchable: false,
          orderable: false,
          render : function(data){
            var renderButton;
            renderButton = '<div class="btn-group">';
              renderButton += '<button onClick="editKategori('+data.id_kategori+');" class="btn btn-info btn-xs" title="Edit">';
               renderButton += '<i class="fas fa-edit"></i>';
              renderButton += '</button>';
              renderButton += '<button onClick="deleteKategori('+data.id_kategori+',\''+data.kategori+'\');"  class="btn btn-danger btn-xs" title="Delete">';
                renderButton += '<i class="fas fa-trash"></i>';
              renderButton += '</button>';
            renderButton += '</div>';
            return renderButton;
          }
        }
      ],
      "columnDefs" :[{
        "targets" : [ 0, 2, 3 ],
        "className" : 'text-center'
      }]
    });

    return renderTableKategori;
  }

  function reloadTableKategori(){
    $('#list_kategori').dataTable().api().ajax.reload();
  }

  function editKategori(id){
    $.ajax({
      url : '<?=site_url('admin_kategori/edit');?>',
      type : 'GET',
      data : { id : id }
    })
    .done(function(result){
      $('#edit_data').empty();
      $('#edit_data').html(result);
    });

    $('#edit_modal').modal('show');

    $('#edit_save').on('click', function(){
      var id_kategori = $('#id_kategori_edit').val();
      var kategori = $('#kategori_edit').val();
      var status = $('#status_edit').val();
      $.ajax({
        url : '<?=site_url('admin_kategori/update');?>',
        type : 'POST',
        data : { 
          id_kategori : id_kategori,
          kategori : kategori,
          status : status,
        }
      })
      .done(function(result){
        $('#edit_data').empty();
        $('#edit_modal').modal('hide');
        var heading = 'Success!';
        var message = 'Edit Kategori Berhasil.';
        var color = 'success';
        generateToast(heading, message, color);
        reloadTableKategori();
      });
    });
  }

  function deleteKategori(id, kategori){
    $.confirm({
      title: 'Delete!',
      content: 'Apakah kamu ingin menghapus Data <span class="text-danger font-weight-bold">'+ kategori +'</span> ?',
      type: 'red',
      typeAnimated: true,
      columnClass: 'medium',
      draggable: true,
      theme: 'material',
      buttons: {
        confirm: {
          text: 'Hapus',
          btnClass: 'btn-danger',
          action: function(){
            // action delete
            $.ajax({
              url : '<?=site_url('admin_kategori/delete');?>',
              type : 'GET',
              data : { 
                id : id
              },
              statusCode: {
                500: function(response){
                  var heading = 'Failed!';
                  var message = 'Delete Kategori Gagal dikarenkan ada Produk yang terkait Kategori '+ kategori +'.';
                  var color = 'danger';
                  generateToast(heading, message, color);
                  reloadTableKategori();
                }
              }
            })
            .done(function(result){
              if(result == 'berhasil'){
                var heading = 'Success!';
                var message = 'Delete Kategori Berhasil.';
                var color = 'success';
                generateToast(heading, message, color);
                reloadTableKategori();
              }else{
                var heading = 'Failed!';
                var message = 'Delete Kategori gagal dikarenkan ada produk yang terkait kategori '+ kategori +'.';
                var color = 'danger';
                generateToast(heading, message, color);
                reloadTableKategori();
              }
              
            });
          }
        },
        cancel: function () {
            // NOTHING
        }
      }
    });
  }
  </script>

  <!-- PRODUK SCRIPT -->
  <script>
  function generateTableProduk(){
    var renderTableProduk = $('#list_produk').dataTable({
      "pageLength": 10,
      "dom": '<"top"f>rt<"bottom"lip>',
      "lengthMenu": [ [10,25,50,-1], [ '10', '25', '50', 'All' ] ],
      "processing": true,
      "serverSide": true,
      "ajax"      : {
        url     : '<?=site_url('admin_produk/json_produk');?>',
        type    : 'GET',
        dataSrc : 'data'
      },
      "columns" :[
        { data: "no" },
        { data: "nama_produk" },
        { data: "kategori" },
        { data: "berat" },
        { data: "harga" },
        {
          data: null,
          searchable: false,
          orderable: false,
          render : function(data){
            var renderButton;
            renderButton = '<div class="btn-group">';
              renderButton += '<button onClick="editProduk('+data.id_produk+');" class="btn btn-info btn-xs" title="Edit">';
               renderButton += '<i class="fas fa-edit"></i>';
              renderButton += '</button>';
              renderButton += '<button onClick="deleteProduk('+data.id_produk+',\''+data.nama_produk+'\');"  class="btn btn-danger btn-xs" title="Delete">';
                renderButton += '<i class="fas fa-trash"></i>';
              renderButton += '</button>';
            renderButton += '</div>';
            return renderButton;
          }
        }
      ],
      "columnDefs" :[
        {
          "targets" : [ 0, 2, 5 ],
          "className" : 'text-center'
        },
        {
          "targets" : [ 3, 4 ],
          "className" : 'text-right'
        }
      ]
    });

    return renderTableProduk;
  }

  function reloadTableProduks(){
    $('#list_produk').dataTable().api().ajax.reload();
  }

  function editProduk(id){
    $.ajax({
      url : '<?=site_url('admin_produk/edit');?>',
      type : 'GET',
      data : { id : id }
    })
    .done(function(result){
      $('#edit_produk_data').empty();
      $('#edit_produk_data').html(result);
    });

    $('#edit_produk_modal').modal('show');

    $('#edit_produk_save').on('click', function(){
      var id_produk_edit = $('#id_produk_edit').val();
      var nama_produk_edit = $('#nama_produk_edit').val();
      var id_kategori_edit = $('#id_kategori_edit').val();
      var berat_edit = $('#berat_edit').val();
      var harga_edit = $('#harga_edit').val();
      $.ajax({
        url : '<?=site_url('admin_produk/update');?>',
        type : 'POST',
        data : { 
          id_produk_edit : id_produk_edit,
          nama_produk_edit : nama_produk_edit,
          id_kategori_edit : id_kategori_edit,
          berat_edit : berat_edit,
          harga : harga_edit
        }
      })
      .done(function(result){
        $('#edit_produk_data').empty();
        $('#edit_produk_modal').modal('hide');
        var heading = 'Success!';
        var message = 'Edit Produk Berhasil.';
        var color = 'success';
        generateToast(heading, message, color);
        reloadTableProduks();
      });
    });
  }

  function deleteProduk(id, nama_produk){
    $.confirm({
      title: 'Delete!',
      content: 'Apakah kamu ingin menghapus Data <span class="text-danger font-weight-bold">'+ nama_produk +'</span> ?',
      type: 'red',
      typeAnimated: true,
      columnClass: 'medium',
      draggable: true,
      theme: 'material',
      buttons: {
        confirm: {
          text: 'Hapus',
          btnClass: 'btn-danger',
          action: function(){
            // action delete
            $.ajax({
              url : '<?=site_url('admin_produk/delete');?>',
              type : 'GET',
              data : { 
                id : id
              },
              statusCode: {
                500: function(response){
                  var heading = 'Failed1!';
                  var message = 'Delete Produk Gagal dikarenkan ada Order yang terkait Produk '+ nama_produk +'.';
                  var color = 'danger';
                  generateToast(heading, message, color);
                  reloadTableProduks();
                }
              }
            })
            .done(function(result){
              console.log(result);
              if(result == 'berhasil'){
                var heading = 'Success!';
                var message = 'Delete Produk Berhasil.';
                var color = 'success';
                generateToast(heading, message, color);
                reloadTableProduks();
              }else{
                var heading = 'Failed2!';
                var message = 'Delete Produk Gagal dikarenkan ada Order yang terkait Produk '+ nama_produk +'.';
                var color = 'danger';
                generateToast(heading, message, color);
                reloadTableProduks();
              }
              
            });
          }
        },
        cancel: function () {
            // NOTHING
        }
      }
    });
  }

  </script>

  <!-- REKENING SCRIPT -->
  <script>
  function generateTableRekening(){
    var renderTableRekening = $('#list_rekening').dataTable({
      "pageLength": 10,
      "dom": '<"top"f>rt<"bottom"lip>',
      "lengthMenu": [ [10,25,50,-1], [ '10', '25', '50', 'All' ] ],
      "processing": true,
      "serverSide": true,
      "ajax"      : {
        url     : '<?=site_url('admin_rekening/json_rekening');?>',
        type    : 'GET',
        dataSrc : 'data'
      },
      "columns" :[
        { data: "no" },
        { data: "nama_bank" },
        { data: "no_rekening" },
        { data: "atas_nama" },
        {
          data: null,
          searchable: false,
          orderable: false,
          render : function(data){
            var renderButton;
            renderButton = '<div class="btn-group">';
              renderButton += '<button onClick="editRekening('+data.id_bank+');" class="btn btn-info btn-xs" title="Edit">';
               renderButton += '<i class="fas fa-edit"></i>';
              renderButton += '</button>';
              renderButton += '<button onClick="deleteRekening('+data.id_bank+',\''+data.no_rekening+'\');"  class="btn btn-danger btn-xs" title="Delete">';
                renderButton += '<i class="fas fa-trash"></i>';
              renderButton += '</button>';
            renderButton += '</div>';
            return renderButton;
          }
        }
      ],
      "columnDefs" :[{
        "targets" : [ 0, 4 ],
        "className" : 'text-center'
      }]
    });

    return renderTableRekening;
  }

  function reloadTableRekening(){
    $('#list_rekening').dataTable().api().ajax.reload();
  }

  function editRekening(id){
    $.ajax({
      url : '<?=site_url('admin_rekening/edit');?>',
      type : 'GET',
      data : { id : id }
    })
    .done(function(result){
      $('#edit_rekening_data').empty();
      $('#edit_rekening_data').html(result);
    });

    $('#edit_rekening_modal').modal('show');

    $('#edit_rekening_save').on('click', function(){
      var id_bank_edit = $('#id_bank_edit').val();
      var nama_bank_edit = $('#nama_bank_edit').val();
      var no_rekening_edit = $('#no_rekening_edit').val();
      var atas_nama_edit = $('#atas_nama_edit').val();
      $.ajax({
        url : '<?=site_url('admin_rekening/update');?>',
        type : 'POST',
        data : { 
          id_bank_edit : id_bank_edit,
          nama_bank_edit : nama_bank_edit,
          no_rekening_edit : no_rekening_edit,
          atas_nama_edit : atas_nama_edit
        }
      })
      .done(function(result){
        $('#edit_rekening_data').empty();
        $('#edit_rekening_modal').modal('hide');
        var heading = 'Success!';
        var message = 'Edit Rekening Berhasil.';
        var color = 'success';
        generateToast(heading, message, color);
        reloadTableRekening();
      });
    });
  }

  function deleteRekening(id, no_rekening){
    $.confirm({
      title: 'Delete!',
      content: 'Apakah kamu ingin menghapus Data <span class="text-danger font-weight-bold">'+ no_rekening +'</span> ?',
      type: 'red',
      typeAnimated: true,
      columnClass: 'medium',
      draggable: true,
      theme: 'material',
      buttons: {
        confirm: {
          text: 'Hapus',
          btnClass: 'btn-danger',
          action: function(){
            // action delete
            $.ajax({
              url : '<?=site_url('admin_rekening/delete');?>',
              type : 'GET',
              data : { 
                id : id
              },
              statusCode: {
                500: function(response){
                  var heading = 'Failed1!';
                  var message = 'Delete Rekening Gagal dikarenkan ada Order & Pembayaran yang terkait No Rekening '+ no_rekening +'.';
                  var color = 'danger';
                  generateToast(heading, message, color);
                  reloadTableRekening();
                }
              }
            })
            .done(function(result){
              if(result == 'berhasil'){
                var heading = 'Success!';
                var message = 'Delete Rekening Berhasil.';
                var color = 'success';
                generateToast(heading, message, color);
                reloadTableRekening();
              }else{
                var heading = 'Failed2!';
                var message = 'Delete Rekening Gagal dikarenkan ada Order & Pembayaran yang terkait No Rekening '+ no_rekening +'.';
                var color = 'danger';
                generateToast(heading, message, color);
                reloadTableRekening();
              }
              
            });
          }
        },
        cancel: function () {
            // NOTHING
        }
      }
    });
  }

  </script>

  <!-- Stock SCRIPT -->
  <script>
  function generateTableStock(){
    var renderTableStock = $('#list_stock').dataTable({
      "pageLength": 10,
      "dom": '<"top"f>rt<"bottom"lip>',
      "lengthMenu": [ [10,25,50,-1], [ '10', '25', '50', 'All' ] ],
      "processing": true,
      "serverSide": true,
      "ajax"      : {
        url     : '<?=site_url('admin_stock/json_stock');?>',
        type    : 'GET',
        dataSrc : 'data'
      },
      "columns" :[
        { data: "no" },
        { data: "nama_produk" },
        { data: "stock_s" },
        { data: "stock_m" },
        { data: "stock_l" },
        { data: "stock_xl" },
        { data: "stock_xxl" }
      ],
      "columnDefs" :[
        {
          "targets" : [ 0 ],
          "className" : 'text-center'
        },
        {
          "targets" : [ 2,3,4,5,6 ],
          "className" : 'text-right'
        },
      ]
    });

    return renderTableStock;
  }

  function reloadTableStock(){
    $('#list_stock').dataTable().api().ajax.reload();
  }
  </script>

  <!-- Stock LOG SCRIPT -->
  <script>
  function generateTableStockLog(){
    var renderTableStockLog = $('#list_stock_log').dataTable({
      "pageLength": 10,
      "dom": '<"top"f>rt<"bottom"lip>',
      "lengthMenu": [ [10,25,50,-1], [ '10', '25', '50', 'All' ] ],
      "processing": true,
      "serverSide": true,
      "ajax"      : {
        url     : '<?=site_url('admin_stock_log/json_stock_log');?>',
        type    : 'GET',
        dataSrc : 'data'
      },
      "columns" :[
        { data: "no" },
        { data: "tanggal" },
        { data: "nama_produk" },
        { data: "tipe" },
        { data: "size" },
        { data: "qty" },
        { data: "no_invoice" },
        { data: "admin" },
      ],
      "columnDefs" :[
        {
          "targets" : [ 0,1,3,4,6,7 ],
          "className" : 'text-center'
        },
        {
          "targets" : [ 5 ],
          "className" : 'text-right'
        },
      ]
    });

    return renderTableStockLog;
  }

  function reloadTableStockLog(){
    $('#list_stock_log').dataTable().api().ajax.reload();
  }
  </script>

  <!-- Member SCRIPT -->
  <script>
  function generateTableMember(){
    var renderTableMember = $('#list_member').dataTable({
      "pageLength": 10,
      "dom": '<"top"f>rt<"bottom"lip>',
      "lengthMenu": [ [10,25,50,-1], [ '10', '25', '50', 'All' ] ],
      "processing": true,
      "serverSide": true,
      "ajax"      : {
        url     : '<?=site_url('admin_member/json_member');?>',
        type    : 'GET',
        dataSrc : 'data'
      },
      "columns" :[
        { data: "no" },
        { data: "nama_lengkap" },
        { data: "email" },
        { data: "phone" },
        { data: "birth_day" },
        { data: "id_province" },
        { data: "id_city" },
        { data: "id_district" },
        { data: "created_date" },
        { data: "last_login" },
        { 
          data: null,
          render: function(data){

            if(data.status == 'Not Verified'){
              return '<span class="badge badge-dark">'+ data.status +'</span>';
            }else if(data.status == 'Active'){
              return '<span class="badge badge-success">'+ data.status +'</span>';
            }else{
              return '<span class="badge badge-danger">'+ data.status +'</span>';
            }
          }
        },
        {
          data: null,
          render: function(data){
            if(data.status == 'Not Verified'){
              return '<button id="'+ data.id_member +'" class="btn btn-xs btn-dark" disabled><i class="fas fa-lock"></i> Lock</button>';
            }else if(data.status == 'Active'){
              return '<button id="'+ data.id_member +'" onClick="lockMember('+ data.id_member +');" class="btn btn-xs btn-danger"><i class="fas fa-lock"></i> Lock</button>';
            }else{
              return '<button id="'+ data.id_member +'" onClick="unlockMember('+ data.id_member +');" class="btn btn-xs btn-success"><i class="fas fa-unlock"></i> Unlock</button>';
            }
          }
        }
      ],
      "columnDefs" :[
        {
          "targets" : [ 0,5,6,7,8,9,10,11 ],
          "className" : 'text-center'
        }
      ]
    });

    return renderTableMember;
  }

  function reloadTableMember(){
    $('#list_member').dataTable().api().ajax.reload();
  }

  function lockMember(id_member){
    $.ajax({
      url : '<?=site_url('admin_member/lock_member');?>',
      type : 'GET',
      data : { id_member : id_member }
    })
    .done(function(result){
      reloadTableMember();
      if(result == 'berhasil'){
        var heading = 'Success!';
        var message = 'Lock Member Berhasil.';
        var color = 'success';
        generateToast(heading, message, color);
      }else{
        var heading = 'Failed!';
        var message = 'Lock Member Gagal, silahkan coba kembali.';
        var color = 'danger';
        generateToast(heading, message, color);
      }
    });
  }

  function unlockMember(id_member){
    $.ajax({
      url : '<?=site_url('admin_member/unlock_member');?>',
      type : 'GET',
      data : { id_member : id_member }
    })
    .done(function(result){
      reloadTableMember();
      if(result == 'berhasil'){
        var heading = 'Success!';
        var message = 'Unlock Member Berhasil.';
        var color = 'success';
        generateToast(heading, message, color);
      }else{
        var heading = 'Failed!';
        var message = 'unlock Member Gagal, silahkan coba kembali.';
        var color = 'danger';
        generateToast(heading, message, color);
      }
    });
  }
  </script>

  <!-- Order Success SCRIPT -->
  <script>
  function generateTableOrderSuccess(){
    var renderTableOrderSuccess = $('#list_order_success').dataTable({
      "dom": '<"top"f>rt<"bottom"lip>',
      "pageLength": 10,
      "order": [[10,"desc"]],
      "lengthMenu": [[10,25,50,-1], [ '10', '25', '50', 'All' ]],
      "processing": true,
      "ajax"      : {
        url     : '<?=site_url('admin_order_success/json_order_success');?>',
        type    : 'POST',
        dataSrc : 'data',
        error   : function(){
          alert("error");
        }
      },
      "columns" :[
        { data: "no" },
        { 
          data: null,
          render: function(data){
            var action_button;
            action_button = '<a class="btn btn-xs btn-info" href="#" onClick="detailOrderSuccessModal('+ data.id_order +')"><i class="fas fa-info-circle"></i> View Detail Order</a>';
            return action_button;
          }
        },
        { data: "nama_lengkap" },
        { data: "no_invoice" },
        { data: "sub_total" },
        { data: "ongkir" },
        { data: "tambahan" },
        { data: "kode_unik" },
        { data: "grand_total" },
        { data: "ekspedisi" },
        { data: "created_date" },
        { data: "updated_date" },
        { data: "created_by" },
        { data: "updated_by" },
        { 
          data: null,
          render: function(data){
            if(data.status_print == 0 || data.status_print == null){
              return '<span class="badge badge-dark">Belum di Print</span>';
            }else if(data.status_print == 1){
              return '<span class="badge badge-success">Sudah di Print</span>';
            }
          } 
        }
      ],
      "columnDefs" :[
        {
          "targets" : [ 1 ],
          "orderable" : false
        },
        {
          "targets" : [ 0,1,2,3,9,10,11,12 ],
          "className" : 'text-center'
        },
        {
          "targets" : [ 4,5,6,7,8 ],
          "className" : 'text-right'
        }
      ]
    });

    return renderTableOrderSuccess;
  }

  function reloadTableOrderSuccess(){
    $('#list_order_success').dataTable().api().ajax.reload();
  }

  function detailOrderSuccessModal(id_order){
    $.ajax({
      url: '<?=site_url('admin_order_success/detail');?>',
      data: { id_order : id_order },
      method: 'GET'
    })
    .done(function(res){
      $('#load_detail_order').empty();
      $('#load_detail_order').html(res);
    });
    $('#detailOrderSuccessModal').modal('show');
  }

  function printOrderSuccess(){
    $('.printThisSuccess').printThis({
      header: '<div class="text-center"><img src="<?=base_url('assets/img/logo.svg');?>" width="100px"></div>'
    });
  }
  </script>

  <!-- Order Cancel SCRIPT -->
  <script>
  function generateTableOrderCancel(){
    var renderTableOrderCancel = $('#list_order_cancel').dataTable({
      "dom": '<"top"f>rt<"bottom"lip>',
      "pageLength": 10,
      "order": [[10,"desc"]],
      "lengthMenu": [[10,25,50,-1], [ '10', '25', '50', 'All' ]],
      "processing": true,
      "ajax"      : {
        url     : '<?=site_url('admin_order_cancel/json_order_cancel');?>',
        type    : 'POST',
        dataSrc : 'data',
        error   : function(){
          alert("error");
        }
      },
      "columns" :[
        { data: "no" },
        { 
          data: null,
          render: function(data){
            var action_button;
            action_button = '<a class="btn btn-xs btn-info" href="#" onClick="detailOrderCancelModal('+ data.id_order +')"><i class="fas fa-info-circle"></i> View Detail Order</a>';
            return action_button;
          }
        },
        { data: "nama_lengkap" },
        { data: "no_invoice" },
        { data: "sub_total" },
        { data: "ongkir" },
        { data: "tambahan" },
        { data: "kode_unik" },
        { data: "grand_total" },
        { data: "ekspedisi" },
        { data: "created_date" },
        { data: "updated_date" },
        { data: "created_by" },
        { data: "updated_by" }
      ],
      "columnDefs" :[
        {
          "targets" : [ 1 ],
          "orderable" : false
        },
        {
          "targets" : [ 0,1,2,3,9,10,11,12 ],
          "className" : 'text-center'
        },
        {
          "targets" : [ 4,5,6,7,8 ],
          "className" : 'text-right'
        }
      ]
    });

    return renderTableOrderCancel;
  }

  function reloadTableOrderSuccess(){
    $('#list_order_cancel').dataTable().api().ajax.reload();
  }

  function detailOrderCancelModal(id_order){
    $.ajax({
      url: '<?=site_url('admin_order_cancel/detail');?>',
      data: { id_order : id_order },
      method: 'GET'
    })
    .done(function(res){
      $('#load_detail_order').empty();
      $('#load_detail_order').html(res);
    });
    $('#detailOrderCancelModal').modal('show');
  }
  </script>

  <!-- Order Pending SCRIPT -->
  <script>
  function generateTableOrderPending(){
    var renderTableOrderPending = $('#list_order_pending').dataTable({
      "dom": '<"top"f>rt<"bottom"lip>',
      "pageLength": 10,
      "order": [[11,"desc"]],
      "lengthMenu": [[10,25,50,-1], [ '10', '25', '50', 'All' ]],
      "processing": true,
      "ajax"      : {
        url     : '<?=site_url('admin_order_pending/json_order_pending');?>',
        type    : 'GET',
        dataSrc : 'data'
      },
      "columns" :[
        { data: "no" },
        { 
          data: null,
          render: function(data){
            var action_button;
            action_button = '<div class="btn-group"> ';
              action_button += '<button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  ';
                action_button += 'Action  ';
              action_button += '</button>  ';
              action_button += '<div class="dropdown-menu">  ';
                //action_button += '<a class="dropdown-item" href="#" onClick="editOrderPendingModal('+ data.id_order +')"><i class="fas fa-edit"></i> Edit Order</a>  ';
                action_button += '<a class="dropdown-item" href="#" onClick="confirmOrderPendingModal('+ data.id_order +')"><i class="fas fa-check"></i> Manual Confirm Order</a>  ';
                action_button += '<div class="dropdown-divider"></div>  ';
                action_button += '<a class="dropdown-item" href="#" onClick="detailOrderPendingModal('+ data.id_order +')"><i class="fas fa-info"></i> View Detail Order</a>  ';
              action_button += '</div>  ';
            action_button += '</div>  ';

            return action_button;
          }
        },
        { data: "nama_lengkap" },
        { data: "no_invoice" },
        { data: "sub_total" },
        { data: "ongkir" },
        { data: "tambahan" },
        { data: "kode_unik" },
        { data: "grand_total" },
        { data: "ekspedisi" },
        { data: "created_date" },
        { data: "created_by" }
      ],
      "columnDefs" :[
        {
          "targets" : [ 0,1,7,9,10,11 ],
          "className" : 'text-center'
        },
        {
          "targets" : [ 4,5,6,7,8 ],
          "className" : 'text-right'
        }
      ]
    });

    return renderTableOrderPending;
  }

  function reloadTableOrderPending(){
    $('#list_order_pending').dataTable().api().ajax.reload();
  }

  function detailOrderPendingModal(id_order){
    $.ajax({
      url: '<?=site_url('admin_order_pending/detail');?>',
      data: { id_order : id_order },
      method: 'GET'
    })
    .done(function(res){
      $('#load_detail_order').empty();
      $('#load_detail_order').html(res);
    });
    $('#detailOrderPendingModal').modal('show');
  }

  function editOrderPendingModal(id_order){
    $('#editOrderPendingModalData').load('<?=site_url('admin_order_pending/edit');?>', { id_order:id_order }, function(result){
      $(this).empty();
      $(this).html(result);
    });
    $('#editOrderPendingModal').modal('show');
    return false;
  }

  function reduceProdukEdit(id_order_produk){
    alert(id_order_produk);
  }

  function confirmOrderPendingModal(id_order){
    $('#confirmOrderPendingModalData').load('<?=site_url('admin_order_pending/confirm');?>', { id_order:id_order }, function(result){
      $(this).empty();
      $(this).html(result);
    });
    $('#confirmOrderPendingModal').modal('show');
    return false;
  }
  </script>

  <!-- New Order SCRIPT -->
  <script>
  function loadCustomerInformation(){
    var id_member = $('#id_member_new_order').val();
    $.get('<?=site_url('admin_new_order/json_customer_information');?>', { id_member : id_member }, function(result){
        var json_data = JSON.parse(result);
        $('#nonl').text(json_data[0]['nama_lengkap']);
        $('#noem').text(json_data[0]['email']);
        $('#noph').text(json_data[0]['phone']);
        $('#noal').text(json_data[0]['alamat']);
        $('#nokp').text(json_data[0]['kode_pos']);
      }
    );

    if(id_member != '' || id_member != null){
      $('#ekspedisi_new_order').attr('disabled', false);
    }else{
      $('#ekspedisi_new_order').attr('disabled', true);
    }
    loadEkspedisiOngkir();
  }

  function loadEkspedisiOngkir(){
    $('#ekspedisi_new_order').select2({
      theme: "bootstrap",
      placeholder: "Pilih Jenis Pengiriman"
    });
    var xxxx = $('#id_member_new_order').val();
    $.ajax({
      url: '<?=site_url('admin_new_order/json_ekspedisi');?>',
      data: { id_member : xxxx },
      method: 'GET',
      dataType: 'json'
    })
    .done(function(result){
      $('#ekspedisi_new_order').empty();
      $('#ekspedisi_new_order').append('<option value=""></option>');
      $.each(result, function(key, value) {
        $('#ekspedisi_new_order').append('<option value='+ value.id +'>'+ value.text +'</option>');
      });
    });
  }

  function loadProdukInformation(){
    $.ajax({
      url: '<?=site_url('admin_new_order/load_produk');?>',
      method: 'POST'
    })
    .done(function(result){
       $('#nolp').html(result);
    });
  }

  function loadProdukFooterInformation(){
    var ong = $('#hidden_ongkir').val();
    var tam = $('#hidden_tambahan').val();
    var cat = $('#hidden_catatan').val();

    $.ajax({
      url: '<?=site_url('admin_new_order/load_produk_footer');?>',
      data: { ong:ong, tam:tam, cat:cat },
      method: 'POST'
    })
    .done(function(result){
       $('#nofo').html(result);
    });
  }

  function biayaTambahanNewOrder(){
    var catatan = $('#catatan_new_order').val();
    var tambahan = $('#tambahan_new_order').val();
    if(catatan == ""){
      var heading = 'Something Wrong!';
      var message = 'Silahkan Isi Catatan Terlebih Dahulu.';
      var color = 'warning';
      generateToast(heading, message, color);
    }else{
      $('#hidden_catatan').val(catatan);
    }

    if(tambahan == ""){
      var heading = 'Something Wrong!';
      var message = 'Silahkan Isi Biaya Tambahan Terlebih Dahulu.';
      var color = 'warning';
      generateToast(heading, message, color);
    }else{
      $('#hidden_tambahan').val(tambahan);
    }

    if(catatan != "" && tambahan != ""){
      loadProdukFooterInformation();
    }

  }

  function addProdukNewOrder(){
    var id_produk = $('#id_produk_new_order').val();
    if(id_produk == null){
      var heading = 'Something Wrong!';
      var message = 'Silahkan Pilih Produk Terlebih Dahulu.';
      var color = 'warning';
      generateToast(heading, message, color);
    }

    var qty = $('#qty_new_order').val();
    if(qty == "" || qty == 0){
      var heading = 'Something Wrong!';
      var message = 'Silahkan Isi Qty Terlebih Dahulu.';
      var color = 'warning';
      generateToast(heading, message, color);
    }

    var size = $('#size_new_order').val();
    if(size == ""){
      var heading = 'Something Wrong!';
      var message = 'Silahkan Pilih Size Terlebih Dahulu.';
      var color = 'warning';
      generateToast(heading, message, color);
    }

    if(id_produk != null && qty != "" && qty != 0 && size != ""){
      $.ajax({
        url: '<?=site_url('admin_new_order/add');?>',
        method: 'POST',
        data: { id_produk : id_produk, qty : qty, size : size }
      })
      .done(function(result){
        if(result == "berhasil"){
          var heading = 'Success!';
          var message = 'Tambah Produk Berhasil.';
          var color = 'success';
          generateToast(heading, message, color);
        }else{
          var heading = 'Something Wrong!';
          var message = 'Tambah Produk Gagal, Silahkan refresh Halaman.';
          var color = 'danger';
          generateToast(heading, message, color);
        }

        $('#id_produk_new_order').val('').change().focus();
        $('#qty_new_order').val('');
        $('#size_new_order').val('').change();
        loadProdukInformation();
        loadProdukFooterInformation();
        loadEkspedisiOngkir()
      });
    }
  }

  function generateGrandTotal(){
    var data = $('#ekspedisi_new_order').val();
    var data = $.trim(data);
    var explode = data.split('|');
    var ongkir = explode[0];
    var ekspedisi = explode[1];
    console.log(data);
    $('#hidden_ongkir').val('0');
    $('#hidden_ekspedisi').val('');
    $('#hidden_ongkir').val(ongkir);
    $('#hidden_ekspedisi').val(ekspedisi);
    loadProdukInformation();
    loadProdukFooterInformation();
  }

  function deleteProdukOrder(id_cart){
    $.ajax({
      url: '<?=site_url('admin_new_order/remove');?>',
      method: 'POST',
      data: { id_cart : id_cart }
    })
    .done(function(result){
      if(result == "berhasil"){
        var heading = 'Success!';
        var message = 'Remove Produk Berhasil.';
        var color = 'success';
        generateToast(heading, message, color);
      }else{
        var heading = 'Something Wreong!';
        var message = 'Remove Produk Gagal.';
        var color = 'danger';
        generateToast(heading, message, color);
      }

      loadProdukInformation();
      loadProdukFooterInformation();
    });

  }
  </script>

</body>

</html>