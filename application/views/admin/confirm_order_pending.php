<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="form-group">
				<label for="id_bank_conf">No Rekening Payment</label>
				<select class="form-control" id="id_bank_conf" name="id_bank_conf" required>
					<option value="">Select No Rekening</option>
				<?php
				foreach($arr_bank->result() as $res){
				?>
					<option value="<?=$res->id_bank;?>"><?=$res->no_rekening;?> - <?=$res->atas_nama;?> (<?=$res->nama_bank;?>)</option>
				<?php } ?>
				</select>
			</div>
			<div class="form-group">
			<?php
			foreach($arr_customer->result() as $res){
			?>
				<input type="hidden" class="form-control" id="id_member_conf" name="id_member_conf" value="<?=$res->id_member;?>" required>
			<?php } ?>
				<input type="hidden" class="form-control" id="id_admin_conf" name="id_admin_conf" value="<?=$id_admin;?>" required>
				<input type="hidden" class="form-control" id="id_order_conf" name="id_order_conf" value="<?=$id_order;?>" required>
			</div>
		</div>
	</div>
</div>