<!-- partial:partials/_footer.html -->
<footer class="footer">
  <div class="container-fluid clearfix">
    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">
      Copyright © 2018 Pekgo Apparel - 
      Powered by 
      <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved. - 
      Modified by <a href="https://twitter.com/adampm" target="_blank">APM Web Dev</a> - 
      Version 2.0.0
    </span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
      <i class="mdi mdi-heart text-danger"></i>
    </span>
  </div>
</footer>
<!-- partial -->