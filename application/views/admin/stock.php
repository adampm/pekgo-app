<div class="content-wrapper">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#tab_list">List Stock</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#tab_add">Manage Stock</a>
    </li>
  </ul>

  <div class="row">
    <div class="col-lg-12">
      <div class="card">

        <div class="tab-content">

          <div class="tab-pane container active" id="tab_list">
            <div class="card-body">
              <div class="table-responsive">
                <table id="list_stock" class="table table-bordered" style="width:100%;">
                  <thead>
                    <tr>
                      <th class="text-center" style="width:30px;">#</th>
                      <th class="text-left">Nama Produk</th>
                      <th class="text-left">Stock S</th>
                      <th class="text-left">Stock M</th>
                      <th class="text-left">Stock L</th>
                      <th class="text-left">Stock XL</th>
                      <th class="text-left">Stock XXL</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>

          <div class="tab-pane container fade" id="tab_add">
            <div class="card-body">
              <?=form_open('admin_stock', $form_attr); ?>
                <div class="form-group">
                  <label for="id_produk">Produk</label>
                  <select id="id_produk" name="id_produk" class="form-control" required>
                    <option value="">- Pilih Produk -</option>
                  <?php
                  foreach($list_produk->result() as $res_produk){
                  ?>
                    <option value="<?=$res_produk->id_produk;?>"><?=$res_produk->nama_produk;?></option>
                  <?php
                  }
                  ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="tipe">Tipe</label>
                  <select id="tipe" name="tipe" class="form-control" required>
                    <option value="">- Tipe Stock -</option>
                    <option value="add">Add Stock</option>
                    <option value="reduce">Reduce Stock</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="harga_s">Stock S</label>
                  <input type="number" id="stock_s" name="stock_s" class="form-control" value="<?=set_value('stock_s');?>" placeholder="Stock S">
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('stock_s'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="stock_m">Stock M</label>
                  <input type="number" id="stock_m" name="stock_m" class="form-control" value="<?=set_value('stock_m');?>" placeholder="Stock M">
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('stock_m'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="stock_l">Stock L</label>
                  <input type="number" id="stock_l" name="stock_l" class="form-control" value="<?=set_value('stock_l');?>" placeholder="Stock L">
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('stock_l'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="stock_xl">Stock XL</label>
                  <input type="number" id="stock_xl" name="stock_xl" class="form-control" value="<?=set_value('stock_xl');?>" placeholder="Stock XL">
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('stock_xl'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="stock_xl">Stock XXL</label>
                  <input type="number" id="stock_xxl" name="stock_xxl" class="form-control" value="<?=set_value('stock_xxl');?>" placeholder="Stock XXL">
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('stock_xxl'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block">Save</button>
                </div>
              <?=form_close();?>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>

</div>
<!-- content-wrapper ends -->