<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <div class="nav-link">
        <div class="user-wrapper">
          <div class="text-wrapper">
            <p class="profile-name"><i class="fas fa-user"></i> <?=$nama_lengkap;?></p>
            <div>
              <small class="designation"><?=$tipe;?></small>
            </div>
          </div>
        </div>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=site_url('admin/dashboard');?>">
        <i class="menu-icon fas fa-desktop"></i> <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#order-management" aria-expanded="false" aria-controls="order-management">
        <i class="menu-icon fas fa-receipt"></i> <span class="menu-title">Order Management</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="order-management">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_new_order');?>">
              <i class="menu-icon fas fa-plus"></i> <span class="menu-title">Bikin Order Baru</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_order_success');?>">
              <i class="menu-icon fas fa-thumbs-up"></i> <span class="menu-title">Order Success</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_order_pending');?>">
              <i class="menu-icon fas fa-thumbtack"></i> <span class="menu-title">Order Pending</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_order_cancel');?>">
              <i class="menu-icon fas fa-thumbs-up"></i> <span class="menu-title">Order Cancel</span>
            </a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#produk-management" aria-expanded="false" aria-controls="produk-management">
        <i class="menu-icon fas fa-tshirt"></i> <span class="menu-title">Produk</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="produk-management">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_produk');?>">
              <i class="menu-icon fas fa-clipboard-list"></i> <span class="menu-title">Product Setting</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_kategori');?>">
              <i class="menu-icon fas fa-thumbs-up"></i> <span class="menu-title">Kategori Produk</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_stock');?>">
              <i class="menu-icon fas fa-layer-group"></i> <span class="menu-title">Stock Management</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_stock_log');?>">
              <i class="menu-icon fas fa-newspaper"></i> <span class="menu-title">Stock Log</span>
            </a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#member-management" aria-expanded="false" aria-controls="member-management">
        <i class="menu-icon fas fa-users-cog"></i> <span class="menu-title">Member Management</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="member-management">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_member');?>">
              <i class="menu-icon fas fa-clipboard-list"></i> <span class="menu-title">Member List</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_bukti');?>">
              <i class="menu-icon fas fa-camera"></i> <span class="menu-title">Bukti Pembayaran</span>
            </a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#report-data" aria-expanded="false" aria-controls="report-data">
        <i class="menu-icon fas fa-book"></i> <span class="menu-title">Laporan</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="report-data">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_member');?>">
              <i class="menu-icon fas fa-book-open"></i> <span class="menu-title">Penjualan by Order</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=site_url('admin_bukti');?>">
              <i class="menu-icon fas fa-book-open"></i> <span class="menu-title">Penjualan by Produk</span>
            </a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=site_url('admin_rekening');?>">
        <i class="menu-icon fas fa-credit-card"></i> <span class="menu-title">Setting No Rekening</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=site_url('admin_kode_unik');?>">
        <i class="menu-icon fas fa-sort-numeric-down"></i> <span class="menu-title">Setting Kode Unik</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?=site_url('admin/logout');?>">
        <i class="menu-icon fas fa-sign-out-alt"></i> <span class="menu-title">Log out</span>
      </a>
    </li>
  </ul>
</nav>
<!-- partial -->