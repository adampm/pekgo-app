<form id="edit_form" class="form">
	<div class="form-group">
		<input type="text" class="form-control" id="id_produk_edit" name="id_produk_edit" value="<?=$id_produk;?>" readonly required>
	</div>
	<div class="form-group">
		<label for="nama_produk_edit">Nama Produk</label>
		<input type="text" class="form-control" id="nama_produk_edit" name="nama_produk_edit" value="<?=$nama_produk;?>" placeholder="Nama Produk" minlength="3" maxlength="100" required>
	</div>
	<div class="form-group">
		<label for="id_kategori_edit">Kategori</label>
		<select class="form-control" id="id_kategori_edit" name="id_kategori_edit" required>
			<option value="">- Select Kategori -</option>
		<?php
		foreach($list_kategori->result() as $res_kategori){
			if($res_kategori->id_kategori == $id_kategori){
				$selected = "selected";
			}else{
				$selected = "";
			}
		?>
			<option value="<?=$res_kategori->id_kategori;?>" <?=$selected;?>>
                <?=$res_kategori->kategori;?>
            </option>
		<?php
		}
		?>
		</select>
	</div>
	<div class="form-group">
		<label for="berat_edit">Berat</label>
		<input type="number" class="form-control" id="berat_edit" name="berat_edit" value="<?=$berat;?>" placeholder="Nama Produk" min="100" max="99999" required>
	</div>
	<div class="form-group">
		<label for="harga_edit">Harga</label>
		<input type="number" class="form-control" id="harga_edit" name="harga_edit" value="<?=$harga;?>" placeholder="Harga" min="1000" max="9999999999" required>
	</div>
</form>