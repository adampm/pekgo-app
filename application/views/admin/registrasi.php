<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Pekgo Apparel Application">
  <meta name="author" content="@manasama77">
  <link rel="shortcut icon" href="<?=base_url('assets/img/logo_fqv_icon.ico');?>" />
  <title>Member Registrasi - Pekgo Apparel</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/iconfonts/mdi/css/materialdesignicons.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.base.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/vendor.bundle.addons.css');?>">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?=base_url('vendor/star-admin/css/style.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/fontawesome/css/all.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('vendor/toast/dist/jquery.toast.min.css');?>">

  <script type="text/javascript" src="<?=base_url('vendor/fontawesome/js/all.min.js');?>"></script>
  <!-- endinject -->
</head>

<body>
<?php
if($this->session->flashdata('temp_sess'))
{
  $tipe_alert = $this->session->flashdata('temp_sess');
}else{
  $tipe_alert = 0;
}
?>
<input type="hidden" id="temp_sess" name="temp_sess" value="<?=$tipe_alert;?>">
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
      <!-- Background Photo by Maroš Markovič from Pexels https://www.pexels.com/photo/aerial-photography-of-forest-1468704/ -->
      <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-three">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto text-center">
            <img class="mb-4" src="<?=base_url('assets/img/logo_white.png');?>" alt="Pekgo Apparel Logo" width="100">
            <h2 class="text-center mb-4 text-white">Registrasi</h2>
            <div class="auto-form-wrapper">
              <?=form_open('member/registrasi', $form_attr); ?>
                <div class="form-group">
                  <div class="input-group">
                    <input type="email" value="<?=set_value('apm_email');?>" class="form-control" id="apm_email" name="apm_email" placeholder="Email">
                  </div>
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('apm_email'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <input type="password" value="<?=set_value('apm_password');?>" class="form-control" id="apm_password" name="apm_password" placeholder="Password">
                  </div>
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('apm_password'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <input type="password" value="<?=set_value('apm_repassword');?>" class="form-control" id="apm_repassword" name="apm_repassword" placeholder="Confirm Password">
                  </div>
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('apm_repassword'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" value="<?=set_value('apm_nama_lengkap');?>" class="form-control" id="apm_nama_lengkap" name="apm_nama_lengkap" placeholder="Nama Lengkap" minlength="3" maxlength="50">
                  </div>
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('apm_nama_lengkap'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                     <input type="text" value="<?=set_value('apm_phone');?>" class="form-control" id="apm_phone" name="apm_phone" placeholder="Phone" minlength="9" maxlength="20">
                  </div>
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('apm_phone'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" value="<?=set_value('apm_birth_place');?>" class="form-control" id="apm_birth_place" name="apm_birth_place" placeholder="Kota Kelahiran" minlength="5" maxlength="50">
                  </div>
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('apm_birth_place'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <input type="date" timezone="Asia/Jakarta" value="<?=set_value('apm_birth_date');?>" class="form-control" id="apm_birth_date" name="apm_birth_date" placeholder="Tanggal Lahir">
                  </div>
                  <div class="help-block text-danger text-left">
                    <?php echo form_error('apm_birth_date'); ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <select id="id_province_inp" name="apm_province" class="form-control" required>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <select id="id_city_inp" name="apm_city" class="form-control" disabled required>
                      <option value="">-Select City-</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group">
                    <select id="id_district_inp" name="apm_district" class="form-control" disabled required>
                      <option value="">-Select District-</option>
                    </select>
                  </div>
                </div>
                <div class="form-group d-flex justify-content-center">
                  <div class="form-check form-check-flat mt-0">
                    <label class="form-check-label text-white">
                      <input type="checkbox" class="form-check-input" required> I agree to the Rules & Terms
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary submit-btn btn-block">Save</button>
                </div>
                <div class="text-block text-center my-3">
                  <button class="btn btn-xs btn-info">View Rules & Terms</button>
                  <hr>
                  <span class="text-small font-weight-semibold text-white">Already have and account ?</span>
                  <a href="<?=base_url('member');?>" class="text-white text-small">Login</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- endinject -->
  <script src="<?=base_url('vendor/star-admin/js/vendor.bundle.base.js');?>"></script>
  <script src="<?=base_url('vendor/star-admin/js/vendor.bundle.addons.js');?>"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?=base_url('vendor/star-admin/js/off-canvas.js');?>"></script>
  <script src="<?=base_url('vendor/star-admin/js/misc.js');?>"></script>
  <!-- endinject -->

<script type="text/javascript" src="<?=base_url('vendor/toast/dist/jquery.toast.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function(){
  var tempSess = $('#temp_sess').val();
  if(tempSess != 0){
    showToast(tempSess);
  }

  loadProvince();

  $('#id_province_inp').on('change', function(){
    var id_province = $(this).val();
    if(id_province == ''){
      $('#id_city_inp').attr('disabled', true).val('').change();
    }else{
      loadCity(id_province);
    }
    
  });

  $('#id_city_inp').on('change', function(){
    var id_city = $(this).val();
    if(id_city == ''){
      $('#id_district_inp').attr('disabled', true).val('').change();
    }else{
      loadSubDistrict(id_city);
    }
    
  });

  function loadProvince(){
    $.ajax({
      url : '<?=base_url('member/json_province');?>',
      method : 'POST',
      dataType : 'json'
    }).
    done(function(result){
      $('#id_province_inp').append('<option value="">-Select Provnice-</option>');
      $.each(result['rajaongkir']['results'], function(key, value){
        $('#id_province_inp').append('<option value="'+ value['province_id'] +'">'+ value['province'] +'</option>');
      });
    });
  }

  function loadCity(id_province){
    $.ajax({
      url : '<?=base_url('member/json_city');?>',
      method : 'POST',
      data : { id_province : id_province },
      dataType : 'json'
    }).
    done(function(result){
      $('#id_city_inp').attr('disabled', false);
      $('#id_city_inp').empty();
      $('#id_city_inp').append('<option value="">-Select City-</option>');
      $.each(result['rajaongkir']['results'], function(key, value){
        $('#id_city_inp').append('<option value="'+ value['city_id'] +'">' + value['type'] + ' '+ value['city_name'] +'</option>');
      });
    });
  }

  function loadSubDistrict(id_city){
    $.ajax({
      url : '<?=base_url('member/json_subdistrict');?>',
      method : 'POST',
      data : { id_city : id_city },
      dataType : 'json'
    }).
    done(function(result){
      $('#id_district_inp').attr('disabled', false);
      $('#id_district_inp').empty();
      $('#id_district_inp').append('<option value="">-Select District-</option>');
      $.each(result['rajaongkir']['results'], function(key, value){
        $('#id_district_inp').append('<option value="'+ value['subdistrict_id'] +'">'+ value['subdistrict_name'] +'</option>');
      });
    });
  }

  function showToast(tipe){
  if(tipe == 'success'){
    var heading = 'Registrasi Berhasil!';
    var message = 'Silahkan kamu cek Email kamu di Inbox Email / Spam Email kamu untuk aktivasi account kamu. Terima Kasih.';
    var color = 'success';
  }else if(tipe == 'error'){
    var heading = 'Registrasi Gagal!';
    var message = 'Silahkan Coba Kembali. Terima Kasih.';
    var color = 'danger';
  }

  $.toast({
      text: message,
      heading: heading,
      icon: color,
      showHideTransition: 'fade',
      allowToastClose: false,
      hideAfter: 10000,
      stack: 1,
      position: 'top-left',
      textAlign: 'left',
      loader: true,
      loaderBg: '#9EC600',    
  });
}

});
</script>
</body>

</html>